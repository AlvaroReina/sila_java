package sila_tools.bridge.utils;

import java.io.IOException;
import java.io.StringReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Nonnull;

import com.google.protobuf.Descriptors;

import io.grpc.HandlerRegistry;
import io.grpc.ManagedChannel;
import io.grpc.MethodDescriptor;
import io.grpc.ServerMethodDefinition;
import sila_tools.code_generator.MalformedSiLAFeature;
import sila_tools.code_generator.SiLACodeGenerator;

/**
 * A Grpc HanderReqistry that keeps the server and its connection and dynamically can look up the server methods.
 * This is used in the bridge so the bridge can act as a proxy for the server by using this as the so-called
 * FallbackHandlerRegistry.
 *
 * @implNote Meta Features could register "Plugs" to alter calls to server
 * @implNote This will need a programmatic structure of the Feature Definition to check on,
 * For now, this code only intercepts at the proto level.
 */
public class SiLAServerProxyRegistry extends HandlerRegistry {
    private final MethodDescriptor.Marshaller<byte[]> byteMarshaller = new GrpcProxy.ByteMarshaller();

    private final Map<String, Descriptors.ServiceDescriptor> featureMap = new HashMap<>();
    private ManagedChannel serverChannel;

    /**
     * Registry Constructor
     *
     * @param serverChannel gRPC Channel to be used to forward to SiLA Server
     * @param featureDefinitions Map with FeatureIdentifier as Keys and and the Content of the FDL as Values
     */
    SiLAServerProxyRegistry(@Nonnull ManagedChannel serverChannel,
                            @Nonnull Map<String, String> featureDefinitions) throws IOException,
            MalformedSiLAFeature {
        this.serverChannel = serverChannel;

        for (Map.Entry<String, String> entry : featureDefinitions.entrySet()) {
            final Descriptors.FileDescriptor protoFile;
            try {
                final SiLACodeGenerator siLACodeGenerator = new SiLACodeGenerator();

                protoFile = siLACodeGenerator.getProto(new StringReader(entry.getValue()));
            } catch (Descriptors.DescriptorValidationException e) {
                throw new IOException(e.getMessage());
            }

            List<Descriptors.ServiceDescriptor> services = protoFile.getServices();
            for (Descriptors.ServiceDescriptor service : services) {
                String key = service.getName();
                featureMap.put(key, service);
            }
        }
    }

    @Override
    public ServerMethodDefinition<?, ?> lookupMethod(String methodName, String authority) {
        // (pkg).Feature/Command
        final String[] splitMethod = methodName.split("\\.");
        final String featureCommand = splitMethod[splitMethod.length - 1];
        final String featureName = featureCommand.split("/")[0];
        final String commandName = featureCommand.split("/")[1];

        Descriptors.ServiceDescriptor feature = featureMap.get(featureName);
        if(feature == null) {
            throw new IllegalStateException(featureName + " not found for server");
        }
        Descriptors.MethodDescriptor commandDescriptor = feature.findMethodByName(commandName);
        GrpcProxy handler = new GrpcProxy(this.serverChannel, commandDescriptor);

        MethodDescriptor<byte[], byte[]> methodDescriptor
                = MethodDescriptor.newBuilder(byteMarshaller, byteMarshaller)
                .setFullMethodName(methodName)
                .setType(MethodDescriptor.MethodType.UNKNOWN)
                .build();
        return ServerMethodDefinition.create(methodDescriptor, handler);
    }
}
