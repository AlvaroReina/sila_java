package sila_tools.bridge.utils;

import com.google.common.io.ByteStreams;
import com.google.protobuf.Descriptors;
import com.google.protobuf.DynamicMessage;
import io.grpc.*;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import sila2.org.silastandard.SiLAFramework;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import static sila_library.utils.SiLAErrors.generateGRPCError;

/**
 * A gRPC Proxy to validate calls in front of the implementation
 */
@Slf4j
public class GrpcProxy implements ServerCallHandler<byte[], byte[]> {
    private final Channel channel;

    // @implNote: Used in the future to act on protobuf description of the service
    private final Descriptors.MethodDescriptor commandDescriptor;

    GrpcProxy(Channel channel,
                     Descriptors.MethodDescriptor commandDescriptor) {
        this.channel = channel;
        this.commandDescriptor = commandDescriptor;
    }

    @Override
    public ServerCall.Listener<byte[]> startCall(
            ServerCall<byte[], byte[]> serverCall, Metadata headers) {
        ClientCall<byte[], byte[]> clientCall
                = channel.newCall(serverCall.getMethodDescriptor(), CallOptions.DEFAULT);
        CallProxy proxy = new CallProxy(serverCall, clientCall);
        clientCall.start(proxy.clientCallListener, headers);
        serverCall.request(1);
        clientCall.request(1);

        return proxy.serverCallListener;
    }

    private class CallProxy {
        final RequestProxy serverCallListener;
        final ResponseProxy clientCallListener;

        CallProxy(ServerCall<byte[], byte[]> serverCall, ClientCall<byte[], byte[]> clientCall) {
            serverCallListener = new RequestProxy(clientCall);
            clientCallListener = new ResponseProxy(serverCall);
        }

        private class RequestProxy extends ServerCall.Listener<byte[]> {
            private final ClientCall<byte[], ?> clientCall;
            // Hold 'this' lock when accessing
            private boolean needToRequest;

            RequestProxy(ClientCall<byte[], ?> clientCall) {
                this.clientCall = clientCall;
            }

            @Override public void onCancel() {
                clientCall.cancel("Server cancelled", null);
            }

            @Override public void onHalfClose() {
                clientCall.halfClose();
            }

            @Override public void onMessage(byte[] message) {
                clientCall.sendMessage(message);

                synchronized (this) {
                    if (clientCall.isReady()) {
                        clientCallListener.serverCall.request(1);
                    } else {
                        needToRequest = true;
                    }
                }
            }

            @Override public void onReady() {
                clientCallListener.onServerReady();
            }

            synchronized void onClientReady() {
                if (needToRequest) {
                    clientCallListener.serverCall.request(1);
                    needToRequest = false;
                }
            }

            /**
             * @implNote This code snippet just serves as an example how the proxy could cancel on Field Inexistence
             *      Actually would have to be properly updates, as there are for example optional fields as for filters
             *      of dynamic properties
             *
             * Example of usage
             * * <pre>
             * {@code
             * final Descriptors.Descriptor inputType = GrpcProxy.this.commandDescriptor.getInputType();
             * final DynamicMessage dynamicMessage = DynamicMessage.parseFrom(inputType, message);
             * cancelOnFieldInexistence(dynamicMessage,
             *     dynamicMessage.getDescriptorForType().getFields()
             * )
             * }
             * </pre>
             *
             * @param dynamicMessage      Dynamic Protobuf Message
             * @param fieldDescriptorList Field Descriptions of Protobuf Message
             */
            private void cancelOnFieldInexistence(
                    DynamicMessage dynamicMessage,
                    List<Descriptors.FieldDescriptor> fieldDescriptorList
            ) {
                for (Descriptors.FieldDescriptor field : fieldDescriptorList) {
                    if (field.getType().equals(Descriptors.FieldDescriptor.Type.MESSAGE)) {
                        cancelOnFieldInexistence(
                                dynamicMessage,
                                field.getMessageType().getFields()
                        );

                        if (!dynamicMessage.hasField(field)) {
                            final String fieldName = field.getName();
                            final String errorMessage =
                                    fieldName + " parameter was not set in call.";
                            log.error(errorMessage);

                            final StatusRuntimeException statusRuntimeException = generateGRPCError(
                                    SiLAFramework.SiLAError.newBuilder()
                                            .setErrorType(SiLAFramework.SiLAError.ErrorType.VALIDATION)
                                            .setIdentifier(fieldName)
                                            .setMessage(errorMessage)
                                            .build());

                            throwClientError(statusRuntimeException);
                            return;
                        }
                    }
                }
            }

            /**
             * Manually call the closing to imitate same behaviour as StreamObserver
             */
            private void throwClientError(StatusRuntimeException statusRuntimeException) {
                clientCall.halfClose();

                val metaData = new Metadata();
                metaData.put(Metadata.Key.of("content-type", Metadata.ASCII_STRING_MARSHALLER), "aplication/grpc");
                clientCallListener.serverCall.close(statusRuntimeException.getStatus(), metaData);
            }
        }

        private class ResponseProxy extends ClientCall.Listener<byte[]> {
            private final ServerCall<?, byte[]> serverCall;
            // Hold 'this' lock when accessing
            private boolean needToRequest;

            ResponseProxy(ServerCall<?, byte[]> serverCall) {
                this.serverCall = serverCall;
            }

            @Override
            public void onClose(Status status, Metadata trailers) {
                serverCall.close(status, trailers);
            }

            @Override public void onHeaders(Metadata headers) {
                serverCall.sendHeaders(headers);
            }

            @Override public void onMessage(byte[] message) {
                serverCall.sendMessage(message);
                synchronized (this) {
                    if (serverCall.isReady()) {
                        serverCallListener.clientCall.request(1);
                    } else {
                        needToRequest = true;
                    }
                }
            }

            @Override public void onReady() {
                serverCallListener.onClientReady();
            }

            void onServerReady() {
                if (needToRequest) {
                    serverCallListener.clientCall.request(1);
                    needToRequest = false;
                }
            }
        }
    }

    static class ByteMarshaller implements MethodDescriptor.Marshaller<byte[]> {
        @Override public byte[] parse(InputStream stream) {
            try {
                return ByteStreams.toByteArray(stream);
            } catch (IOException ex) {
                throw new RuntimeException();
            }
        }

        @Override public InputStream stream(byte[] value) {
            return new ByteArrayInputStream(value);
        }
    }
}
