package sila_tools.bridge.utils;

import io.grpc.*;
import lombok.Value;
import lombok.extern.slf4j.Slf4j;
import sila_features.SiLAServiceServer;
import sila_library.discovery.SiLAServerRegistration;
import sila_tools.code_generator.MalformedSiLAFeature;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Slf4j
public class SiLAServerProxy {
    private final String interfaceName;
    private final SiLAServiceServer silaService;
    private final SiLAServerRegistration serverRegistration;

    private ManagedChannel serverChannel = null;
    private Server proxyServer = null;

    /**
     * Informational construct that shows the server handled by the Proxy
     */
    private SiLAServer serverInfo = null;

    @Value
    private static class SiLAServer {
        String serverName;
        int bridgePort;
        int serverPort;
    }

    /**
     * Proxy constructor.
     *
     * @param interfaceName Name of network interface to use discovery
     */
    public SiLAServerProxy(@Nonnull String interfaceName) {
        this.interfaceName = interfaceName;
        this.silaService = new SiLAServiceServer();
        this.serverRegistration = new SiLAServerRegistration();
    }

    /**
     * Register SiLA Server both for discovery and the mandatory SiLAService.
     *
     * @param serverName         Name used for SiLA Server in Network
     * @param serverInfo         Server Information exposed by the SiLAService
     * @param bridgePort         Port on which the Bridge runs and that gets exposed to the client.
     * @param serverPort         Port on which the server runs. The bridge acts as a proxy and routes requests to this port.
     * @param featureDefinitions Map with FeatureIdentifier as Keys and and the Content of the FDL as Values
     *
     * @implNote Should be started after the gRPC Server is run on serverPort.
     */
    public void start(
            @Nonnull String serverName,
            @Nonnull SiLAServiceServer.ServerInfo serverInfo,
            int bridgePort,
            int serverPort,
            @Nonnull Map<String, String> featureDefinitions
    ) throws IOException, MalformedSiLAFeature {
        log.info(
                "[start] serverName={}, bridgePort={}, serverPort={} ...",
                serverName,
                bridgePort,
                serverPort
        );
        // Shutdown hook
        Runtime.getRuntime().addShutdownHook(
                new Thread(this::stop)
        );

        // Setup Proxy
        this.serverChannel = ManagedChannelBuilder.forAddress(
                "localhost", serverPort)
                .usePlaintext(true)
                .build();

        SiLAServerProxyRegistry registry = new SiLAServerProxyRegistry(
                this.serverChannel,
                featureDefinitions
        );

        this.proxyServer = ServerBuilder.forPort(bridgePort)
                .addService(this.silaService.getService())
                .fallbackHandlerRegistry(registry)
                .build()
                .start();

        log.info("[start] proxyServer started on port={}",bridgePort);
        // Setup Feature & Server Discovery
        this.silaService.registerServer(serverInfo, featureDefinitions);

        log.info("[start] Server registered on SiLAService with features={}", featureDefinitions.keySet());

        this.serverRegistration.RegisterServer(serverName, this.interfaceName, bridgePort);
        log.info("[start] Server registered with discovery.");

        this.serverInfo = new SiLAServer(serverName, bridgePort, serverPort);
        log.info("[start] Server registration finished!");
    }

    public void stop() {
        log.info("[stop] {}: stopping ...", serverInfo.toString());
        // unregister server
        this.serverRegistration.UnregisterServer();
        log.info("[stop] {}: was unregistered from mDNS", serverInfo.toString());

        if (this.serverChannel != null) {
            try {
                this.serverChannel.shutdownNow().awaitTermination(30, TimeUnit.SECONDS);
                log.info("[stop] {}: closed the serverChannel.", serverInfo.toString());
            } catch (InterruptedException e) {
                log.warn(
                        "[stop] {}: could not shutdown the "
                                + "serverChannel within 30 seconds. Exception e={}",
                        serverInfo.toString(), e
                );
            }
        }

        // Stop proxy server
        if (this.proxyServer != null && !(this.proxyServer.isTerminated())) {
            try {
                log.info("[stop] {}: stopping the proxyServer ...", serverInfo.toString());
                this.proxyServer.shutdownNow().awaitTermination(30, TimeUnit.SECONDS);
                log.info("[stop] {}: the proxyServer was stopped", serverInfo.toString());
            } catch (InterruptedException e) {
                log.warn(
                        "[stop] {}: could not shutdown the proxyServer within 30 seconds. Exception e={}",
                        serverInfo.toString(),
                        e
                );
            }
        }
        log.info("[stop] {}: stopped", serverInfo.toString());
    }
}
