package sila_tools.bridge.server;

import io.grpc.*;
import lombok.extern.slf4j.Slf4j;
import sila_features.SiLAServiceServer;
import sila_library.discovery.SiLAServerRegistration;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Slf4j
public class SiLAServerCore {
    private final static int SHUTDOWN_TIMEOUT = 20; // [s]

    private SiLAServerRegistration serverRegistration = new SiLAServerRegistration();
    private final SiLAServiceServer silaService = new SiLAServiceServer();

    private Server server = null;

    /**
     * Start the Core to expose the SiLA Features
     *
     * @param serverName            Name used for SiLA Server in Network
     * @param serverInfo            Server Information exposed by the SiLAService
     * @param featureDefinitions    Map with FeatureIdentifier as Keys and and the
     *                              Filepath or Content of the FDL as Values
     * @param port                  Port on which the server runs.
     * @param interfaceName         Name of network interface to use discovery
     * @param bindableServices      Variadic inputs of Service Implementations to bind
     */
    public void start(
            @Nonnull final String serverName,
            @Nonnull final SiLAServiceServer.ServerInfo serverInfo,
            @Nonnull final Map<String, String> featureDefinitions,
            final int port,
            @Nonnull String interfaceName,
            BindableService... bindableServices
    ) throws IOException {
        if (server != null) {
            log.info("Server already started, stopping and restarting...");
            stop();
            server = null;
        }

        this.silaService.registerServer(serverInfo, featureDefinitions);
        log.info("[start] Server registered on SiLAService with features={}", featureDefinitions.keySet());

        final ServerBuilder serverBuilder = ServerBuilder.forPort(port)
                .addService(this.silaService.getService());

        for (BindableService bindableService : bindableServices) {
            serverBuilder.addService(bindableService);
        }

        this.server = serverBuilder.build().start();

        log.info("[start] Server started on port={}", port);

        this.serverRegistration.RegisterServer(serverName, interfaceName, port);
        log.info("[start] Server registered with discovery.");

        Runtime.getRuntime().addShutdownHook(
                new Thread(this::stop)
        );
    }

    private void stop() {
        log.info("[stop] stopping server...");

        // Stop server
        if (this.server != null && !(this.server.isTerminated())) {
            try {
                log.info("[stop] stopping the server ...");
                this.server.shutdownNow().awaitTermination(SHUTDOWN_TIMEOUT, TimeUnit.SECONDS);
                log.info("[stop] the server was stopped");
            } catch (InterruptedException e) {
                log.warn("[stop] could not shutdown the server within {} seconds",
                        String.valueOf(SHUTDOWN_TIMEOUT));
            }
        }
        log.info("[stop] stopped");
    }
}
