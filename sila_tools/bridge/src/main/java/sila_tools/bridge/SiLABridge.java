package sila_tools.bridge;

import lombok.Value;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import sila_features.SiLAServiceServer;
import sila_tools.bridge.utils.SiLAServerProxy;
import sila_tools.code_generator.MalformedSiLAFeature;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.util.*;

/**
 * The SiLABridge is a utility that manages a proxy for SiLA Servers and adds the SiLA core capabilities so the servers
 * do not need to implement it.
 *
 * The current provided capabilities are:
 * - SiLA Discovery
 * - SiLAService Implementation
 *
 * Clients connect to the bridge-proxy port instead of the server directly and can call methods on the bridge the corresponding calls
 * are routed to the actual server.
 *
 * Why is it implemented as a proxy? To be able to provide advanced features such as Parameter Validation
 * and Authentication via the SiLA Bridge.
 *
 * Why not simply interceptors? Because they have to be added either per service or for all of them.
 *
 * You can register a server that running on its own port like this:
 * <code>
 * bridge = new SiLABridge("localhost");
 * int proxyPort = 50021;
 * int serverPort = 50022;
 * bridge.registerServer("MyServer", proxyPort, serverPort,
 * new HashMap<String,String>{{ put("MyFeature","<Feature>...</Feature>"}}
 * );
 * //start your server on the serverPort
 * myServer.start(serverPort)
 * </code>
 *
 * @implNote The idea is to add more "meta features" in the future - server agnostic features such as SiLAService
 */
@Slf4j
public class SiLABridge {
    final private String interfaceName;
    private Map<ServerKey, SiLAServerProxy> proxyMap = new HashMap<>();

    @Value
    private static class ServerKey {
        final String serverName;
        final int serverPort;
    }

    /**
     * Bridge constructor.
     *
     * @param interfaceName Name of network interface to use discovery
     */
    public SiLABridge(@Nonnull String interfaceName) {
        this.interfaceName = interfaceName;
    }

    /**
     * Stop Bridge
     */
    public void stop() {
        for (SiLAServerProxy proxy : this.proxyMap.values()) {
            proxy.stop();
        }
    }

    /**
     * Creates proxy {@link SiLAServerProxy#start(String, SiLAServiceServer.ServerInfo, int, int, Map)}
     *
     * @implNote Only one proxy per server is allowed
     */
    public void registerServer(
            @Nonnull String serverName,
            @Nonnull SiLAServiceServer.ServerInfo serverInfo,
            int bridgePort,
            int serverPort,
            @Nonnull Map<String, String> featureDefinitions
    ) throws IOException, MalformedSiLAFeature {
        log.info(
                "[registerServer] Registering serverName={}, bridgePort={}, serverPort={}, features={} ...",
                serverName,
                bridgePort,
                serverPort,
                featureDefinitions.keySet()
        );
        val key = new ServerKey(serverName, serverPort);
        val existing = proxyMap.get(key);

        if (existing != null) {
            log.info("[registerServer] Server serverName={}, serverPort={} is already " +
                            "registered therefore it is removed first",
                    serverName, serverPort
            );
            existing.stop();
            proxyMap.remove(key);
            log.info("[registerServer] Server serverName={}, serverPort={} stopped and removed",
                    serverName, serverPort
            );
        }

        val proxy = new SiLAServerProxy(interfaceName);
        log.info("[registerServer] starting Server Proxy on interface={} ...", interfaceName);
        try {
            proxy.start(serverName, serverInfo, bridgePort, serverPort, featureDefinitions);
            this.proxyMap.put(key, proxy);
            log.info(
                    "[registerServer] Server serverName={} complete! Now got these registered servers={}",
                    serverName,
                    this.proxyMap.keySet()
            );
        } catch (RuntimeException e) {
            log.error("[registerServer] starting server proxy failed! Reason={}", e);
            proxy.stop();
            throw e;
        }
    }
}