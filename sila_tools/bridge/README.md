# SiLA Bridge
The bridge package contains tools to easily get started exposing a SiLA Conforming Server.

## Components
### SiLA Server Core
A core Utility class for the minimum features of SiLA.

### SiLA Bridge
Component that acts as a gRPC proxy to enable the exposure of server independent
Features, SiLA Discovery and other utilities.

It also exposes SiLA capabilities to processes in different languages, 
such as for example server drivers written in C# or python.

## Building and Running

To install the package, simply use maven:

    mvn install
    
Then the JARs can be deployed on machines running JDK 8:

    cd target/
    java -jar bridge-1.0.0-SNAPSHOT-jar-with-dependencies.jar [port] [interfaceName]

Note: interfaceName "local" is also valid (this is for discovery)
