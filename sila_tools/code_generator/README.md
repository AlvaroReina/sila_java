# Code Generator
Code generator parsing XML to Java Class and creating protobuf definitions.

Future functionality is open for discussion.

## Building and Running

@TODO: Just do it with assembly:assembly
@TODO: Add java_package as an optional

To install the package, simply use maven:

    mvn install
    
Then the JARs can be deployed on machines running JDK 8:

    cd target/
    java -jar code-generator-1.0.0-SNAPSHOT-jar-with-dependencies.jar [path_to_xml] [path_to_proto]
    
To use for example the TestFeature:

    java -jar target/code-generator-1.0.0-SNAPSHOT-jar-with-dependencies.jar src/test/resources/TestFeature.xml TestFeature.proto
    
## Command Line Tool
### Unix
To call the code generator handily, use the install script (only works from directory):

```bash
cd unix_install
./install.sh
```

After installation, the code generator can be called from terminal via terminal:
```bash
sila_code_gen [path_to_feature_def_xml/feature_def.xml] [path_to_generated_proto/generated_proto.proto]
```

### Windows
For windows there are many tools that provide native .exe provision 
such as [JSmooth](http://jsmooth.sourceforge.net/). This hasn't been implemented
or tested yet, so any contributions are appreciated.