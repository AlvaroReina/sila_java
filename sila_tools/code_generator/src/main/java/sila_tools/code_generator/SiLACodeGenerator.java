package sila_tools.code_generator;

import lombok.Data;
import sila2.org.silastandard.SiLAFramework;
import sila_library.models.*;
import com.google.protobuf.Descriptors;

import javax.annotation.Nonnull;
import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigInteger;
import java.util.*;

import static sila_library.utils.FeatureGenerator.generateFeature;
import static sila_tools.code_generator.DynamicProtoBuilder.printProtoFile;

/**
 * Code Generator
 *
 * Currently responsible for generating proto definitions from Feature Definitions
 */
public class SiLACodeGenerator {
    /* SiLA Basic Types Protobuf Type Mapping */
    private final static Map<BasicType, String> typeMap;

    static {
        Map<BasicType, String> tmpMap = new HashMap<>();
        tmpMap.put(BasicType.STRING, SiLAFramework.String.getDescriptor().getFullName());
        tmpMap.put(BasicType.INTEGER, SiLAFramework.Integer.getDescriptor().getFullName());
        tmpMap.put(BasicType.REAL, SiLAFramework.Real.getDescriptor().getFullName());
        tmpMap.put(BasicType.BOOLEAN, SiLAFramework.Boolean.getDescriptor().getFullName());
        tmpMap.put(BasicType.SMALL_BINARY, SiLAFramework.SmallBinary.getDescriptor().getFullName());
        tmpMap.put(BasicType.LARGE_BINARY, SiLAFramework.LargeBinary.getDescriptor().getFullName());
        tmpMap.put(BasicType.DATE, SiLAFramework.Date.getDescriptor().getFullName());
        tmpMap.put(BasicType.TIME, SiLAFramework.Time.getDescriptor().getFullName());
        tmpMap.put(BasicType.TIMESTAMP, SiLAFramework.Timestamp.getDescriptor().getFullName());
        tmpMap.put(BasicType.ANY, SiLAFramework.Any.getDescriptor().getFullName());
        typeMap = Collections.unmodifiableMap(tmpMap);
    }

    private Map<String, DataTypeType> dataTypeIdentifierMap = new HashMap<>();
    private List<String> filterIdentifierList = new ArrayList<>();

    /**
     * Main function takes absolute path to XML document and absolute path to
     * proto file it should create. the XML document is assumed to be SiLA
     * conforming and the .proto file will be created accordingly.
     */
    public static void main(String[] args) throws MalformedSiLAFeature {
        if (args.length != 2) {
            throw new IllegalArgumentException("[path_to_xml] [path_to_proto]");
        }
        final String xmlPath = args[0];
        final String protoPath = args[1];

        Descriptors.FileDescriptor protoFile;
        SiLACodeGenerator siLACodeGenerator = new SiLACodeGenerator();
        try {
            protoFile = siLACodeGenerator.getProto(new FileReader(xmlPath));
        } catch (IOException e) {
            throw new RuntimeException("XML Document coulnd't be parsed: " + e.getMessage());
        } catch (Descriptors.DescriptorValidationException e2) {
            System.out.println("Generated proto is not valid!");
            throw new RuntimeException(e2);
        }

        try (BufferedWriter writer = new BufferedWriter(
                new FileWriter(  protoPath ))) {
            writer.write( printProtoFile(protoFile));
        } catch (IOException e) {
            throw new RuntimeException("Error while creating proto!");
        }
    }

    /**
     * Maps from XML Field definitions to Protofields, essentially covers the type mapping of SiLA.
     *
     * @param xmlFile xml File as an InputStream
     */
    public Descriptors.FileDescriptor getProto(Reader xmlFile)
            throws IOException, Descriptors.DescriptorValidationException, MalformedSiLAFeature {
        return getProto(generateFeature(xmlFile));
    }

    /**
     * Maps from XML Field definitions to Protofields, essentially covers the type mapping of SiLA.
     *
     * @param feature Feature POJO generated from XML
     *      {@link sila_library.utils.FeatureGenerator#generateFeature}
     *
     * @return FileDescriptorProto for programmatic proto definition
     */
    public Descriptors.FileDescriptor getProto(
            @Nonnull Feature feature) throws Descriptors.DescriptorValidationException, MalformedSiLAFeature {
        if (!feature.getSiLAVersion().equals(BigInteger.valueOf(2))) {
            throw new MalformedSiLAFeature("Only SiLA 2 features are supported currently!");
        }

        // Programmatically build Proto File
        String useCase = "";
        if (feature.getUseCase() != null) {
            useCase = feature.getUseCase() + ".";
        }

        final String packageName = "sila2." + feature.getOriginator() + "." +
                useCase + feature.getIdentifier().toLowerCase() + "." +
                "v" + String.valueOf(feature.getMajorVersion());

        DynamicProtoBuilder proto = new DynamicProtoBuilder(
                feature.getIdentifier(),
                packageName);

        dataTypeIdentifierMap.clear();
        for (Feature.DataTypeDefinition dataTypeDefinition : feature.getDataTypeDefinition()) {
            final String dataTypeId = dataTypeDefinition.getIdentifier();

            if (dataTypeIdentifierMap.containsKey(dataTypeId)) {
                throw new MalformedSiLAFeature(dataTypeId + " data type is defined twice!");
            }
            dataTypeIdentifierMap.put(dataTypeId, dataTypeDefinition.getDataType());

            final String msdId = GrpcMapper.getDataType(dataTypeId);
            proto.addMessage(
                    getMessageDescription(msdId, Collections.singletonList(dataTypeDefinition))
            );
        }

        filterIdentifierList.clear();
        for (Feature.Filter filter : feature.getFilter()) {
            final String filterId = filter.getIdentifier();

            if (filterIdentifierList.contains(filterId)) {
                throw new MalformedSiLAFeature(filterId + " filter is defined twice!");
            }
            filterIdentifierList.add(filterId);

            final String msgId = GrpcMapper.getFilter(filterId);
            proto.addMessage(
                    getMessageDescription(msgId, Collections.singletonList(filter))
            );
        }

        final List<String> commandIdentifierList = new ArrayList<>();
        for (Feature.Command command : feature.getCommand()) {
            final String cmdId = command.getIdentifier();

            if (commandIdentifierList.contains(cmdId)) {
                throw new MalformedSiLAFeature(cmdId + " command is defined twice!");
            }
            commandIdentifierList.add(cmdId);

            final String parId = GrpcMapper.getParameter(cmdId);
            final String resId = GrpcMapper.getResponse(cmdId);

            proto.addMessage(getMessageDescription(parId, command.getParameter()));
            proto.addMessage(getMessageDescription(resId, command.getResponse()));

            if (command.getObservable().matches("No")) {
                // Non-observable command
                proto.addCall(cmdId, parId, false, resId, false);
            } else if (command.getObservable().matches("Yes")) {
                // Observable command
                final String cmdExecutionIdType = SiLAFramework.CommandExecutionUUID
                        .getDescriptor().getFullName();

                // Initial Proto Call
                proto.addCall(cmdId, parId, false,
                        SiLAFramework.CommandConfirmation.getDescriptor().getFullName(), false);

                // Status Proto Call
                proto.addCall(GrpcMapper.getStateCommand(cmdId), cmdExecutionIdType, false,
                        SiLAFramework.ExecutionState.getDescriptor().getFullName(), true);

                // Intermediate Responses
                final List<Feature.Command.IntermediateResponse> intermediateResponses = command.getIntermediateResponse();
                if (!intermediateResponses.isEmpty()) {
                    final String interResId = GrpcMapper.getIntermediateResponse(cmdId);

                    proto.addMessage(getMessageDescription(interResId, intermediateResponses));

                    proto.addCall(GrpcMapper.getIntermediateCommand(cmdId),
                            cmdExecutionIdType, false,
                            interResId, true);
                }

                // Final Result
                proto.addCall(GrpcMapper.getResult(cmdId),
                        cmdExecutionIdType, false,
                        resId, false);
            } else {
                throw new MalformedSiLAFeature(
                        "(should never happen) Unsupported SiLAType found");
            }
        }

        final List<String> propertyIdentifierList = new ArrayList<>();
        for (Feature.Property property : feature.getProperty()) {
            final String propId = property.getIdentifier();

            if (propertyIdentifierList.contains(propId)) {
                throw new MalformedSiLAFeature(propId + " property is defined twice!");
            }
            propertyIdentifierList.add(propId);

            final Feature.Property.Filters filters = property.getFilters();

            if (property.getPropertyType().matches("Static")) {
                if (filters != null) {
                    throw new MalformedSiLAFeature("Static Properties don't provide filters.");
                }

                final String callId = GrpcMapper.getStaticProperty(propId);
                final String parId = GrpcMapper.getParameter(callId);
                final String resId = GrpcMapper.getResponse(callId);

                // @Note: property field id = feature id
                proto.addMessage(getMessageDescription(parId, Collections.EMPTY_LIST)); // Empty parameters
                proto.addMessage(
                        getMessageDescription(resId, Collections.singletonList(property))
                );

                proto.addCall(callId, parId, false, resId, false);
            } else if (property.getPropertyType().matches("Dynamic")) {
                final String callId = GrpcMapper.getDynamicProperty(propId);
                final String parId = GrpcMapper.getParameter(callId);
                final String resId = GrpcMapper.getResponse(callId);

                // @implNote we only generate a one layered message so we don't use the generic getters here
                DynamicProtoBuilder.MessageDescription.MessageDescriptionBuilder messageDescriptionBuilder =
                        new DynamicProtoBuilder.MessageDescription.MessageDescriptionBuilder();

                messageDescriptionBuilder.identifier(parId);
                List<DynamicProtoBuilder.FieldDescription> fieldDescriptions = new ArrayList<>();
                if (filters != null) {
                    for (String filterIdentifier : filters.getFilterIdentifier()) {
                        if (!filterIdentifierList.contains(filterIdentifier)) {
                            throw new MalformedSiLAFeature(filterIdentifier + " not defined in Feature!");
                        }

                        final String typeName = GrpcMapper.getFilter(filterIdentifier);
                        fieldDescriptions.add(
                            new DynamicProtoBuilder.FieldDescription(
                                    filterIdentifier,
                                    typeName,
                                    false
                            )
                        );
                    }
                }
                messageDescriptionBuilder.fields(fieldDescriptions);
                // @implNote The description builder needs to have a list of nested messages
                messageDescriptionBuilder.nestedMessages(Collections.EMPTY_LIST);

                proto.addMessage(messageDescriptionBuilder.build());

                proto.addMessage(
                        getMessageDescription(resId, Collections.singletonList(property))
                );

                proto.addCall(callId, parId, false, resId, true);
            }
        }

        // Framework proto dependencies
        Descriptors.FileDescriptor[] empty_arr = new Descriptors.FileDescriptor[1];
        empty_arr[0] = SiLAFramework.getDescriptor();

        return Descriptors.FileDescriptor.buildFrom(proto.getDescriptor(),
                empty_arr);
    }

    private <T> DynamicProtoBuilder.MessageDescription getMessageDescription(
            String identifier,
            @Nonnull List<T> SiLAField) throws MalformedSiLAFeature {
        DynamicProtoBuilder.MessageDescription.MessageDescriptionBuilder messageDescriptionBuilder =
                new DynamicProtoBuilder.MessageDescription.MessageDescriptionBuilder();

        messageDescriptionBuilder.identifier(identifier);

        List<DynamicProtoBuilder.FieldDescription> fieldDescriptions = new ArrayList<>();
        List<DynamicProtoBuilder.MessageDescription> messageDescriptions = new ArrayList<>();

        for (T siLAField : SiLAField) {
            fieldDescriptions.add(
                    generateFieldDescription(siLAField, messageDescriptions)
            );
        }

        messageDescriptionBuilder.fields(fieldDescriptions);
        messageDescriptionBuilder.nestedMessages(messageDescriptions);

        return messageDescriptionBuilder.build();
    }

    @Data
    private class SiLAField {
        private final String identifier;
        private final DataTypeType dataType;
    }

    private DynamicProtoBuilder.FieldDescription generateFieldDescription(
            Object field, List<DynamicProtoBuilder.MessageDescription> messageDescriptions) throws MalformedSiLAFeature {
        // Get fields via Reflection
        String identifier;
        DataTypeType dataTypeType;
        try {
            final Method identifierMethod = field.getClass().getMethod("getIdentifier");
            identifier = (String) identifierMethod.invoke(field);

            final Method typeMethod = field.getClass().getMethod("getDataType");
            dataTypeType = (DataTypeType) typeMethod.invoke(field);
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            throw new MalformedSiLAFeature(e.getMessage());
        }

        // Then invoke creation dependent on type
        if (dataTypeType.getBasic() != null) {
            return new DynamicProtoBuilder.FieldDescription(
                    identifier,
                    typeMap.get(dataTypeType.getBasic()),
                    false
            );
        } else if (dataTypeType.getDataTypeIdentifier() != null) {
            final String dataTypeIdentifier = dataTypeType.getDataTypeIdentifier();
            final String typeName = GrpcMapper.getDataType(dataTypeIdentifier);

            if (!dataTypeIdentifierMap.containsKey(dataTypeIdentifier)) {
                throw new MalformedSiLAFeature(dataTypeIdentifier + " not defined in Feature!");
            }

            return new DynamicProtoBuilder.FieldDescription(
                    identifier,
                    typeName,
                    false
            );
        } else if (dataTypeType.getConstrained() != null) {
            final ConstrainedType constrainedType = dataTypeType.getConstrained();
            checkConstrainedValidity(constrainedType);

            final SiLAField elementField = new SiLAField(identifier, constrainedType.getDataType());

            return generateFieldDescription(elementField, messageDescriptions);
        } else if (dataTypeType.getList() != null) {
            final ListType listType = dataTypeType.getList();

            final DataTypeType elementType = listType.getDataType();

            // @Note: There shall be no Lists of Lists (this includes Lists of Constrained Lists)
            if (elementType.getList() != null) {
                throw new MalformedSiLAFeature(
                        "There MUST be no Lists of Lists (this includes Lists of Constrained Lists)");
            }

            final SiLAField elementField = new SiLAField(identifier, elementType);

            final DynamicProtoBuilder.FieldDescription baseField =
                    generateFieldDescription(elementField, messageDescriptions);
            baseField.setRepeated(true);

            return baseField;
        } else if (dataTypeType.getStructure() != null) {
            StructureType structureType = dataTypeType.getStructure();
            final String nestedMessageIdentifier = GrpcMapper.getStruct(identifier);

            messageDescriptions.add(
                    getMessageDescription(
                            nestedMessageIdentifier,
                            structureType.getElement()
                    )
            );

            return new DynamicProtoBuilder.FieldDescription(
                    identifier,
                    nestedMessageIdentifier,
                    false
            );
        } else {
            throw new MalformedSiLAFeature(
                    "(should never happen) Unsupported SiLAType found");
        }
    }

    private void checkConstrainedValidity(ConstrainedType constrainedType) throws MalformedSiLAFeature {
        final ConstrainedType.Constraints constraints = constrainedType.getConstraints();

        // Get constrained type nested
        final DataTypeType dataTypeType = retrieveNestedDefinition(constrainedType.getDataType());

        // Check constraints for either Basic or List Types (Union types would be handy in Java ...)
        Object siLAType = dataTypeType.getBasic();
        if (siLAType == null) {
            siLAType = dataTypeType.getList();
        }

        if (constraints.getLength() != null) {
            if (!siLAType.equals(BasicType.STRING)) {
                throw new MalformedSiLAFeature("Length Constraint only applies to Basic Type String");
            }
        }

        // @TODO: Do full constrain validity checks
    }

    private DataTypeType retrieveNestedDefinition(final DataTypeType superType) throws MalformedSiLAFeature {
        if (superType.getConstrained() != null ||
                superType.getStructure() != null) {
            throw new MalformedSiLAFeature("The 'SiLA Constrained Type' is based on either a 'SiLA Basic Type' or a 'SiLA List Type'.");
        }

        if (superType.getDataTypeIdentifier() != null) {
            final String dataTypeIdentifier = superType.getDataTypeIdentifier();
            if (!dataTypeIdentifierMap.containsKey(dataTypeIdentifier)) {
                throw new MalformedSiLAFeature(dataTypeIdentifier + " not defined in Feature!");
            }
            return retrieveNestedDefinition(dataTypeIdentifierMap.get(dataTypeIdentifier));
        }

        return superType;
    }
}
