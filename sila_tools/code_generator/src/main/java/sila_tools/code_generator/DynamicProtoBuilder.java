package sila_tools.code_generator;

import com.google.protobuf.DescriptorProtos;
import com.google.protobuf.Descriptors;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import javax.annotation.Nonnull;
import java.util.*;

/**
 * Wrapper for programmatic Proto description, avoids using SiLA Terminology.
 */
public class DynamicProtoBuilder {
    private static final String tab = "    "; // 4 Spaces

    private DescriptorProtos.FileDescriptorProto.Builder current_desc;
    private DescriptorProtos.ServiceDescriptorProto.Builder service_desc;

    /**
     * Constructor
     *
     * @param name Name of the proto file and service
     * @param packageName Package of the created definition
     */
    DynamicProtoBuilder(String name, String packageName) {
        current_desc = DescriptorProtos.FileDescriptorProto.newBuilder();
        current_desc.setName(name);

        current_desc.setPackage(packageName);

        service_desc = DescriptorProtos.ServiceDescriptorProto.newBuilder();
        service_desc.setName(name);
    }

    @Data
    @AllArgsConstructor
    static class FieldDescription {
        String identifier;
        String typeName;
        boolean repeated;
    }

    @Data
    @Builder
    static class MessageDescription {
        String identifier = "";
        List<FieldDescription> fields = new ArrayList<>();
        List<MessageDescription> nestedMessages = new ArrayList<>();
    }

    /**
     * Add Message Type to Proto Definition
     *
     * @param messageDescription Custom Message Definition
     */
    public void addMessage(MessageDescription messageDescription) {
        current_desc.addMessageType(createMessageType(messageDescription));
    }

    private DescriptorProtos.DescriptorProto createMessageType(MessageDescription messageDescription) {
        DescriptorProtos.DescriptorProto.Builder msgType = DescriptorProtos.DescriptorProto.newBuilder();
        msgType.setName(messageDescription.identifier);

        for (MessageDescription nestedMessage : messageDescription.nestedMessages) {
            msgType.addNestedType(createMessageType(nestedMessage));
        }

        int fieldIndex = 0;
        for (FieldDescription field : messageDescription.fields) {
            DescriptorProtos.FieldDescriptorProto.Builder fieldBuilder =
                    DescriptorProtos.FieldDescriptorProto.newBuilder();

            fieldBuilder.setName(field.getIdentifier());
            fieldBuilder.setNumber(fieldIndex + 1);

            if (field.repeated) {
                fieldBuilder.setLabel(DescriptorProtos.FieldDescriptorProto.Label.LABEL_REPEATED);
            }

            fieldBuilder.setTypeName(field.getTypeName());
            msgType.addField(fieldBuilder);
            fieldIndex++;
        }

        return msgType.build();
    }

    /**
     * Add Service Call to Proto Definition
     *
     * @param name Service Call Name
     * @param input Input Message Name (Parameters)
     * @param clientStreaming If input is streamed from client to server
     * @param output Output Message Name (Responses)
     * @param serverStreaming If input is streamed from server to client
     */
    public void addCall(String name,
            String input, boolean clientStreaming,
            String output, boolean serverStreaming) {
        // rpc call
        DescriptorProtos.MethodDescriptorProto.Builder method_desc = DescriptorProtos.MethodDescriptorProto.newBuilder();
        method_desc.setName(name);

        method_desc.setInputType(input);
        method_desc.setClientStreaming(clientStreaming);

        method_desc.setOutputType(output);
        method_desc.setServerStreaming(serverStreaming);

        service_desc.addMethod(method_desc);
    }

    public DescriptorProtos.FileDescriptorProto getDescriptor() throws Descriptors.DescriptorValidationException {
        current_desc.addService(service_desc);
        return current_desc.build();
    }

    /**
     * Convert FileDescriptor to Protofile String
     *
     * @Note: This probably doesn't work for all protos, but is tested with
     *        generated protos complying to SiLA.
     *
     * @param fileDescriptor Proto file descriptor generated e.g. with
     *                       {@link DynamicProtoBuilder#getDescriptor()}
     * @return String containing the content of the proto file.
     */
    public static String printProtoFile(@Nonnull Descriptors.FileDescriptor fileDescriptor) {
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("syntax = \"proto3\";\n\n");

        for (Descriptors.FileDescriptor dependency : fileDescriptor.getDependencies()) {
            stringBuilder.append("import \"" + dependency.getName() + "\";\n\n");
        }

        stringBuilder.append("package " + fileDescriptor.getPackage() + ";\n\n");

        /* Write Services */
        for (Descriptors.ServiceDescriptor service : fileDescriptor.getServices()) {
            stringBuilder.append("service " + service.getName() + " {\n");
            for (Descriptors.MethodDescriptor method : service.getMethods()) {
                stringBuilder.append(tab + "rpc " + method.getName() + "(" +
                        (method.toProto().getClientStreaming() ? "stream " : "") +
                        method.getInputType().getFullName() + ") returns (" +
                        (method.toProto().getServerStreaming() ? "stream " : "") +
                        method.getOutputType().getFullName() + ") {}\n"
                );
            }
            stringBuilder.append("}\n\n");
        }

        /* Write Messages */
        for (Descriptors.Descriptor message : fileDescriptor.getMessageTypes()) {
            stringBuilder.append(createMessageDefinition(message, ""));
            stringBuilder.append("\n");
        }

        return stringBuilder.toString();
    }

    /**
     * Private Utility to create Message Definition
     *
     * @param message Message Descriptor
     * @param nestedTab Tab to nest the message
     * @return Message Definition in String format
     */
    private static String createMessageDefinition(Descriptors.Descriptor message, String nestedTab) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(nestedTab + "message " + message.getName() + " {\n");

        for (Descriptors.Descriptor nestedMessage : message.getNestedTypes()) {
            stringBuilder.append(createMessageDefinition(nestedMessage, nestedTab + tab));
        }

        int index = 1; // proto starts at 1
        for (Descriptors.FieldDescriptor field : message.getFields()) {
            final Descriptors.OneofDescriptor oneofDescriptor =
                    field.getContainingOneof();

            String oneOfOpen = "";
            String endingCurl = "";

            if (oneofDescriptor != null) {
                oneOfOpen = "oneof " + oneofDescriptor.getName() + " { ";
                endingCurl = " }";
            }

            final String repeated = field.isRepeated() ? "repeated " : "";
            final String typeName = field.getMessageType().getFullName();

            stringBuilder.append(nestedTab + tab + oneOfOpen + repeated + typeName +
                    " " + field.getName() + " = " + index + ";" + endingCurl + "\n");
            index++;
        }

        stringBuilder.append(nestedTab + "}\n");
        return stringBuilder.toString();
    }
}