package sila_tools.code_generator;

public class GrpcMapper {
    private final static String parameterSuffix = "_Parameters";
    private final static String responseSuffix = "_Responses";

    private final static String stateCommandSuffix = "_State";
    private final static String intermediateCommandSuffix = "_Intermediate";
    private final static String intermediateResponseSuffix = "_IntermediateResponses";
    private final static String resultSuffix = "_Result";

    private final static String staticPropertyPrefix = "Get_";
    private final static String dynamicPropertyPrefix = "Subscribe_";

    private final static String filterPrefix = "Filter_";
    private final static String dataTypePrefix = "DataType_";
    private final static String structSuffix = "_Struct";

    public static String getParameter(final String parameter) {
        return (parameter + GrpcMapper.parameterSuffix);
    }

    public static String getResponse(final String response) {
        return (response + GrpcMapper.responseSuffix);
    }

    public static String getStateCommand(final String stateCommand) {
        return (stateCommand + GrpcMapper.stateCommandSuffix);
    }

    public static String getIntermediateCommand(final String intermediateCommand) {
        return (intermediateCommand + GrpcMapper.intermediateCommandSuffix);
    }

    public static String getIntermediateResponse(final String intermediateResponse) {
        return (intermediateResponse + GrpcMapper.intermediateResponseSuffix);
    }

    public static String getResult(final String result) {
        return (result + GrpcMapper.resultSuffix);
    }

    public static String getStaticProperty(final String staticProperty) {
        return (GrpcMapper.staticPropertyPrefix + staticProperty);
    }

    public static String getDynamicProperty(final String dynamicProperty) {
        return (GrpcMapper.dynamicPropertyPrefix + dynamicProperty);
    }

    public static String getFilter(final String filter) {
        return (GrpcMapper.filterPrefix + filter);
    }

    public static String getDataType(final String dataType) {
        return (GrpcMapper.dataTypePrefix + dataType);
    }

    public static String getStruct(final String struct) {
        return (struct + GrpcMapper.structSuffix);
    }
}
