package sila_tools.code_generator;

import com.google.protobuf.Descriptors;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import javax.xml.transform.stream.StreamSource;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static sila_library.utils.Utils.validateFeatureXML;
import static sila_tools.code_generator.DynamicProtoBuilder.printProtoFile;

@DisplayName("Test SiLACodeGenerator and Utilities")
class SiLACodeGeneratorTest {
    final static String featureName = "TestFeature";
    File featureFile;
    String protoFileContent;

    SiLACodeGeneratorTest() throws URISyntaxException, IOException {
        final String xmlFilePath = "TestFeature.xml";
        final String protoFilePath = "TestFeature.proto";

        featureFile = new File(getURL(xmlFilePath).toURI());
        URL protoURL = getURL(protoFilePath);
        byte[] encoded = Files.readAllBytes(Paths.get(protoURL.toURI()));
        protoFileContent = new String(encoded);
        // Remove carriage return for platform neutrality
        protoFileContent = protoFileContent.replaceAll("\\r", "");
    }

    private URL getURL(String filePath) {
        java.net.URL url = getClass().getClassLoader().getResource(filePath);
        if (url == null) {
            throw new NullPointerException(filePath + " couldn't be found!");
        }
        return url;
    }

    @Test
    public void testXML() throws IOException {
        // Test if XML can be validated
        validateFeatureXML(new StreamSource(this.featureFile));
    }

    @Test
    public void testProto() throws IOException,
            Descriptors.DescriptorValidationException, MalformedSiLAFeature {
        // Create Proto Descriptor and check obvious parameters
        SiLACodeGenerator siLACodeGenerator = new SiLACodeGenerator();
        Descriptors.FileDescriptor protoFileDesc = siLACodeGenerator.getProto(new FileReader(this.featureFile));

        assertTrue(protoFileDesc.getFullName().matches(featureName));
        assertTrue(protoFileDesc.getName().matches(featureName));

        // @Note: It needs to be exactly the same...
        final String genProtoFile = printProtoFile(protoFileDesc);
        assertEquals(protoFileContent, genProtoFile);
    }
}
