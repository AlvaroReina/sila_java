# SiLA Broker
Providing utilities for late binded SiLA calls. Also providing a REST layer for other applications
or debugging purposes.

Note: As this tool is currently only for accessibility and debugging purposes, the error messages
are not very descriptive.

## Installation
First install the module with maven: `mvn clean install`, then you can execute
the JAR with `java -jar target/broker-1.0.0-SNAPSHOT.jar` (depending on the version).

# REST Interface
To check how to use the provided jar, simply execute `java -jar target/broker-1.0.0-SNAPSHOT.jar -h`.
By default, all calls can be accessed through `localhost:8000/v1`

Test the broker for example by starting the `HelloSiLAServer` and using Postman to issue the REST Calls.

The following explanations assume you started the broker with the default configuration.

## Get All Servers
Simply issue a GET Request to `/v1/servers` to retrieve all servers

## Get More Information about specific Server
You will get more Information about the a specific server, its meta info and features, by issuing
a GET Request to `/v1/server/:id`, with the id being the one exposed by the servers call

## Get Property of Server
Properties are retrieved via POST Requests to `/v1/properties` and a JSON Object as body identifying
the property, by example:

```json
{
    "serverId": "HelloSiLAServer",
    "featureId": "HelloSiLA",
    "propertyId": "CurrentYear"
}
```

For dynamic properties, simply use the `/v1/dynamic_properties` endpoint with the same behaviour, 
in the Broker Application it simply returns the first value.

## Call the Command of a Server
Commands are called via POST Requests to `/v1/calls` and a JSON Object as body identifying the command.
The parameters itself are expected to be a JSON body that can be serialised into the correct protobuf message
for the SiLA Parameters, according to the 
[Proto 3 JSON specification](https://developers.google.com/protocol-buffers/docs/proto3#json).

HelloSiLA Example Body:

```json
{
    "serverId": "HelloSiLAServer",
    "featureId": "HelloSiLA",
    "commandId": "SayHello",
    "parameters": {"Name": {"value": "SiLA"}}
}
```

For observable commands, simply use the `/v1/observable_calls` endpoint with the same behaviour,
in the Broker Application it simply blocks until the command is finished.