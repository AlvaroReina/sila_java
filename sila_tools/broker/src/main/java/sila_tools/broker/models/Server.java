package sila_tools.broker.models;

import sila_features.SiLAServiceServer;
import sila_library.models.Feature;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Server Model for SiLA Servers
 */
public class Server {
    public String identifier = null;
    public String host = null;
    public Integer port = null;
    public SiLAServiceServer.ServerInfo info = null;
    public Date joined = null;
    public List<Feature> features = new ArrayList<>();
    public List<String> errorMessages = new ArrayList<>();
}
