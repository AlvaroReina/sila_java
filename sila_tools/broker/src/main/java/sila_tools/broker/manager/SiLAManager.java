package sila_tools.broker.manager;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import sila_library.discovery.SiLAServerDiscovery;
import sila_tools.broker.models.Server;

import javax.annotation.Nonnull;

import java.security.KeyException;
import java.util.*;

/**
 * Manager to use from the BrokerApplication Level to access SiLA Servers
 */
@Slf4j
public class SiLAManager {
    private final ServerListener serverListener = new ServerListener();

    /**
     * Constructor, if empty list then listens on all interfaces
     * @param discoveryEnabled Enable Discovery or add Servers Manually
     */
    public SiLAManager(boolean discoveryEnabled) {
        if (discoveryEnabled) {
            SiLAServerDiscovery.enable();
            SiLAServerDiscovery.addListener(serverListener);
        }
    }

    /**
     * Get all Server Identifiers discovered or added
     */
    public Set<String> getServers() {
        return serverListener.getServerNames();
    }

    /**
     * Get One Specific Server Representation
     * @param id SiLA Identifiers retrieved with {@link #getServers()}
     */
    public Server getServer(String id) {
        return serverListener.getServer(id);
    }

    /**
     * Adding Server manually
     *
     * @param id Server Id used for referencing to the Server
     */
    public void addServer(String id, String host, int port) {
        serverListener.serverAdded(id, host, port);
    }

    /**
     * Removing Server manually
     *
     * @param id Server Id used for referencing to the Server
     */
    public void removeServer(String id) {
        serverListener.serverRemoved(id);
    }

    public void addSiLAServerListener(@Nonnull ServerListener.SiLAServerListener siLAServerListener) {
        serverListener.addSiLAServerListener(siLAServerListener);
    }

    public void removeSiLAServerListener(@Nonnull
                                                 ServerListener.SiLAServerListener siLAServerListener) {
        serverListener.removeSiLAServerListener(siLAServerListener);
    }

    /**
     * Getting SiLA Call Executor from a SiLA Call defining it
     */
    public ServerCallExecutor newCallExecutor(@NonNull SiLACall siLACall) throws KeyException {
        final SiLAConnection connection = this.serverListener.getSiLAConnection(siLACall.getServerId());
        return new ServerCallExecutor(connection, siLACall);
    }
}
