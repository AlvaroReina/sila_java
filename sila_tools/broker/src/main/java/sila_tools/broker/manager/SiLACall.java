package sila_tools.broker.manager;

import lombok.*;

/**
 * Represents a generic SiLA Call
 */
@Getter
@RequiredArgsConstructor
public final class SiLACall {
    @NonNull
    private final String serverId;
    @NonNull
    private final String featureId;
    @NonNull
    private final String callId;
    @NonNull
    private final Type type;
    @NonNull
    private final String parameters;

    @Getter
    public enum Type {
        NON_OBSERVABLE_COMMAND,
        OBSERVABLE_COMMAND,
        STATIC_PROPERTY,
        DYNAMIC_PROPERTY;
    }

    public SiLACall(
            @NonNull final String serverId,
            @NonNull final String featureId,
            @NonNull final String callId,
            @NonNull final Type type) {
        this(serverId, featureId, callId, type, "{}");
    }
}
