package sila_tools.broker.manager;

import com.google.protobuf.Descriptors;
import com.google.protobuf.DynamicMessage;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.util.JsonFormat;
import io.grpc.*;
import io.grpc.stub.ClientCalls;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.SiLAFramework;
import sila_tools.broker.dynamic_grpc.DynamicMessageMarshaller;
import sila_tools.code_generator.GrpcMapper;

import javax.annotation.Nullable;
import java.security.KeyException;
import java.util.List;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Executor to handle all possible SiLA Calls
 */
@Slf4j
public class ServerCallExecutor {
    private static final int MAX_WAIT_TIME = 1;
    private static final TimeUnit MAX_WAIT_TIME_UNIT = TimeUnit.MINUTES;
    private final SiLAConnection connection;
    private final SiLACall baseCall;
    private final Descriptors.ServiceDescriptor feature;

    public ServerCallExecutor(
            @NonNull final SiLAConnection connection,
            @NonNull final SiLACall baseCall
    ) throws KeyException {
        this.connection = connection;
        this.baseCall = baseCall;
        this.feature = connection.getFeatureService(baseCall.getFeatureId());
    }

    public String execute() {
        String result = "";
        switch (this.baseCall.getType()) {
        case NON_OBSERVABLE_COMMAND:
            result = executeNonObservableCommand();
            break;
        case OBSERVABLE_COMMAND:
            result = executeObservableCommand();
            break;
        case STATIC_PROPERTY:
            result = getStaticProperty();
            break;
        case DYNAMIC_PROPERTY:
            result = getDynamicProperty();
            break;
        }

        return result;
    }

    /**
     * Specific Exception for this Executor
     */
    public static class Exception extends RuntimeException {
        public Exception(String message) {
            super(message);
        }

        public Exception(String message, Throwable cause) {
            super(message, cause);
        }

        public Exception(Throwable cause) {
            super(cause);
        }
    }

    private String executeNonObservableCommand() {
        return (this.executeCall(this.baseCall.getCallId(), this.baseCall.getParameters()));
    }

    private String executeObservableCommand() {
        try {
            final SiLAFramework.CommandConfirmation.Builder command = SiLAFramework.CommandConfirmation.newBuilder();
            JsonFormat.parser().merge(this.executeCall(this.baseCall.getCallId(), this.baseCall.getParameters()), command);
            final String commandId = JsonFormat.printer().print(command.getCommandId());
            final AtomicInteger commandStatus = new AtomicInteger(SiLAFramework.ExecutionState.CommandStatus.waiting_VALUE);
            final CompletableFuture<List<String>> future = this.executeStream(
                    GrpcMapper.getStateCommand(this.baseCall.getCallId()),
                    commandId,
                    message -> {
                        try {
                            final SiLAFramework.ExecutionState.Builder stateBuilder = SiLAFramework.ExecutionState.newBuilder();
                            JsonFormat.parser().merge(message, stateBuilder);
                            log.info("Received status for call " + this.baseCall.toString());
                            log.info(stateBuilder.toString());

                            commandStatus.set(stateBuilder.getCommandStatus().getNumber());

                            return (
                                    commandStatus.get() == SiLAFramework.ExecutionState.CommandStatus.running_VALUE ||
                                    commandStatus.get() == SiLAFramework.ExecutionState.CommandStatus.waiting_VALUE
                            );

                        } catch (final InvalidProtocolBufferException e) {
                            log.warn("Received a malformed message: ", e);
                            return (false);
                        }
                    }
            );
            try {
                future.get(); // ignoring results
            } catch (CompletionException | InterruptedException | ExecutionException e) {
                if (e.getCause() != null && e.getCause() instanceof StatusRuntimeException) {
                    throw (StatusRuntimeException)e.getCause();
                }
                throw new ServerCallExecutor.Exception(e);
            }

            // @TODO: Retrieve Proper Observable Error (will be done with implementation to next specification)
            if (commandStatus.get() == SiLAFramework.ExecutionState.CommandStatus.finishedSuccessfully_VALUE) {
                return (this.executeCall(GrpcMapper.getResult(this.baseCall.getCallId()), commandId));
            } else {
                throw new ServerCallExecutor.Exception("Command state ended with an invalid state: " + commandStatus.get());
            }
        } catch (InvalidProtocolBufferException e) {
            throw new ServerCallExecutor.Exception("Received a malformed message", e.getCause());
        }
    }

    private String getStaticProperty() {
        return (this.executeCall(GrpcMapper.getStaticProperty(this.baseCall.getCallId()), this.baseCall.getParameters()));
    }

    private String getDynamicProperty() {
        CompletableFuture<List<String>> future = this.executeStream(
                GrpcMapper.getDynamicProperty(this.baseCall.getCallId()),
                this.baseCall.getParameters(),
                message -> false);
        try {
            List<String> results = future.get();
            if (results.isEmpty()) {
                throw new RuntimeException("No result");
            } else {
                return (results.get(results.size() - 1));
            }
        } catch (CompletionException | InterruptedException | ExecutionException e) {
            if (e.getCause() != null && e.getCause() instanceof StatusRuntimeException) {
                throw (StatusRuntimeException)e.getCause();
            }
            throw new ServerCallExecutor.Exception(e);
        }
    }

    /**
     * Simple Unary gRPC Call with given Service Id and parameters
     * @param callId Service Name provided in the proto definition
     * @param params Protobuf Parameters in JSON Format
     * @return Protobuf Response in JSON Format
     */
    private String executeCall(@NonNull String callId, @NonNull String params) {
        final Descriptors.MethodDescriptor method = this.feature.findMethodByName(callId);
        if (method == null)
            throw new ServerCallExecutor.Exception("Server " + this.baseCall.getServerId() + " doesn't expose call to " + callId);
        final DynamicMessage request = ServerCallExecutor.getRequestMessage(method, params);
        final MethodDescriptor<Object, Object> methodDescriptor = getMethodDescriptor(method);
        final DynamicMessage unaryCall = (DynamicMessage) ClientCalls.blockingUnaryCall(
                connection.getManagedChannel().newCall(
                        methodDescriptor,
                        CallOptions.DEFAULT.withDeadlineAfter(MAX_WAIT_TIME, MAX_WAIT_TIME_UNIT)
                ),
                request
        );

        // Compose Results
        final String results;
        try {
            results = JsonFormat.printer().print(unaryCall);
        } catch (InvalidProtocolBufferException e) {
            throw new IllegalArgumentException(e.getMessage());
        }

        return results;
    }

    /**
     * Calling Server Side Streaming
     * @param callId Service Name provided in the proto definition
     * @param params Protobuf Parameters in JSON Format
     * @param callback Callback to consume Streaming messages
     * @return Future to get a list of JSON Responses
     */
    private CompletableFuture<List<String>> executeStream(
            @NonNull String callId,
            @NonNull String params,
            @Nullable StaticStreamObserver.StreamCallback callback
    ) {
        final Descriptors.MethodDescriptor method = this.feature.findMethodByName(callId);
        if (method == null)
            throw new ServerCallExecutor.Exception("Server " + this.baseCall.getServerId() + " doesn't expose call to " + callId);

        final DynamicMessage request = ServerCallExecutor.getRequestMessage(method, params);
        final MethodDescriptor<Object, Object> methodDescriptor = getMethodDescriptor(method);
        final ClientCall<Object, Object> clientCall = this.connection.getManagedChannel().newCall(methodDescriptor, CallOptions.DEFAULT);

        final StaticStreamObserver propertyObserver = new StaticStreamObserver(clientCall, callback);

        ClientCalls.asyncServerStreamingCall(clientCall, request, propertyObserver);
        return (propertyObserver.getFuture());
    }

    // Private Helpers to retrieve dynamic protobuf constructs
    private static MethodDescriptor<Object, Object> getMethodDescriptor(@NonNull Descriptors.MethodDescriptor method) {
        return (MethodDescriptor
                .newBuilder()
                .setType(MethodDescriptor.MethodType.UNARY)
                .setFullMethodName(getFullMethodName(method))
                .setRequestMarshaller(new DynamicMessageMarshaller(method.getInputType()))
                .setResponseMarshaller(new DynamicMessageMarshaller(method.getOutputType()))
                .build()
        );
    }

    private static String getFullMethodName(@NonNull Descriptors.MethodDescriptor method) {
        return (MethodDescriptor.generateFullMethodName(
                method.getService().getFullName(),
                method.getName())
        );
    }

    private static DynamicMessage getRequestMessage(
            @NonNull Descriptors.MethodDescriptor method,
            @NonNull String params
    ) {
        final DynamicMessage.Builder parBuilder = DynamicMessage.newBuilder(method.getInputType());

        try {
            JsonFormat.parser().merge(params, parBuilder);
        } catch (InvalidProtocolBufferException e) {
            throw new IllegalArgumentException(e.getMessage());
        }

        return (parBuilder.build());
    }
}
