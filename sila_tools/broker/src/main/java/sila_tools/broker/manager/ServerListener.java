package sila_tools.broker.manager;

import com.google.protobuf.Descriptors;
import io.grpc.ManagedChannelBuilder;
import io.grpc.StatusRuntimeException;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.apache.commons.io.input.CharSequenceReader;
import sila2.org.silastandard.core.silaservice.v1.SiLAServiceGrpc;
import sila2.org.silastandard.core.silaservice.v1.SiLAServiceOuterClass;
import sila_features.SiLAServiceServer;
import sila_library.models.Feature;
import sila_tools.broker.models.Server;
import sila_tools.code_generator.SiLACodeGenerator;

import javax.annotation.Nonnull;
import java.security.KeyException;
import java.util.*;
import java.util.concurrent.TimeUnit;

import static sila_library.utils.FeatureGenerator.generateFeature;

/**
 * Listener attaching to SiLA Discovery and Managing SiLA Server Life Cycle
 *
 * @implNote This listener currently can not handle several SiLA Servers with the same discovery name!
 */
@Slf4j
public class ServerListener implements sila_library.discovery.listener.ServerListener {
    public interface SiLAServerListener {
        void onSiLAServerAdded(String serverName, Server server);
        void onSiLAServerRemoved(String serverName);
    }

    private final static long MAX_SERVICE_TIMEOUT = 10; // [s]

    private final Map<String, SiLAConnection> silaConnections = new HashMap<>();
    private final Map<String, Server> silaServers = new HashMap<>();
    private final List<SiLAServerListener> siLAServerListenerList = new ArrayList<>();

    /**
     * Get Server Information based on Discovery Name
     * @param id Discovery Name as ID
     * @return null if doesn't exist, server info if exists
     */
    Server getServer(String id) {
        return silaServers.get(id);
    }

    Set<String> getServerNames() {
        return silaServers.keySet();
    }

    /**
     * Get SiLA Connection based on Discovery Name
     * @param serverId Discovery Name as ID
     * @return SiLA Server wrapping server and gRPC variables
     */
    SiLAConnection getSiLAConnection(String serverId) throws KeyException {
        if (!this.silaConnections.containsKey(serverId)) {
            throw new KeyException(serverId + " not registered on ServerListener!");
        }
        return this.silaConnections.get(serverId);
    }

    /**
     * Add an additional listener to retrieve SiLA Server information in-process
     */
    public void addSiLAServerListener(@Nonnull SiLAServerListener siLAServerListener) {
        siLAServerListenerList.add(siLAServerListener);
    }

    public void removeSiLAServerListener(@Nonnull SiLAServerListener siLAServerListener) {
        siLAServerListenerList.remove(siLAServerListener);
    }

    @Override
    public synchronized void serverAdded(String serverName, String host, int port) {
        log.info("[serverAdded] Found SiLA Server {} on {}:{}", serverName, host, port);

        /* SiLA Server */
        final SiLAConnection siLAConnection = new SiLAConnection(
                ManagedChannelBuilder
                        .forAddress(host, port)
                        .usePlaintext(true)
                        .build()
        );

        final Server server = new Server();
        server.identifier = serverName;
        server.joined = new Date();
        server.host = host;
        server.port = port;

        loadServer(serverName, server, siLAConnection);

        // Add Server and notify when successful resolving
        silaConnections.put(serverName, siLAConnection);
        silaServers.put(serverName, server);
        for (val silaServerListener : siLAServerListenerList) {
            silaServerListener.onSiLAServerAdded(serverName, server);
        }

        log.info(
                "[serverAdded] Broker resolved SiLA Server serverName={} on {}:{}",
                serverName,
                host,
                port
        );
    }

    @Override
    public synchronized void serverRemoved(String serverName) {
        log.info("[serverRemoved] remove serverName={} ...", serverName);
        if (silaConnections.containsKey(serverName)) {
            silaConnections.get(serverName).close();
            silaConnections.remove(serverName);
        }

        if (silaServers.containsKey(serverName)) {
            silaServers.remove(serverName);
        }

        log.info("[serverRemoved] removed serverName={} !", serverName);
        // also update listeners
        for (val silaServerListener : siLAServerListenerList) {
            silaServerListener.onSiLAServerRemoved(serverName);
        }
    }

    /**
     * Load features from SiLAService and initialize the siLAServer and restServer.
     *
     * @param serverName Server Name advertised on Network
     * @param server Server to provide SiLA Server information
     * @param siLAConnection Connection to call the SiLA Server
     */
    private void loadServer(String serverName,
            Server server,
            SiLAConnection siLAConnection) {
        log.info("{} Calling SiLAService", serverName);

        // Call SiLAService
        SiLAServiceGrpc.SiLAServiceBlockingStub siLAService = SiLAServiceGrpc
                .newBlockingStub(siLAConnection.getManagedChannel());

        try {
            // Load Server Info
            final String serverDisplayName = siLAService
                    .withDeadlineAfter(ServerListener.MAX_SERVICE_TIMEOUT, TimeUnit.SECONDS)
                    .getServerDisplayName(
                            SiLAServiceOuterClass.Get_ServerDisplayName_Parameters
                                    .newBuilder()
                                    .build())
                    .getServerDisplayName()
                    .getValue();
            log.info("{} Got serverDisplayName: {}", serverName, serverDisplayName);

            final String serverDescription = siLAService
                    .withDeadlineAfter(ServerListener.MAX_SERVICE_TIMEOUT, TimeUnit.SECONDS)
                    .getServerDescription(
                            SiLAServiceOuterClass.Get_ServerDescription_Parameters
                                    .newBuilder()
                                    .build())
                    .getServerDescription()
                    .getValue();
            log.info("{} Got serverDescription: {}",serverName, serverDescription);

            final String serverVendorURL = siLAService
                    .withDeadlineAfter(ServerListener.MAX_SERVICE_TIMEOUT, TimeUnit.SECONDS)
                    .getServerVendorURL(
                            SiLAServiceOuterClass.Get_ServerVendorURL_Parameters
                                    .newBuilder()
                                    .build())
                    .getServerVendorURL()
                    .getURL()
                    .getValue();
            log.info("{} Got serverVendorURL: {}", serverName, serverVendorURL);

            server.info = new SiLAServiceServer.ServerInfo(
                            serverDisplayName,
                            serverDescription,
                            serverVendorURL
            );
            log.info("{} Saved Server Information", serverName);

            // Get FeatureList
            final long start = System.currentTimeMillis();
            final SiLAServiceOuterClass.Get_ImplementedFeatures_Responses featureList = siLAService
                    .withDeadlineAfter(ServerListener.MAX_SERVICE_TIMEOUT, TimeUnit.SECONDS)
                    .getImplementedFeatures(
                            SiLAServiceOuterClass.Get_ImplementedFeatures_Parameters
                                    .newBuilder()
                                    .build()
                    );
            log.info(
                    "{} Got List of Features in {} ms", serverName,
                    (System.currentTimeMillis() - start)
            );

            // Compile single Features
            loadFeatures(siLAConnection, siLAService, server, featureList);
        } catch (StatusRuntimeException e) {
            final String errorMessage = String.format(
                    "{} SiLA Service of %s doesn't return because %s. Timeout was set to %d s",
                    serverName,
                    e.getMessage(),
                    ServerListener.MAX_SERVICE_TIMEOUT
            );
            log.warn(errorMessage);
            server.errorMessages.add(errorMessage);
        }
    }

    private void loadFeatures(final SiLAConnection siLAConnection,
            final SiLAServiceGrpc.SiLAServiceBlockingStub siLAService,
            final Server server,
            final SiLAServiceOuterClass.Get_ImplementedFeatures_Responses featureList) {
        for (SiLAServiceOuterClass.DataType_Identifier featureIdentifier : featureList
                .getImplementedFeaturesList()) {
            final String featureId = getFeatureId(featureIdentifier);

            final SiLAServiceOuterClass.GetFeatureDefinition_Parameters par =
                    SiLAServiceOuterClass.GetFeatureDefinition_Parameters.newBuilder()
                            .setQualifiedFeatureIdentifier(featureIdentifier)
                            .build();

            final SiLAServiceOuterClass.GetFeatureDefinition_Responses featureDefinition = siLAService
                    .withDeadlineAfter(ServerListener.MAX_SERVICE_TIMEOUT, TimeUnit.SECONDS)
                    .getFeatureDefinition(par);

            final String rawFeatureDefinition = featureDefinition
                    .getFeatureDefinition()
                    .getFeatureDefinition()
                    .getValue();
            try {
                // Deserialize into feature
                final Feature featurePojo = generateFeature(
                        new CharSequenceReader(
                                rawFeatureDefinition
                        )
                );

                // Save Feature for late binding
                final SiLACodeGenerator siLACodeGenerator = new SiLACodeGenerator();
                final Descriptors.FileDescriptor protoFile = siLACodeGenerator
                        .getProto(featurePojo);
                siLAConnection.addFeatureService(featureId, protoFile.getServices().get(0));

                log.info("{} Feature {}=", featureIdentifier);
                log.info("{} {}", featurePojo);

                server.features.add(featurePojo);
            } catch (Exception e) {
                final String errorMessage = String.format(
                        "Parsing of Feature %s failed. Reason=%s",
                        featureIdentifier,
                        e.getMessage()
                );

                log.warn(errorMessage);
                server.errorMessages.add(errorMessage);
            }
        }
    }

    /**
     * This is a temporary hack to retrieve the Feature Identifier correctly, even if it's fully
     * qualified (delimited by '/'
     *
     * @param featureIdentifier The full Feature Identifier
     * @return the subset of the feature identifier useful for gRPC Calls
     */
    private static String getFeatureId(final SiLAServiceOuterClass.DataType_Identifier featureIdentifier) {
        final String featureId = featureIdentifier.getIdentifier().getValue();
        final int lastIndexOf = featureId.lastIndexOf('/');
        if (lastIndexOf > 0 && lastIndexOf != featureId.length() - 1) {  // If fully qualified feature, convert to simple feature id
            return (featureId.substring(lastIndexOf + 1));
        }
        return (featureId);
    }
}
