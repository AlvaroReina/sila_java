package sila_tools.broker.manager;

import com.google.protobuf.Descriptors;
import io.grpc.ManagedChannel;
import lombok.Getter;

import java.io.Closeable;
import java.security.KeyException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * Represents the properties of a SiLA Connection: a gRPC Channel and gRPC Services
 */
public class SiLAConnection implements Closeable {
    private final Map<String, Descriptors.ServiceDescriptor> featureMap = new HashMap<>();

    @Getter
    private final ManagedChannel managedChannel;

    public SiLAConnection(ManagedChannel managedChannel) {
        this.managedChannel = managedChannel;

        Runtime.getRuntime().addShutdownHook(
                new Thread(this::close)
        );
    }

    public Descriptors.ServiceDescriptor getFeatureService(String featureId) throws KeyException {
        if (!featureMap.containsKey(featureId)) {
            throw new KeyException("Can\'t find expose Feature" + featureId);
        } else {
            return featureMap.get(featureId);
        }
    }

    public void addFeatureService(String featureId, Descriptors.ServiceDescriptor serviceDescriptor) {
        this.featureMap.put(featureId, serviceDescriptor);
    }

    @Override
    public void close() {
        if (managedChannel != null) {
            try {
                managedChannel.shutdown().awaitTermination(5, TimeUnit.SECONDS);
            } catch (InterruptedException e) {
                System.out.println(e.getMessage());
            }
        }
    }
}
