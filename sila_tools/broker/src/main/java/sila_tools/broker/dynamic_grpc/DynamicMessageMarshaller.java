package sila_tools.broker.dynamic_grpc;

import com.google.protobuf.Descriptors;
import com.google.protobuf.DynamicMessage;
import com.google.protobuf.ExtensionRegistryLite;
import io.grpc.MethodDescriptor;

import java.io.IOException;
import java.io.InputStream;

/** A Marshaller for dynamic messages. */
public class DynamicMessageMarshaller implements MethodDescriptor.Marshaller<Object> {
    private final Descriptors.Descriptor messageDescriptor;

    public DynamicMessageMarshaller(Descriptors.Descriptor messageDescriptor) {
        this.messageDescriptor = messageDescriptor;
    }

    @Override
    public InputStream stream(Object o) {
        if (!(o instanceof DynamicMessage))
            throw new IllegalArgumentException("Expected " + DynamicMessage.class.getName() + " but got " + o.getClass().getName());
        return (this.stream((DynamicMessage) o));
    }

    @Override
    public DynamicMessage parse(InputStream inputStream) {
        try {
            return DynamicMessage.newBuilder(messageDescriptor)
                    .mergeFrom(inputStream, ExtensionRegistryLite.getEmptyRegistry())
                    .build();
        } catch (IOException e) {
            throw new RuntimeException("Unable to merge from the supplied input stream", e);
        }
    }

    private InputStream stream(DynamicMessage abstractMessage) {
        return abstractMessage.toByteString().newInput();
    }
}
