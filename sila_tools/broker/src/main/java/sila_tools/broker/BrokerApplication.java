package sila_tools.broker;

import io.javalin.event.EventType;
import lombok.extern.slf4j.Slf4j;
import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.ArgumentParserException;
import net.sourceforge.argparse4j.inf.Namespace;
import org.apache.commons.configuration2.Configuration;
import org.apache.commons.configuration2.ex.ConfigurationException;
import sila_tools.broker.manager.SiLAManager;
import io.javalin.Javalin;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static sila_library.utils.Utils.getLocalConfiguration;

/**
 * Main Entry Point for Broker
 */
@Slf4j
public class BrokerApplication {
    private Javalin server;

    public static void main(String[] args) throws ConfigurationException {
        final String propFileName = "broker.properties";
        final Configuration config = getLocalConfiguration(propFileName);

        // Argument Parser
        ArgumentParser parser = ArgumentParsers.newFor("broker").build()
                .defaultHelp(true)
                .description("Broker service to register SiLA Servers "
                        + "and serve REST about them");
        parser.addArgument("-p", "--port")
                .type(Integer.class)
                .setDefault(config.getInt("restPort"))
                .help("Specify port to use.");
        parser.addArgument("-n", "--network_interface")
                .type(String.class)
                .help("Specify internet interface. Check ifconfig (LNX & MAC)"
                        + " and for windows, ask us for a tiny java app @ www.unitelabs.ch .");
        parser.addArgument("-a", "--api-path")
                .type(String.class)
                .setDefault(config.getString("apiPrefix"))
                .help("Specify desired API Path. ex.: /v1");

        Namespace ns = null;
        try {
            ns = parser.parseArgs(args);
        } catch (ArgumentParserException e) {
            parser.handleError(e);
            System.exit(1);
        }

        // Comma delimited network interfaces
        final String networkInterfaces = ns.getString("network_interface");
        List<String> networkInterfaceList = new ArrayList<>();

        if (networkInterfaces != null) {
            networkInterfaceList = Arrays.asList(networkInterfaces.split(","));
        }

        // Start Application
        final BrokerApplication app = new BrokerApplication();
        app.start(
                new SiLAManager(true),
                ns.getInt("port"),
                ns.getString("api_path")
        );
    }

    /**
     * Start REST Server exposing SiLA calls
     *
     * @param manager Manager to call servers from
     * @param port for HTTP Server
     * @param apiPrefix Prefix for API calls
     */
    private void start(SiLAManager manager, int port, String apiPrefix) {
        final SiLAActions siLAActions = new SiLAActions(manager, apiPrefix);

        // Create HTTP Server
        this.server = Javalin.create()
                // Listen to successful start
                .event(EventType.SERVER_STARTED, e -> {
                    log.info("SiLA Broker started.");
                })
                .event(EventType.SERVER_START_FAILED, e -> {
                    log.error("Start of HTTP Server failed");
                    throw new RuntimeException("Could not start HTTP Server, aborting...");
                });

        // Graceful aborting
        Runtime.getRuntime().addShutdownHook(
                new Thread(this::stop)
        );

        // Enable CORS to allow application port
        server
                .enableCorsForAllOrigins()
                .port(port)
                .enableDynamicGzip()
                .start()
                // routes
                .get(apiPrefix + "/servers", siLAActions.getServers)
                .get(apiPrefix + "/servers/:id", siLAActions.getServer)
                .post(apiPrefix + "/properties", siLAActions.getStaticProperty)
                .post(apiPrefix + "/dynamic_properties", siLAActions.getDynamicProperty)
                .post(apiPrefix + "/calls", siLAActions.executeNonObservableCommand)
                .post(apiPrefix + "/observable_calls", siLAActions.executeObservableCommand);
    }

    private void stop() {
        if(server != null)
            server.stop();
    }
}
