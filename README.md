**Important**: This code, in its current form, is mainly to give active SiLA 2 WG Members 
and other interested parties a reference point. It might not comply with the latest
version of the Standard, and much of its content might change in the future.

The release of SiLA 2 is scheduled for end 2018.

For more general information about the standard, we refer to the 
[sila_base](https://gitlab.com/SiLA2/sila_base) repository.

# SiLA Java
SiLA base repository consisting reference implementations, libraries and generators 
to drive adoption of the standard.

As an entry point, refer to `sila_implementations/hello_sila`.

## Cloning
You must clone the repo with submodules:
```bash
git clone --recurse-submodules git@gitlab.com:SiLA2/sila_java.git
```
Initialize the yor local configuration file:
```bash
git submodule update --init --recursive
```
Fetch all the data from the project and check out the appropriate commit listed.
```bash
git submodule update --recursive
```

## License
This code is licensed under the [MIT License](https://en.wikipedia.org/wiki/MIT_License)

## Maintainer
This repository is maintained by [UniteLabs AG](http://www.unitelabs.ch), contact
Maximilian Schulz ([max@unitelabs.ch](mailto:max@unitelabs.ch)).

## Components
### sila_base
Maven wrapper around the `sila_base` repository, to allow the use of the base definitions in all
packages.

### sila_features
Definitions of standardised SiLA Features and reference implementations.

### sila_implementations
SiLA Servers conforming with the SiLA standard.

### sila_library
The SiLA Library provides base classes for implementing SiLA Servers and SiLA Clients according to Part A+B of the SiLA 2 Specification (incl. SiLA discovery).

### sila_tools
Tools such as the code generator to facilitate implementation.

## Installation
The code has been tested with JRE 1.8.0 / Oracle JDK 8. 

You can simply install all modules by invoking `mvn clean install` in the 
top directory. You can then test the entry package in `sila_implementations/hello_sila`.

If you want to install without running the tests every time, simply 
use `mvn clean install -DskipTests`.

### Purge Repositories
Sometimes there can be artifacts in your local maven repository that make the build irreproducible,
to purge it while installing, simply use the sila-purge-repo profile:

```bash
mvn clean install -P sila-purge-repo
```


### Linux
You can check if Orace JDK is provided by aptitude by tipping:

`apt-cache search oracle-java`

If it is not provided, add the ppa:

`sudo add-apt-repository ppa:webupd8team/java`

`sudo apt-get update`

Then installation becomes trivial:

`sudo apt-get install oracle-java8-jdk` or `sudo apt-get install oracle-java8-installer`

To learn more about Java in linux (ubuntu) follow these instructions [link](https://www.digitalocean.com/community/tutorials/how-to-install-java-with-apt-get-on-ubuntu-16-04)

To install Maven, follow the instructions on
[link](https://maven.apache.org/install.html).

### Mac OS
To clean installved maven projects:
`rm -rf ~/.m2/repository/org/sila-standard/*`

### Windows
Download and install:
- JDK [jdk1.8.0_144](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
- [Maven](https://maven.apache.org/install.html)

Note: 
* Do not foget to set JAVA_HOME and update the PATH variable to include the bin 
directory of Maven

## Continuous Integration
You can test the "GitLab runner" locally, installation instructions are
[here](https://docs.gitlab.com/runner/install/). 

To use the runner in the same environment as the CI server, you need to install 
docker (e.g. for 
[mac OS](https://docs.docker.com/docker-for-mac/install/) or 
[Windows](https://docs.docker.com/docker-for-windows/install/)).

To run the test steps, use:
```
gitlab-runner exec docker .validate
gitlab-runner exec docker .verify
```

This is especially useful if the CI is failing on your commit or you want to test
the build before setting up a Merge Request. (Tip: Use 
`gitlab-runner exec shell .validate` for quicker debugging, if the same problems
occur.)

Note that it only tests the current GIT commit you are on, not any uncommited changes.

## Development Notes
### Style Guide
The style guide for this repository is currently being worked on. Special notes:

- We aim to use the `lombok` library for all POJOs for better readability.

### XML Schema
On IntelliJ we haven't found a way to properly validate the XML FDL Schema while editing.
The schema is currently hosted on the [sila_base](https://gitlab.com/SiLA2/sila_base)
repository. The solution for now is to simply fetch the XSD locally and assign
the appropriate namespace in IntelliJ IDEA -> Preferences... -> Schemas and DTD.

### Maven Project Structure
In case you change the directory structure of the Maven projects, 
you also need to change the `pom.xml`, maven doesn't allow absolute
parent paths [link](https://stackoverflow.com/questions/36134651/maven-how-set-absolute-path-for-parent-pom).