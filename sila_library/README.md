# SiLA Library
Components for easy use of SiLA principles.

## SiLA Discovery
Components which monitor the network by using discovery to look for 
other SiLA servers.

### Known Issues
Discovery currently only works properly under certain conditions.

*macOS*
Currently, the discovery is rather unreliable on macOS, one needs to turn off any other network interfaces other than the one used for the local network of interest to be certain that the discovery mechanism will work.

*Windows*
Potential problems coming up: 
[link](slightfuture.com/technote/windows-mdns-dnssd).

## Implementation Notes
The feature models are generated from the `FeatureDefinition.xsd` located on 
[Gitlab](https://gitlab.com/SiLA2/sila_base/raw/master/schema/FeatureDefinition.xsd) using
the `xjc` tool from `JAXB` with additional bindings defined in resources.