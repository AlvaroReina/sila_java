package sila_library.discovery;

import com.google.common.net.HostAndPort;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import jdk.nashorn.internal.runtime.regexp.joni.exception.ValueException;
import lombok.extern.slf4j.Slf4j;
import sila_library.discovery.listener.ServerListener;
import sila_library.discovery.listener.SimpleListener;
import sila_library.discovery.networking.service_discovery.Discovery;

import java.security.KeyException;
import java.time.Duration;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import static sila_library.discovery.SiLADiscovery.SILA_MDNS_DOMAIN;
import static sila_library.discovery.SiLADiscovery.SILA_MDNS_TYPE;
import static sila_library.utils.MethodPoller.await;

/**
 * Discover SiLA Servers in local network.
 *
 * @implNote There only exists one discovery instance for the whole application
 * @implNote IPv6 Networks still have to be tested properly, currently only IPv4 supported
 */
@Slf4j
public class SiLAServerDiscovery {
    private static final int MAX_FINDING_TIMEOUT = Integer.MAX_VALUE;
    private static final int QUERY_RATE = 1000; // [ms]

    private static Discovery discovery = null;

    // Pure Static Class
    private SiLAServerDiscovery(){
        throw new AssertionError();
    }

    /**
     * Enables discovery on all network interfaces
     */
    public static synchronized void enable() {
        if (discovery != null) {
            return;
        }

        discovery = new Discovery(SILA_MDNS_TYPE, SILA_MDNS_DOMAIN, QUERY_RATE);
        addListener(SimpleListener.getInstance());
    }

    /**
     * Add custom {@link ServerListener}
     *
     * @param listener the listener to add
     */
    public static void addListener(final ServerListener listener) {
        if (null != listener) {
            discovery.addListener(listener);
        }
    }

    /**
     * Remove custom {@link ServerListener}
     *
     * @param listener the listener to remove
     */
    public static void removeListener(final ServerListener listener) {
        if (null != listener) {
            discovery.removeListener(listener);
        }
    }

    /**
     * Blocking call until server is found via SiLA discovery
     * (uses default listener {@link SimpleListener})
     *
     * @param serverName SiLA Server name
     */
    public static void blockUntilServerFound(String serverName) {
        try {
            blockUntilServerFound(serverName, MAX_FINDING_TIMEOUT);
        } catch (TimeoutException e) {
            log.error("[blockUntilServerFound] {}", e.getMessage());
            //such a long timeout should never occur but if just wrap the exception
            throw new RuntimeException(e);
        }
    }

    /**
     * Blocking call until server is found via SiLA discovery
     * (uses default listener {@link SimpleListener})
     *
     * @param serverName SiLA Server name
     * @param timeOut    in ms
     * @throws TimeoutException when the server could not be found within the timeOut period
     */
    public static void blockUntilServerFound(String serverName, int timeOut)
            throws TimeoutException {
        try {
            await()
                    .atMost(Duration.ofMillis(timeOut))
                    .until(() -> SimpleListener.getInstance().onlineServers.containsKey(serverName))
                    .execute();
        } catch (ExecutionException e) {
            throw new RuntimeException(e.getCause());
        } catch (TimeoutException e) {
            throw new TimeoutException("Can not find SiLA Server " + serverName + " in " +
                    String.valueOf(timeOut) + " ms"
            );
        }

        log.info("Found SiLA Server " + serverName);
    }

    /**
     * Blocking call until server is found and then retrieves the gRPC Channel
     * @param serverName SiLA Server name
     * @return Channel connected to Server
     */
    public static ManagedChannel blockAndGetChannel(String serverName) {
        blockUntilServerFound(serverName);
        return getChannel(serverName);
    }

    /**
     * Blocking call until server is found and then retrieves the gRPC Channel
     * @param serverName SiLA Server name
     * @param timeOut    in ms
     * @return Channel connected to Server
     */
    public static ManagedChannel blockAndGetChannel(String serverName, int timeOut)
            throws TimeoutException{
        blockUntilServerFound(serverName, timeOut);
        return getChannel(serverName);
    }

    /**
     * Get gRPC Channel related to SiLA Server
     * @param serverName SiLA Server name
     * @return Channel connected to Server
     */
    private static ManagedChannel getChannel(String serverName) {
        HostAndPort channelInfo;
        try {
            channelInfo = SiLAServerDiscovery.getSocketInfo(serverName);
        } catch (KeyException e) {
            // Nothing to handle, internal logic error
            throw new RuntimeException(e.getMessage());
        }
        return ManagedChannelBuilder.forAddress(
                channelInfo.getHostText(),
                channelInfo.getPort())
                .usePlaintext(true)
                .build();
    }

    /**
     * Retrieve socket information that is advertised of the SiLA Server
     * (uses default listener {@link SimpleListener}) - should be used to setup
     * gRPC clients.
     *
     * @param serverName SiLA Server name
     */
    public static HostAndPort getSocketInfo(String serverName)
            throws KeyException, ValueException {
        if (!SimpleListener.getInstance().onlineServers.containsKey(serverName))
            throw new KeyException(serverName + " is not found by SiLA Discovery.");

        SimpleListener.Server server = SimpleListener.getInstance().onlineServers.get(serverName);
        if (server == null)
            throw new ValueException(serverName + " hasn't been properly resolved.");

        return HostAndPort.fromParts(server.host, server.port);
    }
}