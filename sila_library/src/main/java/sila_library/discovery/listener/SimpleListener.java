package sila_library.discovery.listener;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Shared List of Servers online in the Network
 */
public final class SimpleListener implements ServerListener {
    public class Server {
        public String host;
        public int port;
        public Date joined;
    }

    public Map<String, Server> onlineServers = new HashMap<>();

    private static SimpleListener instance = null;

    private SimpleListener() {
        // Singleton.
    }
    public synchronized static SimpleListener getInstance() {
        if(instance == null) {
            instance = new SimpleListener();
        }
        return instance;
    }

    @Override
    public void serverAdded(String serverName, String host, int port) {
        Server server = new Server();
        server.host = host;
        server.port = port;
        server.joined = new Date();
        onlineServers.put(serverName, server);
    }

    @Override
    public void serverRemoved(String serverName) {
        onlineServers.remove(serverName);
    }
}
