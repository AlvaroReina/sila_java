package sila_library.discovery.listener;

/**
 * Listener interface that gets notified when a SiLA Server is added or removed to the mdns.
 */
public interface ServerListener {
    void serverAdded(String serverName, String host, int port);
    void serverRemoved(String serverName);
}
