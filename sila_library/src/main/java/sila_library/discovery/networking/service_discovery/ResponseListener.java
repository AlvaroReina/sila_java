package sila_library.discovery.networking.service_discovery;

import lombok.extern.slf4j.Slf4j;
import sila_library.discovery.networking.dns.Response;
import sila_library.discovery.networking.dns.records.PtrRecord;
import sila_library.discovery.networking.dns.records.Record;
import sila_library.discovery.networking.service_discovery.Instance;
import sila_library.discovery.networking.service_discovery.InstancesCache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;
import java.util.Set;
import java.util.function.Consumer;

/**
 * Listens to DNS Responses and Updates instance Cache
 */
@Slf4j
public class ResponseListener implements Consumer<Response> {
    private final InstancesCache instancesCache;
    private final String serviceType;

    public ResponseListener(InstancesCache instancesCache, String serviceType) {
        this.instancesCache = instancesCache;
        this.serviceType = serviceType;
    }

    @Override
    public void accept(Response response) {
        log.debug("Listener response: {}", response);

        // Add Instance if PTR Record exists and Service Type is matched
        final Optional<Record> ptrRecord = response.getRecords()
                .stream()
                .filter(record -> record instanceof PtrRecord && record.getName().contains(serviceType))
                .findFirst();

        if (ptrRecord.isPresent()) {
            final PtrRecord ptr = (PtrRecord) ptrRecord.get();

            try {
                final Optional<Instance> optionalInstance = Instance.createFromRecords(ptr, response.getRecords());
                if (!optionalInstance.isPresent()) {
                    return;
                } else {
                    if (optionalInstance.get().getTtl() > 0) {
                        instancesCache.addInstance(optionalInstance.get());
                    } else {
                        log.debug("Instance {} TTL is 0", optionalInstance.get().getName());
                        instancesCache.removeInstance(optionalInstance.get());
                    }
                }
            } catch (Exception e) {
                log.debug(e.getMessage());
            }
        }
    }
}
