package sila_library.discovery.networking.service_discovery;

import io.netty.util.internal.ConcurrentSet;
import lombok.extern.slf4j.Slf4j;
import sila_library.discovery.listener.ServerListener;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

@Slf4j
public class InstancesCache {
    private static final int CONNECT_TIMEOUT = 1000; // [ms] When a server is assumed to be down

    private final Map<String, Instance> instances = new ConcurrentHashMap<>();
    private final Set<ServerListener> serverListeners = new ConcurrentSet<>();

    private final int discoveryValidPeriod; // [ms]

    /**
     * Maintains the Cache of Instances
     *
     * @param discoveryValidPeriod in [ms] - indicates the time an Instance is trusted
     */
    public InstancesCache(int discoveryValidPeriod) {
        this.discoveryValidPeriod = discoveryValidPeriod; // incoming in ms

    }

    public void run() {
        new Timer().schedule(new TimerTask() {
            @Override public void run() {
                pingAll();
            }
        }, 0, this.discoveryValidPeriod);
    }


    public void addListener(ServerListener listener) {
        this.serverListeners.add(listener);
    }

    public void removeListener(ServerListener listener) {
        this.serverListeners.remove(listener);
    }

    public void addInstance(Instance instance) {
        log.debug("Trying to add instance: {}", instance.getName());

        // If Instance exists, we stamp it
        final String instanceName = instance.getName();
        if (instances.containsKey(instanceName)) {
            instances.get(instanceName).stamp();
        } else {
            if (instance.getTtl() != 0) {
                log.info("Instance added: {}", instance.toString());
                instances.put(instance.getName(), instance);
                for (ServerListener listener : serverListeners) {
                    listener.serverAdded(instance.getName(), instance.getHostAddress(), instance.getPort());
                }
            }
        }
    }

    public void removeInstance(Instance instance){
        // If Instance exists, we remove it
        final String instanceName = instance.getName();
        if (instances.containsKey(instanceName)) {
            log.info("Instance removed: {}", instanceName);
            instances.remove(instanceName);
            for (ServerListener listener : serverListeners) {
                listener.serverRemoved(instanceName);
            }
        }

    }

    private void pingAll() {
        for (Instance instance : instances.values()) {
            if (instance.checkHowLongLived() > (this.discoveryValidPeriod)) {
                if (!checkConnection(instance.getHostAddress(), instance.getPort())) {
                    removeInstance(instance);
                }
            }
        }
    }

    private boolean checkConnection(String host, int port) {
        boolean result;
        final Socket socket = new Socket();
        final InetSocketAddress endPoint = new InetSocketAddress(host, port);
        if ( endPoint.isUnresolved() ) {
            log.error("Failure " + endPoint);
            return false;
        } else try {
            socket.connect(endPoint, CONNECT_TIMEOUT);
            log.debug("Success reaching instance {}", endPoint);
            result = true;
        } catch(IOException ioe) {
            log.warn("Failure reaching instance {} message: {} - {}",
                    endPoint , ioe.getClass().getSimpleName(),  ioe.getMessage());
            result = false;
        } finally {
            try {
                socket.close();
            } catch( IOException ioe ) {
                log.warn(ioe.getMessage());
            }
        }
        return result;
    }
}
