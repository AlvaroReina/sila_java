package sila_library.discovery.networking.service_discovery;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import sila_library.discovery.networking.dns.records.*;

import java.net.*;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Service Instance
 */
@Slf4j
@Getter
public class Instance {
    private final String name;
    private final String hostAddress;
    private final int port;
    private final Map<String, String> attributes;
    private Long ttl;
    private Instant instantStamp;

    private Instance(String name, InetAddress address,
            int port, Map<String, String> attributes, Long ttl) {
        this.name = name;
        this.ttl = ttl;
        this.hostAddress = address.getHostAddress();
        this.port = port;
        this.attributes = attributes;
        this.instantStamp = Instant.now();
    }

    /**
     * Create Service Instance from a Set of Records
     * @param ptr PTR Record
     * @param records Set of All Records
     * @return an Instance Object
     */
    public static Optional<Instance> createFromRecords(PtrRecord ptr, Set<Record> records) {
        String name = ptr.getUserVisibleName();
        int port;
        long ttl;
        List<InetAddress> addresses = new ArrayList<>();
        Map<String, String> attributes = Collections.emptyMap();

        // Check SRV Record and populate values
        Optional<SrvRecord> srv = records.stream()
                .filter(r -> r instanceof SrvRecord && r.getName().equals(ptr.getPtrName()))
                .map(r -> (SrvRecord) r).findFirst();
        if (srv.isPresent()) {
            ttl = srv.get().getTTL();
            port = srv.get().getPort();
        } else {
            log.debug("Cannot create Instance when no SRV record is available");
            return Optional.empty();
        }

        final Optional<TxtRecord> txt = records.stream()
                .filter(r -> r instanceof TxtRecord && r.getName().equals(ptr.getPtrName()))
                .map(r -> (TxtRecord) r).findFirst();
        if (txt.isPresent()) {
            attributes = txt.get().getAttributes();
            ttl = srv.get().getTTL();
        }

        Optional<ARecord> aRecord = records.stream()
                .filter(r -> r instanceof ARecord && r.getName().equals(srv.get().getTarget()))
                .map(r -> (ARecord) r).findFirst();
        Optional<AaaaRecord> aaaRecord = records.stream()
                .filter(r -> r instanceof AaaaRecord && r.getName().equals(srv.get().getTarget()))
                .map(r -> (AaaaRecord) r).findFirst();

        if (aRecord.isPresent()) {
            addresses.addAll(records.stream().filter(r -> r instanceof ARecord)
                    .filter(r -> r.getName().equals(srv.get().getTarget())).map(r -> ((ARecord) r).getAddress())
                    .collect(Collectors.toList()));
        } else if (aaaRecord.isPresent()) {
            addresses.addAll(records.stream().filter(r -> r instanceof AaaaRecord)
                    .filter(r -> r.getName().equals(srv.get().getTarget())).map(r -> ((AaaaRecord) r).getAddress())
                    .collect(Collectors.toList()));
        } else {
            return Optional.empty();
        }
        return Optional.of(new Instance(name, addresses.get(0), port, attributes, ttl));
    }

    public void stamp() {
        log.debug("Instance " + getName() + " stamped!");
        this.instantStamp = Instant.now();
    }

    public long checkHowLongLived() {
        return ChronoUnit.MILLIS.between(instantStamp, Instant.now());
    }
}
