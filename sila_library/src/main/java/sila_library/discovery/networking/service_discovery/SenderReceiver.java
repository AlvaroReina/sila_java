package sila_library.discovery.networking.service_discovery;

import lombok.Value;
import lombok.extern.slf4j.Slf4j;
import sila_library.discovery.networking.dns.Question;
import sila_library.discovery.networking.dns.Response;

import java.io.IOException;
import java.net.*;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.channels.ClosedChannelException;
import java.nio.channels.DatagramChannel;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Consumer;

import static sila_library.discovery.networking.helpers.MulticastDns.*;

/**
 * Manages Datagram Channels to send UDP mDNS Messages and receive them
 */
@Slf4j
public class SenderReceiver implements AutoCloseable {
    // Rate to check if Network is up and create interfaces
    private static final int CHECK_NETWORK_PERIOD = 1000; // [ms]
    private static final int CHANNEL_READ_DELAY = 100; // [ms]

    // Continuously running receiver thread
    private final Thread receiverThread;

    // listener to be invoked whenever a new message is received.
    private final Consumer<Response> listener;

    private final List<NetworkInterface> networkInterfaces;
    private final Map<String, List<mDNSChannel>> channelsMap = new ConcurrentHashMap<>();

    /**
     * Utility Class that combines the channel with the target
     * address for mDNS
     */
    @Value
    private class mDNSChannel {
        DatagramChannel datagramChannel;
        InetSocketAddress target;

        public void close() {
            try {
                datagramChannel.disconnect();
                datagramChannel.close();
            } catch (IOException e) {
                log.warn(e.getMessage());
            }
        }
    }

    /**
     * Timer Task to create UDP Datagram Channels on specified Interfaces
     */
    private final class NetworkAssigner extends TimerTask {
        @Override
        public final void run() {
            for (final NetworkInterface networkInterface : networkInterfaces) {
                try {
                    final String networkInterfaceName = networkInterface.getName();

                    // No discovery possible in p2p interface
                    if (networkInterface.isPointToPoint()) {
                        continue;
                    }

                    // Network is down and channels are saved: Close Channels and remove the entry
                    if (!networkInterface.isUp() && channelsMap
                            .containsKey(networkInterfaceName)) {
                        log.debug("Removing interface {}", networkInterfaceName);
                        final List<mDNSChannel> mDNSChannels = channelsMap
                                .get(networkInterfaceName);
                        for (mDNSChannel mDNSChannel : mDNSChannels) {
                            mDNSChannel.close();
                        }
                        channelsMap.remove(networkInterfaceName);
                    }

                    // Network is up and channels not saved, check if there is any IPv4 or IPv6 and create channels
                    if (networkInterface.isUp() && !channelsMap
                            .containsKey(networkInterfaceName)) {
                        final List<mDNSChannel> mDNSChannelList = new ArrayList<>();

                        if (hasIpv(networkInterface, Inet4Address.class)) {
                            openChannel(networkInterface, IPV4_ADDR)
                                    .ifPresent(datagramChannel ->
                                            mDNSChannelList.add(new mDNSChannel(
                                                    datagramChannel,
                                                    IPV4_SOA
                                            ))
                                    );
                        }

                        //@TODO: IPv6 not supported yet, needs to be tested
                        /*
                        if (hasIpv(networkInterface, Inet6Address.class)) {
                            openChannel(networkInterface, IPV6_ADDR)
                                    .ifPresent(datagramChannel ->
                                            mDNSChannelList.add(new mDNSChannel(
                                                    datagramChannel,
                                                    IPV6_SOA
                                            ))
                                    );
                        }
                        */

                        channelsMap.put(networkInterfaceName, mDNSChannelList);
                    }
                } catch (SocketException e) {
                    log.error(e.getMessage());
                }
            }
        }
    }

    /**
     * Asynchronously reading all channel buffers and informing listeners
     */
    private final class Receiver implements Runnable {
        @Override
        public final void run() {
            final ByteBuffer buf = ByteBuffer.allocate(MAX_DNS_MESSAGE_SIZE);
            buf.order(ByteOrder.BIG_ENDIAN);
            while (!Thread.currentThread().isInterrupted()) {
                try {
                    for (List<mDNSChannel> mDNSChannelList : channelsMap.values()) {
                        for (mDNSChannel mDNSChannel : mDNSChannelList) {
                            final DatagramChannel datagramChannel = mDNSChannel.getDatagramChannel();

                            if (!datagramChannel.isOpen()) continue;

                            buf.clear();

                            final SocketAddress address = datagramChannel.receive(buf);
                            if (address != null && buf.position() != 0) {
                                buf.flip();
                                final byte[] bytes = new byte[buf.remaining()];
                                buf.get(bytes);

                                final Response response = Response.createFrom(
                                        new DatagramPacket(bytes, bytes.length )
                                );
                                log.debug("Received " + response + " on " + address);
                                listener.accept(response);
                            }
                        }
                    }
                    Thread.sleep(CHANNEL_READ_DELAY);
                } catch (final ClosedChannelException e) {
                    log.debug("Channel closed while waiting to receive DNS message, {}", e);
                    Thread.currentThread().interrupt();
                } catch (final IOException e) {
                    log.debug("I/O error while receiving DNS message {}", e);
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                    log.warn("Thread interrupted while receiving DNS message {}", e);
                }
            }
        }
    }

    /**
     * Constructor
     *
     * @implNote Currently only possible on all found network interfaces
     *
     * @param listener Listener Listening to incoming mDNS messages
     */
    SenderReceiver(
            final Consumer<Response> listener
    ) {
        try {
            this.networkInterfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
        } catch (SocketException e) {
            throw new RuntimeException(e);
        }
        this.receiverThread = new Thread(new Receiver());
        this.listener = listener;

        final Timer networkTimer = new Timer();
        networkTimer.schedule(new NetworkAssigner(), 0, CHECK_NETWORK_PERIOD);
    }

    @Override
    public final synchronized void close() {
        log.info("Closing channel");
        receiverThread.interrupt();
    }

    /**
     * Enables receiving DNS messages.
     */
    final synchronized void enable() {
        receiverThread.start();
    }

    /**
     * Send the given DNS Message on all Network Interfaces
     *
     * @param questionQuery question to send
     */
    final void send(final Question questionQuery) {
        log.debug("Sending {}", questionQuery);

        //
        final ByteBuffer buf = ByteBuffer.allocate(MAX_DNS_MESSAGE_SIZE);
        buf.order(ByteOrder.BIG_ENDIAN);

        final byte[] packet = questionQuery.encode();
        buf.clear();
        buf.put(packet);
        buf.flip();

        for (List<mDNSChannel> mDNSChannelList : channelsMap.values()) {
            for (mDNSChannel mDNSChannel : mDNSChannelList) {
                send(mDNSChannel, buf);
            }
        }
    }

    /**
     * Open and Configuring the Datagram Channel
     * @param networkInterface Network Interface to Bind to
     * @param inetAddress Inet Address to bind to
     * @return Optional Return
     */
    private Optional<DatagramChannel> openChannel(
            final NetworkInterface networkInterface, final InetAddress inetAddress
    ) {
        try {
            if (networkInterface.isUp() && (networkInterface.supportsMulticast() || networkInterface.isLoopback())) {
                final ProtocolFamily family = (inetAddress instanceof Inet6Address)
                        ? StandardProtocolFamily.INET6 : StandardProtocolFamily.INET;
                final DatagramChannel channel = DatagramChannel.open(family);

                channel.configureBlocking(false);
                channel.setOption(StandardSocketOptions.SO_REUSEADDR, true);
                channel.setOption(StandardSocketOptions.IP_MULTICAST_TTL, 255);
                channel.bind(new InetSocketAddress(MDNS_PORT));

                channel.setOption(StandardSocketOptions.IP_MULTICAST_IF, networkInterface);
                channel.join(inetAddress, networkInterface);

                log.debug("Joined networking address " + inetAddress + " on " + networkInterface);
                return Optional.of(channel);
            } else {
                return Optional.empty();
            }
        } catch (IOException e) {
            log.debug("Ignored " + networkInterface + " for " + inetAddress);
            return Optional.empty();
        }
    }

    /**
     * Check if the Network Interface has any InetAdress from the given Class
     */
    private boolean hasIpv(final NetworkInterface ni, final Class<? extends InetAddress> ipv) {
        for (InetAddress inetAddress : Collections.list(ni.getInetAddresses())) {
            if (inetAddress.getClass().isAssignableFrom(ipv)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Sends given datagram to given channel an address
     *
     * @param mDNSChannel the channel on which the target is connected
     * @param src the buffer containing the datagram to be sent
     */
    private void send(final mDNSChannel mDNSChannel, final ByteBuffer src) {
        final DatagramChannel datagramChannel = mDNSChannel.getDatagramChannel();
        final InetSocketAddress target = mDNSChannel.getTarget();

        if (!datagramChannel.isOpen()) return;

        final int position = src.position();
        try {
            datagramChannel.send(src, target);
            log.debug("Sent DNS message to " + target);
        } catch (final IOException e) {
            log.debug("I/O error while sending DNS message to " + target);
        } finally {
            src.position(position);
        }
    }
}
