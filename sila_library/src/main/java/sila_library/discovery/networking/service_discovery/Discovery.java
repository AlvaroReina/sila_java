package sila_library.discovery.networking.service_discovery;

import lombok.extern.slf4j.Slf4j;
import sila_library.discovery.listener.ServerListener;
import sila_library.discovery.networking.dns.*;

import javax.annotation.Nonnull;
import java.util.*;
import java.util.function.Consumer;

@Slf4j
public class Discovery {
    private final InstancesCache instancesCache;
    private final String dnsQueryName;

    private final SenderReceiver senderReceiver;

    /**
     * Discovery Class to get DNS-SD records via mDNS
     *
     * @param serviceType Service Type according to the ZeroConfiguration specification
     * @param serviceDomain Service Domain According to the ZeroConfiguration specification
     * @param queryPeriod Query Period in ms
     */
    public Discovery(
            @Nonnull String serviceType,
            @Nonnull String serviceDomain,
            int queryPeriod) {
        this.dnsQueryName = (serviceType + serviceDomain).toLowerCase();

        // 2 Rounds of Querying will be trusted
        instancesCache = new InstancesCache(2*queryPeriod);
        instancesCache.run();

        final Consumer<Response> listener = new ResponseListener(instancesCache, serviceType);
        senderReceiver = new SenderReceiver(listener);
        senderReceiver.enable();

        final Timer queryTimer = new Timer();
        queryTimer.schedule(new TimerTask() {
            @Override public void run() {
                query();
            }
        }, 0, queryPeriod);
    }

    /**
     * Add Listener
     */
    public void addListener(ServerListener serverListener) {
        instancesCache.addListener(serverListener);
    }

    /**
     *
     */
    public void removeListener(ServerListener serverListener) {
        instancesCache.removeListener(serverListener);
    }

    /**
     * Issue a query on all specified Network Interfaces
     */
    public void query() {
        senderReceiver.send(new Question(dnsQueryName, Question.QType.PTR, Question.QClass.IN));
    }
}
