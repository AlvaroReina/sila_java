package sila_library.discovery.networking.helpers;

import java.net.*;
import java.time.Duration;

/**
 * Multicast-DNS constants.
 */
public final class MulticastDns {
    /** maximum size of DNS message in bytes. */
    public static final int MAX_DNS_MESSAGE_SIZE = 65536;

    /** mDNS IPV4 address. */
    public static final InetAddress IPV4_ADDR;

    /** mDNS IPV6 address. */
    public static final InetAddress IPV6_ADDR;

    /** mDNS port. */
    public static final int MDNS_PORT;

    /** IPV4 socket address. */
    public static final InetSocketAddress IPV4_SOA;

    /** IPV6 socket address. */
    public static final InetSocketAddress IPV6_SOA;

    /** interval between probe message. */
    public static final Duration PROBING_INTERVAL;

    /** probing timeout. */
    public static final Duration PROBING_TIMEOUT;

    /** number of probes before announcing a registered service. */
    public static final int PROBE_NUM;

    /** interval between goodbyes messages. */
    public static final Duration CANCELLING_INTERVAL;

    /** number of cancel message sent when de-registering a service. */
    public static final int CANCEL_NUM;

    /** cache record reaper interval. */
    public static final Duration REAPING_INTERVAL;

    /** default resolution timeout. */
    public static final Duration RESOLUTION_TIMEOUT;

    /** interval between resolution question. */
    public static final Duration RESOLUTION_INTERVAL;

    /** number of queries. */
    public static final int QUERY_NUM;

    /** interval between browsing query. */
    public static final Duration QUERYING_DELAY;

    /** interval between browsing query. */
    public static final Duration QUERYING_INTERVAL;

    /** time to live: 1 hour. */
    public static final Duration TTL;

    /** time to live after expiry: 1 second. */
    public static final Duration EXPIRY_TTL;

    /** query or response mask (unsigned). */
    public static final short FLAGS_QR_MASK = (short) 0x8000;

    /** query flag (unsigned). */
    public static final short FLAGS_QR_QUERY = 0x0000;

    /** response flag (unsigned). */
    public static final short FLAGS_QR_RESPONSE = (short) 0x8000;

    /** authoritative answer flag (unsigned). */
    public static final short FLAGS_AA = 0x0400;

    /** class mask (unsigned). */
    public static final short CLASS_MASK = 0x7FFF;

    /** unique class (unsigned). */
    public static final short CLASS_UNIQUE = (short) 0x8000;

    public final static int USHORT_MASK = 0xFFFF;
    public final static long UINT_MASK = 0xFFFFFFFFL;

    public final static int FLAG_QR_MASK = 0x8000;
    public final static int FLAG_OPCODE_MASK = 0x7800;
    public final static int FLAG_RCODE_MASK = 0xF;

    public static final String MDNS_IP4_ADDRESS;
    public static final String MDNS_IP6_ADDRESS;

    // Many of these values are not currently used, but they come from the OS library.
    static {
        MDNS_PORT = 5353;
        MDNS_IP4_ADDRESS = "224.0.0.251";
        MDNS_IP6_ADDRESS = "FF02::FB";
        try {
            IPV4_ADDR = InetAddress.getByName(MDNS_IP4_ADDRESS);
            IPV6_ADDR = InetAddress.getByName(MDNS_IP6_ADDRESS);
            IPV4_SOA = new InetSocketAddress(IPV4_ADDR, MDNS_PORT);
            IPV6_SOA = new InetSocketAddress(IPV6_ADDR, MDNS_PORT);
        } catch (UnknownHostException e) {
            throw new RuntimeException(e);
        }
        RESOLUTION_TIMEOUT = Duration.ofMillis(6000);
        RESOLUTION_INTERVAL = Duration.ofMillis(200);
        PROBING_TIMEOUT = Duration.ofMillis(6000);
        PROBING_INTERVAL = Duration.ofMillis(250);
        PROBE_NUM = 3;
        QUERYING_DELAY = Duration.ofMillis(120);
        QUERYING_INTERVAL = Duration.ofMillis(1200000);
        QUERY_NUM = 3;
        CANCELLING_INTERVAL = Duration.ofMillis(250);
        CANCEL_NUM = 3;
        REAPING_INTERVAL = Duration.ofMillis(10000);
        TTL = Duration.ofMillis(3600000);
        EXPIRY_TTL = Duration.ofMillis(1000);
    }

    // Constructor.
    public MulticastDns() { }

    /**
     * Decodes the given class and returns an array with the class index and whether the class is unique (a value
     * different from 0 denotes a unique class).
     *
     * @param clazz class
     * @return an array of 2 shorts, first is class index, second whether class is unique
     */
    public static short[] decodeClass(final short clazz) {
        return new short[] { (short) (clazz & CLASS_MASK), (short) (clazz & CLASS_UNIQUE) };
    }

    /**
     * Encodes the given class index and whether it is unique into a class. This is the reverse operation of
     * {@link #encodeClass(short, boolean)}.
     *
     * @param classIndex class index
     * @param unique whether the class is unique
     * @return encoded class
     */
    public static short encodeClass(final short classIndex, final boolean unique) {
        return (short) (classIndex | (unique ? CLASS_UNIQUE : 0));
    }

    /**
     * Makes the given class unique.
     *
     * @param classIndex class index
     * @return unique class
     */
    public static short uniqueClass(final short classIndex) {
        return (short) (classIndex | CLASS_UNIQUE);
    }
}
