package sila_library.discovery.networking.dns.records;

import lombok.extern.slf4j.Slf4j;

import java.io.UnsupportedEncodingException;
import java.net.UnknownHostException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
public abstract class Record {
    public final static int USHORT_MASK = 0xFFFF;
    private final static long UINT_MASK = 0xFFFFFFFFL;
    private final static String NAME_CHARSET = "UTF-8";

    protected final String name;
    protected final long ttl;

    protected Record(String name, long ttl) {
        this.name = name;
        this.ttl = ttl;
    }

    public String getName() {
        return name;
    }

    public long getTTL() {
        return ttl;
    }

    /**
     * Create DNS Record from Bytebuffer
     *
     * @implNote Currently supported records: A, AAAA, PTR, SRV, TXT
     *
     * @param buffer BytBuffer containing type, buffer, class and record specific information
     *
     * @return Specific Record, UnknownRecord if not supported
     */
    public static Record fromBuffer(ByteBuffer buffer) {
        String name = readNameFromBuffer(buffer);
        final Type type = Type.fromInt(buffer.getShort() & USHORT_MASK);

        // Note: We don't use DNS Classes for now
        buffer.getShort();

        long ttl = buffer.getInt() & UINT_MASK;
        int rdLength = buffer.getShort() & USHORT_MASK;

        switch (type) {
            case A:
                try {
                    return new ARecord(buffer, name, ttl);
                } catch (UnknownHostException e) {
                    throw new IllegalArgumentException("Buffer does not represent a valid A record");
                }
            case AAAA:
                try {
                    return new AaaaRecord(buffer, name, ttl);
                } catch (UnknownHostException e) {
                    throw new IllegalArgumentException("Buffer does not represent a valid AAAA record");
                }
            case PTR:
                return new PtrRecord(buffer, name, ttl, rdLength);
            case SRV:
                return new SrvRecord(buffer, name, ttl);
            case TXT:
                return new TxtRecord(buffer, name, ttl, rdLength);
            default:
                return new UnknownRecord(name, ttl);
        }
    }

    public static String readNameFromBuffer(ByteBuffer buffer) {
        List<String> labels = new ArrayList<>();
        int labelLength;
        int continueFrom = -1;

        try {
            do {
                buffer.mark();
                labelLength = buffer.get() & 0xFF;
                if (isPointer(labelLength)) {
                    buffer.reset();
                    int offset = buffer.getShort() & 0x3FFF;
                    if (continueFrom < 0) {
                        continueFrom = buffer.position();
                    }
                    buffer.position(offset);
                } else {
                    String label = readLabel(buffer, labelLength);
                    labels.add(label);
                }
            } while (labelLength != 0);
        } catch (BufferUnderflowException | IllegalArgumentException e) {
            log.debug(e.getMessage());
        }
        if (continueFrom >= 0) {
            buffer.position(continueFrom);
        }

        return labels.stream().collect(Collectors.joining("."));
    }

    public static List<String> readStringsFromBuffer(ByteBuffer buffer, int length) {
        List<String> strings = new ArrayList<>();
        int bytesRead = 0;
        do {
            int stringLength = buffer.get() & 0xFF;
            String label = readLabel(buffer, stringLength);
            bytesRead += label.length() + 1;
            strings.add(label);
        } while (bytesRead < length);
        return strings;
    }

    private static boolean isPointer(int octet) {
        return (octet & 0xC0) == 0xC0;
    }

    private static String readLabel(ByteBuffer buffer, int length) {
        String label = "";
        if (length > 0) {
            byte[] labelBuffer = new byte[length];
            buffer.get(labelBuffer);
            try {
                label = new String(labelBuffer, NAME_CHARSET);
            } catch (UnsupportedEncodingException e) {
                System.err.println("UnsupportedEncoding: " + e);
            }
        }
        return label;
    }

    @Override
    public String toString() {
        return "Record{" +
                "name='" + name + '\'' +
                ", ttl=" + ttl +
                '}';
    }

    private enum Type {
        UNSUPPORTED(0),
        A(1),
        NS(2),
        CNAME(5),
        SOA(6),
        NULL(10),
        WKS(11),
        PTR(12),
        HINFO(13),
        MINFO(14),
        MX(15),
        TXT(16),
        AAAA(28),
        SRV(33);

        private final int value;

        public static Type fromInt(int val) {
            for (Type type : values()) {
                if (type.value == val) {
                    return type;
                }
            }
            return UNSUPPORTED;
        }

        Type(int value) {
            this.value = value;
        }
    }
}
