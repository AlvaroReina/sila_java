package sila_library.discovery.networking.dns.records;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;

/**
 * The record AAAA (also quad-A record) specifies IPv6 address for given host.
 * So it works the same way as the A record and the difference is the type of IP address.
 */
public class AaaaRecord extends Record {
    private InetAddress address;

    public AaaaRecord(ByteBuffer buffer, String name, long ttl)
            throws UnknownHostException {
        super(name, ttl);
        byte[] addressBytes = new byte[16];
        buffer.get(addressBytes);
        address = InetAddress.getByAddress(addressBytes);
    }

    public InetAddress getAddress() {
        return address;
    }

    @Override
    public String toString() {
        return "AaaaRecord{" +
                "name='" + name + '\'' +
                ", ttl=" + ttl +
                ", address=" + address +
                '}';
    }
}
