package sila_library.discovery.networking.dns.records;

import java.nio.ByteBuffer;

/**
 * An SRV record typically defines a symbolic name and
 * the transport protocol used as part of the domain name
 */
public class SrvRecord extends Record {
    private final int priority;
    private final int weight;
    private final int port;
    private final String target;

    public SrvRecord(ByteBuffer buffer, String name, long ttl) {
        super(name, ttl);
        priority = buffer.getShort() & USHORT_MASK;
        weight = buffer.getShort() & USHORT_MASK;
        port = buffer.getShort() & USHORT_MASK;
        target = readNameFromBuffer(buffer);
    }

    public int getPriority() {
        return priority;
    }

    public int getWeight() {
        return weight;
    }

    public int getPort() {
        return port;
    }

    public String getTarget() {
        return target;
    }

    @Override
    public String toString() {
        return "SrvRecord{" +
                "name='" + name + '\'' +
                ", ttl=" + ttl +
                ", priority=" + priority +
                ", weight=" + weight +
                ", port=" + port +
                ", target='" + target + '\'' +
                '}';
    }
}
