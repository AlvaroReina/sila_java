package sila_library.discovery.networking.dns.records;

import java.nio.ByteBuffer;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The text record can hold arbitrary non-formatted text string.
 */
public class TxtRecord extends Record {
    private Map<String, String> attributes;

    public TxtRecord(ByteBuffer buffer, String name, long ttl, int length) {
        super(name, ttl);
        List<String> strings = readStringsFromBuffer(buffer, length);
        attributes = parseDataStrings(strings);
    }

    private Map<String, String> parseDataStrings(List<String> strings) {
        final Map<String, String> pairs = new HashMap<>();
        strings.stream().forEach(s -> {
            String[] parts = s.split("=");
            if (parts.length > 1) {
                pairs.put(parts[0], parts[1]);
            } else {
                pairs.put(parts[0], "");
            }
        });
        return pairs;
    }

    public Map<String, String> getAttributes() {
        return Collections.unmodifiableMap(attributes);
    }

    @Override
    public String toString() {
        return "TxtRecord{" +
                "name='" + name + '\'' +
                ", ttl=" + ttl +
                ", attributes=" + attributes +
                '}';
    }
}
