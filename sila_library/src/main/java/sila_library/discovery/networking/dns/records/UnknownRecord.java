package sila_library.discovery.networking.dns.records;

/**
 * Handle records that are not necessary for DNS-SD
 */
class UnknownRecord extends Record {
    UnknownRecord(String name, long ttl) {
        super(name, ttl);
    }
}
