package sila_library.discovery.networking.dns;

import lombok.extern.slf4j.Slf4j;
import sila_library.discovery.networking.dns.records.Record;
import sila_library.discovery.networking.helpers.MessageOutputStream;

import java.nio.ByteBuffer;

import static sila_library.discovery.networking.helpers.MulticastDns.FLAGS_QR_QUERY;
import static sila_library.discovery.networking.helpers.MulticastDns.encodeClass;

@Slf4j
public class Question {
    private final static short UNICAST_RESPONSE_BIT = (short) 0x8000;

    private final String qName;
    private final QType qType;
    private final QClass qClass;

    public static Question fromBuffer(ByteBuffer buffer) {
        String name = Record.readNameFromBuffer(buffer);
        QType type = QType.fromInt(buffer.getShort() & Record.USHORT_MASK);
        QClass qClass = QClass.fromInt(buffer.getShort() & Record.USHORT_MASK);
        return new Question(name, type, qClass);
    }

    public Question(String name, QType type, QClass qClass) {
        this.qName = name;
        this.qType = type;
        this.qClass = qClass;
    }

    public final byte[] encode() {
        try (final MessageOutputStream mos = new MessageOutputStream()) {

            // First write the header
            mos.writeShort((short) 0);
            mos.writeShort(FLAGS_QR_QUERY);
            mos.writeShort((short) 1); // number of questions
            mos.writeShort((short) 0); // number of answers
            mos.writeShort((short) 0); // number of authorities
            mos.writeShort((short) 0); // number of additionals

            // Then write the question
            mos.writeName(qName);
            mos.writeShort(qType.asUnsignedShort());
            mos.writeShort(encodeClass((short) qClass.asUnsignedShort(), qClass.equals(QClass.IN)));

            return mos.toByteArray();
        }
    }

    public enum QType {
        A(1),
        NS(2),
        CNAME(5),
        SOA(6),
        MB(7),
        MG(8),
        MR(9),
        NULL(10),
        WKS(11),
        PTR(12),
        HINFO(13),
        MINFO(14),
        MX(15),
        TXT(16),
        AAAA(28),
        SRV(33),
        ANY(255);

        private final int value;

        public static QType fromInt(int val) {
            for (QType type : values()) {
                if (type.value == val) {
                    return type;
                }
            }
            throw new IllegalArgumentException("Can't convert " + val + " to a QType");
        }

        QType(int value) {
            this.value = value;
        }

        public int asUnsignedShort() {
            return value & Record.USHORT_MASK;
        }
    }

    public enum QClass {
        IN(1),
        ANY(255);

        private final int value;

        public static QClass fromInt(int val) {
            for (QClass c : values()) {
                if (c.value == (val & ~UNICAST_RESPONSE_BIT)) {
                    return c;
                }
            }
            throw new IllegalArgumentException("Can't convert " + val + " to a QClass");
        }

        QClass(int value) {
            this.value = value;
        }

        public int asUnsignedShort() {
            return value & Record.USHORT_MASK;
        }
    }
}