package sila_library.discovery.networking.dns.records;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;

/**
 * The record A specifies IP address (IPv4) for given host.
 * A records are used for conversion of domain names to corresponding IP addresses.
 */
public class ARecord extends Record {
    private InetAddress address;

    public ARecord(ByteBuffer buffer, String name, long ttl) throws UnknownHostException {
        super(name, ttl);
        byte[] addressBytes = new byte[4];
        buffer.get(addressBytes);
        address = InetAddress.getByAddress(addressBytes);
    }

    public InetAddress getAddress() {
        return address;
    }

    @Override
    public String toString() {
        return "ARecord{" +
                "name='" + name + '\'' +
                ", ttl=" + ttl +
                ", address=" + address +
                '}';
    }
}
