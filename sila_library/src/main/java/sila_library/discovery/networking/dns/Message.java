package sila_library.discovery.networking.dns;

import java.nio.ByteBuffer;

public abstract class Message {
    protected ByteBuffer buffer;

    public final static int MAX_LENGTH = 9000; // max size of mDNS packets, in bytes

    private final static int USHORT_MASK = 0xFFFF;

    protected Message() {
        buffer = ByteBuffer.allocate(MAX_LENGTH);
    }

    protected int readUnsignedShort() {
        try {
            return buffer.getShort() & USHORT_MASK;
        } catch (Exception e) {
            return 0;
        }
    }
}
