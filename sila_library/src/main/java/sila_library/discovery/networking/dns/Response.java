package sila_library.discovery.networking.dns;

import lombok.extern.slf4j.Slf4j;
import sila_library.discovery.networking.dns.records.Record;

import java.net.DatagramPacket;
import java.nio.BufferUnderflowException;
import java.util.*;

import static sila_library.discovery.networking.helpers.MulticastDns.*;

@Slf4j
public class Response extends Message {
    private final List<Question> questions = new ArrayList<>();
    private final List<Record> records = new ArrayList<>();
    private int numQuestions;
    private int numAnswers;
    private int numNameServers;
    private int numAdditionalRecords;

    public static Response createFrom(DatagramPacket packet) {
        Response response = new Response(packet);
        response.parseRecords();
        return response;
    }

    private Response(DatagramPacket packet) {
        byte[] dstBuffer = buffer.array();
        try {
            System.arraycopy(packet.getData(), packet.getOffset(), dstBuffer, 0, packet.getLength());
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        try {
            buffer.limit(packet.getLength());
            buffer.position(0);
        } catch (Exception e) {
            log.error("Strange error: ", e);
        }
    }

    private void parseRecords() {
        parseHeader();

        try {
            for (int i = 0; i < numQuestions; i++) {
                Question question = Question.fromBuffer(buffer);
                questions.add(question);
            }
            for (int i = 0; i < numAnswers; i++) {
                Record record = Record.fromBuffer(buffer);
                records.add(record);
            }
            for (int i = 0; i < numNameServers; i++) {
                Record record = Record.fromBuffer(buffer);
                records.add(record);
            }
            for (int i = 0; i < numAdditionalRecords; i++) {
                Record record = Record.fromBuffer(buffer);
                records.add(record);
            }
        } catch (BufferUnderflowException e) {
            log.debug("Unparseable Record");
        }
    }

    private void parseHeader() {
        readUnsignedShort(); // Skip over the ID
        int codes = readUnsignedShort();
        if ((codes & FLAG_QR_MASK) != FLAG_QR_MASK) {
            log.debug("Packet is not a DNS response");
            return;
        }
        if ((codes & FLAG_OPCODE_MASK) != 0) {
            // throw new IllegalArgumentException("mDNS response packets can't have OPCODE values");
            log.debug("mDNS response packets can't have OPCODE values");
            return;
        }
        if ((codes & FLAG_RCODE_MASK) != 0) {
            // throw new IllegalArgumentException("mDNS response packets can't have RCODE values");
            log.debug("mDNS response packets can't have RCODE values");
            return;
        }
        numQuestions = readUnsignedShort();
        numAnswers = readUnsignedShort();
        numNameServers = readUnsignedShort();
        numAdditionalRecords = readUnsignedShort();
    }

    public Set<Record> getRecords() {
        return new HashSet<>(Collections.unmodifiableSet(new HashSet<>(records)));
    }

    @Override
    public String toString() {
        return "Response{" +
                "questions=" + questions +
                ", records=" + records +
                ", numQuestions=" + numQuestions +
                ", numAnswers=" + numAnswers +
                ", numNameServers=" + numNameServers +
                ", numAdditionalRecords=" + numAdditionalRecords +
                '}';
    }
}
