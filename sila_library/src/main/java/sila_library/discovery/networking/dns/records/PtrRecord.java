package sila_library.discovery.networking.dns.records;

import java.nio.ByteBuffer;

/**
 * As opposed to forward DNS resolution (A and AAAA DNS records),
 * the PTR record is used to look up domain names based on an IP address.
 */
public class PtrRecord extends Record {
    private final String userVisibleName;
    private final String ptrName;

    private final static String UNTITLED_NAME = "Untitled";

    public PtrRecord(ByteBuffer buffer, String name, long ttl, int rdLength) {
        super(name, ttl);

        if (rdLength > 0) {
            ptrName = readNameFromBuffer(buffer);
        } else {
            ptrName = "";
        }
        userVisibleName = buildUserVisibleName();
    }

    public String getPtrName() {
        return ptrName;
    }

    public String getUserVisibleName() {
        return userVisibleName;
    }

    private String buildUserVisibleName() {
        final String[] parts = ptrName.split("\\.");
        if (parts[0].length() > 0) {
            return parts[0];
        } else {
            return UNTITLED_NAME;
        }
    }

    @Override
    public String toString() {
        return "PtrRecord{" +
                "name='" + name + '\'' +
                ", ttl=" + ttl +
                ", ptrName='" + ptrName + '\'' +
                '}';
    }
}
