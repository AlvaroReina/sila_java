package sila_library.discovery;

import java.net.*;
import java.util.Collections;
import java.util.Enumeration;

/**
 * Container for common discovery functionality
 */
public class SiLADiscovery {
    public final static String SILA_MDNS_DOMAIN = "sila-standard.org.";
    public final static String SILA_MDNS_TYPE = "_sila._tcp.";

    /**
     * Gets IP Address that is assigned on a given network interface
     *
     * @implNote Currently only supports IPv4
     *
     * @param interfaceName Name of network interface
     */
    public static InetAddress getInetAddress(String interfaceName) throws SocketException, UnknownHostException {
        try {
            if (interfaceName.matches("local")) {
                return InetAddress.getLocalHost();
            }
            final String interfaceDisplay = "Network interface " + interfaceName;
            final NetworkInterface networkInterface = NetworkInterface.getByName(interfaceName);
            if (networkInterface == null) {
                throw new ConnectException(interfaceDisplay + " doesn't exist on this machine\n"
                        + "Use one of these instead " + NetworkInterface.getNetworkInterfaces().toString());
            }
            if (!networkInterface.isUp())
                throw new ConnectException(interfaceDisplay + " is not up");
            if (networkInterface.isPointToPoint())
                throw new ConnectException(interfaceDisplay + " can not be PtP");

            Enumeration<InetAddress> addresses = networkInterface.getInetAddresses();
            if (!addresses.hasMoreElements())
                throw new ConnectException(interfaceDisplay + " doesnt have assigned IPs");

            for (InetAddress address : Collections.list(addresses)) {
                if (address instanceof Inet4Address)
                    return address;
            }
            throw new ConnectException(interfaceDisplay + " doesn't have an assigned IPv4 address");
        } catch (SocketException | UnknownHostException e) {
            throw e;
        }
    }
}