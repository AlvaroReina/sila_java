package sila_library.discovery;

import lombok.extern.slf4j.Slf4j;

import javax.jmdns.JmDNS;
import javax.jmdns.ServiceInfo;
import java.io.IOException;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;

/**
 * Register SiLAServer on a local network, it constantly checks if the network
 * interface is "healthy" and otherwise tries to reconnect and advertises its
 * services again.
 *
 * @implNote This class can only hold one server registration for now.
 */
@Slf4j
public class SiLAServerRegistration {
    /**
     * Heartbeat to check if the network interface has been disrupted. The callback
     * assumes that switching to a different network takes longer than the sampling period.
     */
    private class HeartbeatAgent implements Runnable {
        private int SAMPLING_PERIOD = 1000; // [ms]

        private boolean active = true;
        private boolean rebootMode = true;
        private int heartBeat = 0;
        private Thread heartBeatThread;

        /**
         * Starts the HeartbeatAgent asynchronously
         */
        public void start() {
            heartBeatThread = new Thread(this, "SiLAServerRegistration_Heartbeat");
            //terminate the thread with the VM.
            heartBeatThread.setDaemon(true);
            heartBeatThread.start();
        }

        @Override
        public void run() {
            while (active) {
                if(Thread.interrupted()) {
                    //to quit from the middle of the loop
                    log.debug("Thread.interrupted()");
                    active = false;
                    return;
                }
                log.debug("heartBeat: {}", heartBeat);
                try {
                    SiLADiscovery.getInetAddress(SiLAServerRegistration.this.interfaceName);

                    if (rebootMode) {
                        rebootJmDNS();
                    }
                } catch (SocketException | UnknownHostException e) {
                    // If the IPv4 address can not be resolved correctly anymore, try re-registering
                    log.error("Connection to " + SiLAServerRegistration.this.interfaceName +
                            " has been disrupted, reconnecting...");
                    rebootMode = true;
                }

                try {
                    Thread.sleep(SAMPLING_PERIOD);
                } catch (InterruptedException e) {
                    // after we call this.heartBeatThread.interrupt() from outside world
                    // the sleep lock is interrupted
                    log.info("[HeartbeatAgent#run] was interrupted");
                    // so now we break the loop before it reaches the while condition again!
                    active = false;
                }
                heartBeat++;
            }
        }

        void rebootJmDNS() {
            try {
                log.info("Rebooting network");
                SiLAServerRegistration.this.closeMDNS();
                SiLAServerRegistration.this.createMDNS();
                log.info("JmDNS re-constructed");
                rebootMode = false;
            } catch (IOException e) {
                rebootMode = true;
            }
        }
    }

    private HeartbeatAgent agent;

    private JmDNS jmdns = null;
    private String interfaceName = null;
    private String name;
    private int port;

    /**
     * Register SiLAServer on certain network connected through the network
     * interface and on certain port where SiLAService should be served.
     *
     * @param name SiLAServer name to be advertised
     * @param interfaceName Name of network interface
     * @param port where SiLAService is exposed/served
     */
    public void RegisterServer(String name, String interfaceName, int port)
            throws IOException {
        this.interfaceName = interfaceName; // Save for heartbeat
        this.name = name;
        this.port = port;

        if (jmdns != null)
            throw new RuntimeException("SiLAServerRegistration can only register one server");

        agent = new HeartbeatAgent();
        agent.start();
    }

    /**
     * Unregister SiLA server that was registered with
     * {@link SiLAServerRegistration#RegisterServer(String, String, int)}
     */
    public void UnregisterServer() {
        if (agent != null) {
            agent.active = false;
            agent.heartBeatThread.interrupt();
        }
        closeMDNS();
    }

    private void closeMDNS() {
        if (jmdns != null) {
            // Unregister all services
            System.out.println( "Unregistering service..." );
            jmdns.unregisterAllServices();
            try {
                jmdns.close();
                jmdns = null;
            } catch (IOException e) {
                System.out.println(e.getMessage());
            }
            System.out.println( "done." );
        }
    }

    private void createMDNS() throws IOException {
        final InetAddress inetAddress = SiLADiscovery.getInetAddress(this.interfaceName);

        jmdns = JmDNS.create(inetAddress);
        jmdns.registerService(ServiceInfo.create(
                SiLADiscovery.SILA_MDNS_TYPE + SiLADiscovery.SILA_MDNS_DOMAIN,
                this.name,
                this.port,
                "SiLA server discovery service"));
    }
}