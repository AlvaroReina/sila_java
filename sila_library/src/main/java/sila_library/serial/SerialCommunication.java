package sila_library.serial;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.concurrent.*;
import java.util.function.Supplier;

import com.fazecast.jSerialComm.SerialPort;
import lombok.extern.slf4j.Slf4j;

import javax.annotation.Nonnull;

/**
 * Serial Communication Class
 *
 * @implNote It automatically finds a USB-to-Serial Adapter and is used for fairly "slow" communication
 * overa serial port
 */
@Slf4j
public class SerialCommunication {
    private SerialPort serialPort = null;
    private OutputStream out = null;

    private final static int INPUT_INTERVAL = 100; // [ms] Interval that input stream is handled
    private InputStream in = null;
    private Thread inputThread = null;

    // @implNote Use StringBuffer as its threadsafe!
    private StringBuffer inputBuffer = new StringBuffer();

    private boolean isSerialUp = false;

    // Thread pool for async read calls
    final private ExecutorService executor = Executors.newCachedThreadPool();

    // we will get a Serial Com Name from outside
    private final String SERIAL_COM_NAME;
    public final static String DEFAULT_SERIAL_COM_NAME = "USB-to-Serial Port";

    public SerialCommunication() { SERIAL_COM_NAME = DEFAULT_SERIAL_COM_NAME; }

    public SerialCommunication(@Nonnull String serialComName) {
        SERIAL_COM_NAME = serialComName;
    }

    /**
     * Heartbeat to re-establish connection
     */
    private class HeartBeatAgent implements Runnable {
        private final Thread heartBeatThread;
        private final int samplingTime;
        private final Supplier<Boolean> connectionTester;

        HeartBeatAgent(int samplingTime, @Nonnull Supplier<Boolean> connectionTester) {
            this.heartBeatThread = new Thread(this, this.getClass().getName() + "_Thread");
            this.samplingTime = samplingTime;
            this.connectionTester = connectionTester;
        }

        void start() {
            this.heartBeatThread.start();
        }

        void stop() {
            this.heartBeatThread.interrupt();
        }

        @Override
        public void run() {
            while (true) {
                try {
                    if (!isSerialUp()) {
                        try {
                            open();
                        } catch (IOException e) {
                            log.info("Serial Comm IO not possible: ", e.getMessage());
                            continue;
                        }
                    } else {
                        if (!connectionTester.get()) {
                            log.info("Connection got lost");
                            close();
                            continue;
                        }
                    }

                    Thread.sleep(this.samplingTime);
                } catch (InterruptedException e) {
                    close();
                    log.error("Driver heart beat interrupted! Possible shutdown emitted.");
                    break;
                }

            }
        }
    }

    private HeartBeatAgent heartBeatAgent = null;

    /**
     * Start Heartbeat that probes the connection and reestablishes it
     *
     * @param samplingTime Minimum time interval to check if connection is up
     * @param connectionTester Boolean check that indicates if connection nis healthy
     */
    public void startHeartbeat(int samplingTime, @Nonnull Supplier<Boolean> connectionTester) {
        log.info("Starting HeartbeatAgent...");

        if (heartBeatAgent != null) {
            heartBeatAgent.stop();
            heartBeatAgent = null;
        }

        heartBeatAgent = new HeartBeatAgent(samplingTime, connectionTester);
        heartBeatAgent.start();

        Runtime.getRuntime().addShutdownHook(
                new Thread(heartBeatAgent::stop)
        );
    }

    public void stopHearbeat() {
        if (heartBeatAgent != null) {
            heartBeatAgent.stop();
        }
    }

    /**
     * Open the Serial Port
     *
     * @implNote Currently using fixed Settings, change API when necessary
     */
    public synchronized void open() throws IOException {
        // If the serial port is up, simply return
        if (isSerialUp)
            return;

        log.info("Open Serial Port");

        serialPort = null;

        // Try to find Serial Adapter (only one supported currently)
        for (SerialPort port : SerialPort.getCommPorts()) {
            if (port.getDescriptivePortName()
                    .contains(SERIAL_COM_NAME)) {
                log.info("Found serial adapter on " +
                        port.getSystemPortName());
                serialPort = port;
                break;
            }
        }

        if (serialPort == null) {
            throw new IOException("Can not find serial adapter!");
        }
        serialPort.openPort();
        if (!serialPort.isOpen()) {
            throw new IOException("Port " +
                    serialPort.getDescriptivePortName() +
                    " cannot be opened. Perhaps permissions " +
                    "(this user has no access to port)?");
        }
        serialPort.setComPortTimeouts(SerialPort.TIMEOUT_READ_SEMI_BLOCKING,
                5000, 0);

        serialPort.setComPortParameters(9600, 8,
                SerialPort.ONE_STOP_BIT, SerialPort.NO_PARITY);
        serialPort.setFlowControl(SerialPort.FLOW_CONTROL_DISABLED);

        // Continuous output flushing (only ends with interrupting)
        out = serialPort.getOutputStream();
        in = serialPort.getInputStream();

        // Continuous appending to input buffer, cleared from synchronized functions
        inputThread = new Thread(()->{
            try {
                while (true) {
                    if (in.available() > 0) {
                        byte[] newData = new byte[in.available()];
                        final int numRead = in.read(newData, 0, newData.length);

                        if (newData.length != numRead) {
                            throw new IOException("Buffer error in stream reading");
                        }

                        final String character = new String(newData, 0, newData.length);
                        this.inputBuffer.append(character);
                    } else {
                        Thread.sleep(INPUT_INTERVAL);
                    }
                }
            } catch ( IOException | InterruptedException e ) {
                log.info(Arrays.toString(e.getStackTrace()));
            }
        });
        inputThread.start();

        isSerialUp = true;
    }

    /**
     * Close the Serial Port
     */
    public synchronized void close() {
        // If serial is already down, simply return
        if (!isSerialUp)
            return;

        isSerialUp = false;

        log.info("Stopping HeartbeatAgent");


        log.info("Closing Serial Port");
        if (out != null) {
            try {
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (inputThread != null) {
            try {
                inputThread.interrupt();
                inputThread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        if (serialPort != null) serialPort.closePort();
    }

    public boolean isSerialUp() {
        return this.isSerialUp;
    }

    /**
     * Send and Receive Messages over Serial Port
     *
     * @param msg Message to serialise and send
     * @param delimiter Delimiter of Messages for send and receive
     * @param timeout Timeout in [ms]
     *
     * @return The answer from the serial port (raw)
     */
    public synchronized String sendReceive(String msg, String delimiter, long timeout) throws IOException {
        if (!isSerialUp) {
            throw new IOException("Serial Communication is not ready to send");
        }

        final String serial_msg = msg + delimiter;
        out.write(serial_msg.getBytes());
        out.flush();
        return read(delimiter, timeout);
    }

    private String read(String delimiter, long timeout ) throws IOException {
        // parallel read task with timeout
        final Callable<String> readTask = () -> {
            while (!this.inputBuffer.toString().contains(delimiter)) {
                Thread.sleep(INPUT_INTERVAL);
            }
            return this.inputBuffer.toString();
        };
        final Future<String> future = this.executor.submit(readTask);

        final String result;
        try {
            result = future.get(timeout, TimeUnit.MILLISECONDS);
        } catch (TimeoutException | InterruptedException ex) {
            throw new IOException("Reading from serial buffer failed.");
        } catch (ExecutionException e) {
            throw new IOException("Executing serial command failed.");
        } finally {
            future.cancel(true);
        }

        // Clear Input Buffer afterwards
        this.inputBuffer.setLength(0);

        return result;
    }
}
