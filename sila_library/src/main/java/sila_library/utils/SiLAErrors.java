package sila_library.utils;

import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;
import io.grpc.Status;
import io.grpc.StatusRuntimeException;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.SiLAFramework;

import javax.annotation.Nonnull;

/**
 * Utility Functions to handle SiLA Errors
 */
@Slf4j
public class SiLAErrors {

    /**
     * Catching a generic Java Exception and generating a SiLA conforming Execution Error
     */
    public static StatusRuntimeException generateGenericExecutionError(@Nonnull Exception e) {
        return generateGRPCError(SiLAFramework.SiLAError.newBuilder()
                .setErrorType(SiLAFramework.SiLAError.ErrorType.EXECUTION)
                .setIdentifier(e.getClass().getName())
                .setMessage(e.getMessage())
                .build()
        );
    }

    /**
     * Generating gRPC Error from built SiLA Error
     *
     * @param siLAError SiLAError proto message
     * @return StatusRuntime according to SiLA
     */
    public static StatusRuntimeException generateGRPCError(
            @Nonnull SiLAFramework.SiLAError siLAError
    ) {
        return new StatusRuntimeException(
                Status.ABORTED.withDescription(
                        siLAError.toByteString().toStringUtf8()
                )
        );
    }

    /**
     * Retrieve SiLAError from gRPC Exception
     *
     * @param statusRuntimeException gRPC Exception occuring during gRPC call
     * @return SiLAError if well formed, null otherwise
     */
    public static SiLAFramework.SiLAError retrieveSiLAError(
        @Nonnull StatusRuntimeException statusRuntimeException
    ) {
        final Status status = statusRuntimeException.getStatus();

        if (!status.getCode().equals(Status.Code.ABORTED)) {
            return null;
        }

        SiLAFramework.SiLAError siLAError = null;
        try {
            siLAError = SiLAFramework.SiLAError.parseFrom(
                    ByteString.copyFromUtf8(status.getDescription())
            );
        } catch (InvalidProtocolBufferException e) {
            log.error(e.getMessage());
        }

        return siLAError;
    }

    /**
     * @param siLAError SiLA Error retrieved from protobuf serialisation
     * @return SiLA Error as String for debugging
     */
    public static String generateSiLAErrorString(SiLAFramework.SiLAError siLAError) {
        return "SiLA ERROR\n" +
                "Type: " + siLAError.getErrorType().toString() + "\n" +
                siLAError.toString();
    }
}
