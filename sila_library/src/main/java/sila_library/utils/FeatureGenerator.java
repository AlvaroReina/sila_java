package sila_library.utils;

import org.apache.commons.io.IOUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import sila_library.models.Feature;

import javax.annotation.Nonnull;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.stream.StreamSource;
import java.io.*;

import static sila_library.utils.Utils.validateFeatureXML;

/**
 * Converts Feature xml to Feature object representation.
 */
public class FeatureGenerator {
    /**
     * Maps from XML Field definitions to JAVA Class Definition
     *
     * @param xmlFile Reader attached to an XML Document
     *
     * @return Feature POJO for Feature Definition
     */
    public static Feature generateFeature(@Nonnull Reader xmlFile) throws IOException {
        // Get String from File and Cleanup Any characters before first element
        final String xmlString = IOUtils.toString(xmlFile)
                .trim()
                .replaceFirst("^(.*?)<","<");
        xmlFile.close();

        // Validate XML with Schema
        try {
            validateFeatureXML(new StreamSource(new StringReader(xmlString)));
        } catch (IOException e) {
            throw new IOException("Schema Validation failed: " + e.getMessage());
        }

        // Unmarshal Feature using JAXB
        final Document xmlDocument = getXMLDocument(new InputSource(new StringReader(xmlString)));
        final NodeList fList = xmlDocument.getElementsByTagName("Feature");

        // @implNote this should be caught by the Schema Validation
        if (fList.getLength() != 1) {
            throw new IOException("Only one <Feature> tag allowed per feature definition!");
        }

        final Node fNode = fList.item(0);

        try {
            final Unmarshaller unmarshaller = JAXBContext.newInstance(Feature.class).createUnmarshaller();
            return (Feature) unmarshaller.unmarshal(fNode);
        } catch (JAXBException e) {
            throw new IOException("Feature Marshalling failed: " + e.getMessage());
        }
    }

    /**
     * Parse W3C XML Document from Inputstream
     *
     * Example usage:
     * ByteString serialDocument;
     * getXMLDocument(new ByteArrayInputStream(serialDocument.toByteArray()));
     *
     * @param xmlFile XML File as an input stream
     * @return W3C XML Document at root of document
     */
    private static Document getXMLDocument(
            @Nonnull InputSource xmlFile) throws IOException {
        Document xmlDocument;
        try {
            xmlDocument = DocumentBuilderFactory.newInstance().
                    newDocumentBuilder().parse(xmlFile);
        } catch (ParserConfigurationException e) {
            throw new RuntimeException(e.getMessage());
        } catch (SAXException | IOException e2) {
            throw new IOException("XML Document coulndn't be parsed properly:\n" +
                    e2.getMessage());
        }
        xmlDocument.getDocumentElement().normalize();
        return xmlDocument;
    }
}
