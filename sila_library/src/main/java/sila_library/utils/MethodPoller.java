package sila_library.utils;

import lombok.NonNull;

import java.time.Duration;
import java.util.concurrent.*;

/**
 * Simple MethodPoller Utility to wait until a condition is met
 *
 * @implNote Default is 1 Second Interval forever
 */
public class MethodPoller {
    private final static ExecutorService executor = Executors.newCachedThreadPool();
    private Duration pollInterval = Duration.ofSeconds(1);
    private Duration maxDuration = Duration.ofMillis(Long.MAX_VALUE);
    private Callable<Boolean> conditionEvaluator = ()->false;

    public static MethodPoller await() {
        return new MethodPoller();
    }

    public MethodPoller withInterval(@NonNull Duration pollInterval) {
        this.pollInterval = pollInterval;
        return this;
    }

    public MethodPoller atMost(@NonNull Duration maxDuration) {
        this.maxDuration = maxDuration;
        return this;
    }

    public MethodPoller until(@NonNull Callable<Boolean> conditionEvaluator) {
        this.conditionEvaluator = conditionEvaluator;
        return this;
    }

    public void execute() throws ExecutionException, TimeoutException {
        final Future<Void> future = executor.submit(
                ()->{
                    while(!conditionEvaluator.call()) {
                        Thread.sleep(pollInterval.toMillis());
                    }
                    return null;
                }
        );

        try {
            future.get(maxDuration.toMillis(), TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        } catch (TimeoutException e) {
            future.cancel(true);
            if (!future.isCancelled()) {
                throw new ExecutionException("Condition Evaluator can not be cancelled", e.getCause());
            }
            throw e;
        }
    }
}
