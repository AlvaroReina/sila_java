package sila_library.utils;

import org.apache.commons.configuration2.Configuration;
import org.apache.commons.configuration2.builder.fluent.Configurations;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.xml.sax.SAXException;
import sila_base.EmptyClass;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.xml.XMLConstants;
import javax.xml.transform.Source;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.*;
import java.net.*;
import java.nio.charset.StandardCharsets;
import java.util.Properties;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;

/**
 * Container for simple utilities
 */
public class Utils {
    /**
     * Blocking call until 'stop' is typed on terminal
     */
    public static void blockUntilStop() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Type stop to stop the application");
        Boolean run = true;
        while (run) {
            if (sc.hasNextLine()) {
                String line = sc.nextLine();
                switch (line) {
                case "stop":
                    run = false;
                    break;
                default:
                    System.out.println("Unknown command:" + line);
                }
            }
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                System.err.println(e);
            }
        }
        System.out.println("stopping ...");
    }

    /**
     * Check if there if server is running by checking the silaservice.
     * Used mainly by the Heartbeat to see if we have stale entries of servers.
     *
     * @param host of endpoint / socket
     * @param port of endpoint /socket
     * @param connectTimeout timeout for the ping
     * @param log for logging errors (can be null)
     * @return Boolean to indicate if connection is up or not
     */
    public static Boolean checkIfSocketIsOpen(
            @Nonnull String host,
            int port,
            int connectTimeout,
            @Nullable Logger log) {
        final SocketAddress socketAddress = new InetSocketAddress(host, port);
        final Socket socket = new Socket();
        boolean online = true;
        try {
            socket.connect(socketAddress, connectTimeout);
        } catch (IOException iOException) {
            online = false;
        } finally {
            try {
                socket.close();
            } catch (IOException ex) {
                log.error(ex.getMessage());
            }

        }
        return online;
    }

    /**
     * Get File content converted to String
     *
     * @param resourceStream Resource as Stream
     * @return File content as String
     */
    public static String getFileString(@Nonnull InputStream resourceStream) throws IOException {
        return IOUtils.toString(resourceStream, StandardCharsets.UTF_8);
    }

    /**
     * Validate Feature XML
     *
     * @param xmlSource XML File Source
     *
     * @throws IOException Thrown if not correctly validated
     */
    public static void validateFeatureXML(@Nonnull Source xmlSource) throws IOException {
        try {
            final URL schemaLocation = EmptyClass.class
                    .getResource("/sila_base/schema/FeatureDefinition.xsd");
            final SchemaFactory schemaFactory = SchemaFactory
                    .newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            final Schema schema = schemaFactory.newSchema(schemaLocation);
            schema.newValidator().validate(xmlSource);
        } catch (SAXException e) {
            throw new IOException(e.getMessage());
        }
    }

    /**
     * @implNote Currently not used, but only directly validated XML used
     *
     * @param inXMLStr to check if XML or not
     * @return true of the string is XML, false otherwise
     */
    public static boolean isXMLLike(String inXMLStr) {
        Pattern pattern;
        Matcher matcher;

        // Check if it has XML Elements
        final String XML_PATTERN_STR = "<([^\\?;]+)>[\\S\\s]+</\\1>";

        // check if valid string at all
        if (inXMLStr != null && inXMLStr.trim().length() > 0) {
            // Check XML tags
            if (inXMLStr.trim().startsWith("<")) {
                pattern = Pattern.compile(XML_PATTERN_STR,
                        Pattern.CASE_INSENSITIVE | Pattern.DOTALL | Pattern.MULTILINE);

                matcher = pattern.matcher(inXMLStr);

                return matcher.find();
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * Get Configuration File in current Class Context
     *
     * @param fileName Name of properties file
     * @return Configurations class
     */
    public static Configuration getLocalConfiguration(String fileName) {
        return getConfiguration(Thread.currentThread().getContextClassLoader(), fileName);
    }

    /**
     * Get Configuration File with given Class Loader
     *
     * @param classLoader Loader that can access the property file
     * @param fileName Name of properties file
     * @return Configurations class
     */
    public static Configuration getConfiguration(ClassLoader classLoader, String fileName) {
        final URL fileURL = classLoader.getResource(fileName);
        if (fileURL == null) {
            final String errMsg = fileName + " couldn't be found!";
            throw new IllegalStateException(errMsg);
        }

        try {
            return new Configurations().properties(fileURL);
        } catch (ConfigurationException e) {
            throw new IllegalStateException(e);
        }
    }
}
