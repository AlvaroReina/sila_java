package sila_library.utils;

import com.fazecast.jSerialComm.SerialPort;

import static java.lang.System.out;

public class ListSerialComInterfaces {

    public static void main(String args[]) {
        ListSerialComInterfaces.display();
        System.exit(0);
    }

    public static void display() {
        out.println("=====================================================");
        out.println("   Connected USB-SERIAL Communication Interfaces:");
        for (SerialPort port : SerialPort.getCommPorts())
            displayInterfaceInformation(port);
        out.println("=====================================================\n");
    }

    static void displayInterfaceInformation(SerialPort port) {
        out.printf("    System Port Name: /dev/%s\n", port.getSystemPortName());
        out.printf("    Descriptive Port Name: %s\n", port.getDescriptivePortName());
        out.printf("\nNote: \nPlease note some OS's use port name, others descriptive name.\n" +
                " Example: -s [string] :: string can be a substring of the name found (name must contain string).\n");
    }
}
