package sila_features;

import com.google.gson.JsonObject;
import io.grpc.Status;
import io.grpc.StatusRuntimeException;

import javax.annotation.Nonnull;
import javax.xml.transform.stream.StreamSource;
import java.io.IOException;
import java.io.StringReader;
import java.util.HashMap;
import java.util.Map;

import io.grpc.stub.StreamObserver;
import lombok.Value;
import lombok.extern.slf4j.Slf4j;

import sila2.org.silastandard.SiLAFramework;
import sila2.org.silastandard.core.silaservice.v1.SiLAServiceGrpc;
import sila2.org.silastandard.core.silaservice.v1.SiLAServiceOuterClass;

import static sila_library.utils.SiLAErrors.generateGRPCError;
import static sila_library.utils.Utils.validateFeatureXML;

/**
 * SiLAService Implementation used by other SiLA servers
 */
@Slf4j
public class SiLAServiceServer {
    private Map<String, String> featureDefinitions = new HashMap<>();
    private ServerInfo serverInfo = null;
    private boolean serverRegistered = false;

    @Value
    public static class ServerInfo {
        public String serverDisplayName;
        public String serverDescription;
        public String serverVendorURL;
    }

    /**
     * Registers a server to the SiLA Service
     *
     * @implNote Overwrites whatever registration has been made
     *
     * @param serverInfo         Server Information exposed by the SiLAService
     * @param featureDefinitions Map with FeatureIdentifier as Keys and and the the FDL Content as Values
     */
    public void registerServer(
            @Nonnull ServerInfo serverInfo,
            @Nonnull Map<String, String> featureDefinitions
    ) {
        serverRegistered = false;
        this.serverInfo = serverInfo;

        final String serverDisplayName = serverInfo.serverDisplayName;
        log.info(
                "[registerServer] register serverName={}, featureDefinitions={} ...",
                serverDisplayName,
                featureDefinitions.keySet()
        );

        final String errorPrefix = "Error registering feature definition for server=" + serverDisplayName;

        // Get serialised feature definition
        for (String key : featureDefinitions.keySet()) {
            try {
                final String featureDefinition = featureDefinitions.get(key);

                validateFeatureXML(new StreamSource(new StringReader(featureDefinition)));
                this.featureDefinitions.put(key, featureDefinition);
                log.info(
                        "[registerServer] serverName={} feature={} is read from XML string.",
                        serverDisplayName,
                        key
                );
            } catch (IOException e) {
                throw new IllegalArgumentException(errorPrefix + ": feature='"
                        + key + "' is not valid", e);
            }
        }
        log.info(
                "[registerServer] serverName={} with features successfully registered!",
                serverDisplayName
        );
        serverRegistered = true;
    }

    /**
     * Gets gRPC Service to serve
     *
     * @return bindable service
     */
    public io.grpc.BindableService getService() {
        return new ServiceImpl();
    }

    /**
     * gRPC Service Implementation
     */
    private class ServiceImpl extends SiLAServiceGrpc.SiLAServiceImplBase {
        private StatusRuntimeException registrationError = generateGRPCError(
                SiLAFramework.SiLAError.newBuilder()
                        .setErrorType(SiLAFramework.SiLAError.ErrorType.EXECUTION)
                        .setIdentifier("NoServerRegistered")
                        .setMessage("SiLAService can not function as no server has been registered succesfully.")
                        .build()
        );

        @Override
        public void getFeatureDefinition(
                SiLAServiceOuterClass.GetFeatureDefinition_Parameters req,
                StreamObserver<SiLAServiceOuterClass.GetFeatureDefinition_Responses> responseObserver
        ) {
            final String featureIdentifier = req
                    .getQualifiedFeatureIdentifier()
                    .getIdentifier()
                    .getValue();

            log.info(
                    "[{}][getFeatureDefinition] feature={} entered.",
                    serverInfo.serverDisplayName,
                    featureIdentifier
            );

            if (!serverRegistered) {
                responseObserver.onError(registrationError);
                return;
            }

            final String featureDefinitionContent =
                    SiLAServiceServer.this.featureDefinitions.get(featureIdentifier);

            if (featureDefinitionContent == null) {
                JsonObject validationError = new JsonObject();
                validationError.addProperty("Parameter", "FeatureIdentifier");
                validationError.addProperty("Message", "Feature definition for " +
                        featureIdentifier + " doesn't exist, " +
                        " existing Features: " + featureDefinitions.keySet());

                responseObserver.onError(new StatusRuntimeException(Status.INVALID_ARGUMENT
                        .withDescription(validationError.toString())));
                return;
            }
            responseObserver.onNext(
                    SiLAServiceOuterClass.GetFeatureDefinition_Responses.newBuilder()
                            .setFeatureDefinition(
                                    SiLAServiceOuterClass.DataType_FeatureDefinition
                                            .newBuilder()
                                            .setFeatureDefinition(
                                                    SiLAFramework.String.newBuilder()
                                                            .setValue(featureDefinitionContent)
                                                            .build()
                                                    )
                                            .build()
                            )
                            .build()
            );
            responseObserver.onCompleted();
            log.info(
                    "[{}][getFeatureDefinition] feature={} returned={}.",
                    serverInfo.serverDisplayName,
                    featureIdentifier,
                    featureDefinitionContent
            );
        }

        @Override
        public void getServerDisplayName(
                SiLAServiceOuterClass.Get_ServerDisplayName_Parameters req,
                StreamObserver<SiLAServiceOuterClass.Get_ServerDisplayName_Responses> responseObserver
        ) {
            log.info("getServerDisplayName being called");

            if (!serverRegistered) {
                responseObserver.onError(registrationError);
                return;
            }
            responseObserver.onNext(
                    SiLAServiceOuterClass.Get_ServerDisplayName_Responses.newBuilder()
                            .setServerDisplayName(
                                    SiLAFramework.String.newBuilder()
                                            .setValue(serverInfo.serverDisplayName)
                                            .build()

                            )
                            .build()
            );
            responseObserver.onCompleted();
        }

        @Override
        public void getServerDescription(
                SiLAServiceOuterClass.Get_ServerDescription_Parameters req,
                StreamObserver<SiLAServiceOuterClass.Get_ServerDescription_Responses> responseObserver
        ) {
            log.info("getServerDescription being called");

            if (!serverRegistered) {
                responseObserver.onError(registrationError);
                return;
            }
            responseObserver.onNext(
                    SiLAServiceOuterClass.Get_ServerDescription_Responses.newBuilder()
                        .setServerDescription(
                                SiLAFramework.String.newBuilder()
                                        .setValue(serverInfo.serverDescription)
                                        .build()
                        )
                        .build()
            );
            responseObserver.onCompleted();
        }

        @Override
        public void getServerVersion(
                SiLAServiceOuterClass.Get_ServerVersion_Parameters req,
                StreamObserver<SiLAServiceOuterClass.Get_ServerVersion_Responses> responseObserver
        ) {
            log.info("getServerVersion being called");

            // @TODO: Implement real server versioning

            if (!serverRegistered) {
                responseObserver.onError(registrationError);
                return;
            }

            responseObserver.onNext(
                    SiLAServiceOuterClass.Get_ServerVersion_Responses.newBuilder()
                            .setServerVersion(
                                    SiLAFramework.String.newBuilder()
                                            .setValue("v0.0")
                                            .build()
                            )
                            .build()
            );
            responseObserver.onCompleted();
        }

        @Override
        public void getServerVendorURL(
                SiLAServiceOuterClass.Get_ServerVendorURL_Parameters req,
                StreamObserver<SiLAServiceOuterClass.Get_ServerVendorURL_Responses> responseObserver
        ) {
            log.info("getServerVendorName being called");

            if (!serverRegistered) {
                responseObserver.onError(registrationError);
                return;
            }

            responseObserver.onNext(SiLAServiceOuterClass.Get_ServerVendorURL_Responses.newBuilder()
                    .setServerVendorURL(
                            SiLAServiceOuterClass.DataType_URL.newBuilder()
                                    .setURL(
                                            SiLAFramework.String.newBuilder()
                                                    .setValue(serverInfo.serverVendorURL)
                                                    .build()
                                    )
                                    .build()
                    )
                    .build());
            responseObserver.onCompleted();
        }

        @Override
        public void getImplementedFeatures(
                SiLAServiceOuterClass.Get_ImplementedFeatures_Parameters req,
                StreamObserver<SiLAServiceOuterClass.Get_ImplementedFeatures_Responses> responseObserver
        ) {
            log.info("getImplementedFeatures being called");

            if (!serverRegistered) {
                responseObserver.onError(registrationError);
                return;
            }

            SiLAServiceOuterClass.Get_ImplementedFeatures_Responses.Builder id_builder =
                    SiLAServiceOuterClass.Get_ImplementedFeatures_Responses.newBuilder();
            for (String feature_id : featureDefinitions.keySet()) {
                id_builder.addImplementedFeatures(
                        SiLAServiceOuterClass.DataType_Identifier.newBuilder()
                            .setIdentifier(
                                    SiLAFramework.String.newBuilder()
                                            .setValue(feature_id)
                                            .build()
                                    )
                            .build()
                );
            }
            SiLAServiceOuterClass.Get_ImplementedFeatures_Responses response = id_builder.build();
            responseObserver.onNext(id_builder.build());
            responseObserver.onCompleted();
            log.info(
                    "[{}][getImplementedFeatures] returned featuresList={}",
                    serverInfo.serverDisplayName,
                    response.getImplementedFeaturesList()
            );
        }
    }
}