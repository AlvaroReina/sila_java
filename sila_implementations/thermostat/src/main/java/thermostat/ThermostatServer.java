package thermostat;

import io.grpc.Context;
import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.SiLAFramework;
import sila2.org.silastandard.examples.temperaturecontroller.v1.TemperatureControllerGrpc;
import sila2.org.silastandard.examples.temperaturecontroller.v1.TemperatureControllerOuterClass;
import sila_base.EmptyClass;
import sila_features.SiLAServiceServer;
import sila_tools.bridge.server.SiLAServerCore;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.TimeUnit;

import static sila_library.utils.SiLAErrors.generateGRPCError;
import static sila_library.utils.Utils.blockUntilStop;
import static sila_library.utils.Utils.getFileString;
import static thermostat.ThermostatSimulation.KELVIN_ACCURACY;
import static thermostat.ThermostatSimulation.KELVIN_PER_SECONDS;

/**
 * Example Implementation of a SiLA Server with Observable Commands
 *
 * @implNote A lot of functionality should be provided by the sila_library
 */
@Slf4j
public class ThermostatServer {
    final static String SERVER_NAME = "ThermostatServer";
    private final static int SERVER_PORT = 50052; // Default
    private final static int EXECUTION_LIFETIME = 1000; // [s]

    private final static String featureLocation =
            "/sila_base/feature_definitions/org/silastandard/examples/TemperatureController.xml";

    private interface ExecutionStateListener {
        void onExecutionState(SiLAFramework.ExecutionState executionState);
    }

    private class ThermostatExecution implements ThermostatSimulation.TemperatureListener {
        public boolean active = false;
        public double startTemperature;
        public double targetTemperature;
        public SiLAFramework.ExecutionState lastExecutionState = null;
        public List<ExecutionStateListener> executionStateListeners = new CopyOnWriteArrayList<>();

        @Override
        public void temperatureChanged(double temperature) {
            if (active) {
                updateExecutionState(generateExecutionState(temperature));
            }
        }

        public SiLAFramework.ExecutionState generateExecutionState(double temperature) {
            final double absoluteTemperatureDifference = Math.abs(targetTemperature - temperature);

            final SiLAFramework.ExecutionState.Builder builder = SiLAFramework.ExecutionState.newBuilder();
            if (absoluteTemperatureDifference >= KELVIN_ACCURACY) {
                builder.setCommandStatus(SiLAFramework.ExecutionState.CommandStatus.running)
                        .setProgress(SiLAFramework.Real.newBuilder().setValue(
                                1.0 - (absoluteTemperatureDifference/(targetTemperature - startTemperature))
                        ).build())
                        .setEstimatedRemainingTime(
                                SiLAFramework.Duration.newBuilder()
                                        .setSeconds((long) (absoluteTemperatureDifference / KELVIN_PER_SECONDS))
                                        .setNanos(0)
                        );
            } else {
                builder.setCommandStatus(SiLAFramework.ExecutionState.CommandStatus.finishedSuccessfully)
                        .setProgress(SiLAFramework.Real.newBuilder().setValue(1.0).build())
                        .setEstimatedRemainingTime(SiLAFramework.Duration.newBuilder()
                                .setSeconds(0)
                                .setNanos(0));
            }

            return builder.build();
        }

        public void updateExecutionState(@Nonnull SiLAFramework.ExecutionState executionState) {
            lastExecutionState = executionState;
            for (ExecutionStateListener executionStateListener : executionStateListeners) {
                executionStateListener.onExecutionState(executionState);
            }
        }
    }

    private final Map<UUID, ThermostatExecution> executionStateMap = new ConcurrentHashMap<>();
    private final SiLAServerCore siLAServerCore = new SiLAServerCore();
    private final ThermostatSimulation thermostatSimulation = new ThermostatSimulation();

    private void start(@Nonnull String interfaceName, int serverPort) {
        thermostatSimulation.start();

        try {
            Map<String, String> fdl = new HashMap<String, String>() {
                {
                    put(
                            "TemperatureController",
                            getFileString(EmptyClass.class.getResourceAsStream(featureLocation))
                    );
                }
            };

            this.siLAServerCore.start(
                    SERVER_NAME,
                    new SiLAServiceServer.ServerInfo(
                            "Thermostat Server",
                            "Simple Example of a Thermostat",
                            "www.sila-standard.org"
                    ),
                    fdl, serverPort, interfaceName,
                    new TemperatureControlImpl()
            );
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    public static void main(String[] args) {
        final String usage = "Usage: (interface) (serverPort)";

        String interfaceName = "local";
        int serverPort = SERVER_PORT;

        if (args.length > 0) {
            interfaceName = args[0];
        }
        if (args.length > 1) {
            serverPort = Integer.valueOf(args[2]);
        }
        if (args.length > 2) {
            throw new IllegalArgumentException(usage);
        }

        // Start Server
        final ThermostatServer server = new ThermostatServer();
        server.start(interfaceName, serverPort);
        blockUntilStop();
        System.out.println("termination complete.");
    }


    class TemperatureControlImpl extends TemperatureControllerGrpc.TemperatureControllerImplBase {
        @Override
        public void controlTemperature(TemperatureControllerOuterClass.ControlTemperature_Parameters parameters,
                StreamObserver<SiLAFramework.CommandConfirmation> commandConfirmationStreamObserver) {
            // @TODO: Validation Errors
            final double targetTemperature = parameters.getTargetTemperature().getValue();

            // Create Execution
            final ThermostatSimulation thermostatSimulation = ThermostatServer.this.thermostatSimulation;

            // Check if any other execution is running and kill it
            for (ThermostatExecution thermostatExecution : executionStateMap.values()) {
                if (thermostatExecution.active) {
                    thermostatExecution.active = false;
                    thermostatExecution.updateExecutionState(
                            SiLAFramework.ExecutionState.newBuilder()
                                    .setCommandStatus(SiLAFramework.ExecutionState.CommandStatus.finishedWithError)
                                    .setError(
                                            SiLAFramework.SiLAError.newBuilder()
                                                    .setErrorType(SiLAFramework.SiLAError.ErrorType.EXECUTION)
                                                    .setIdentifier("ControlInterrupted")
                                                    .setMessage("Execution was interrupted and can not finish the control.")
                                                    .build()
                                    )
                            .build()
                    );
                }
            }

            // Create Execution
            final UUID newUUID = UUID.randomUUID();
            final ThermostatExecution thermostatExecution = new ThermostatExecution();
            thermostatExecution.active = true;
            thermostatExecution.startTemperature = thermostatSimulation.getCurrentTemperature();
            thermostatExecution.targetTemperature = targetTemperature;
            thermostatExecution.lastExecutionState = thermostatExecution.generateExecutionState(thermostatSimulation.getCurrentTemperature());

            thermostatSimulation.addListener(thermostatExecution);

            final Timer deletionTimer = new Timer();
            deletionTimer.schedule(new TimerTask() {
                public void run() {
                    log.info("Deleting execution " + newUUID.toString());
                    executionStateMap.remove(newUUID);
                }
            }, TimeUnit.SECONDS.toMillis(EXECUTION_LIFETIME));

            // Starting execution
            thermostatSimulation.setTargetTemperature(targetTemperature);
            executionStateMap.put(newUUID, thermostatExecution);

            commandConfirmationStreamObserver.onNext(SiLAFramework.CommandConfirmation.newBuilder()
                    .setCommandId(SiLAFramework.CommandExecutionUUID.newBuilder().setCommandId(newUUID.toString()))
                    .setLifetimeOfExecution(SiLAFramework.Duration.newBuilder()
                            .setSeconds(EXECUTION_LIFETIME)
                            .setNanos(0)
                    ).build());
            commandConfirmationStreamObserver.onCompleted();
        }

        @Override
        public void controlTemperatureState(SiLAFramework.CommandExecutionUUID commandExecutionUUID,
                StreamObserver<SiLAFramework.ExecutionState> executionStateStreamObserver) {
            final UUID executionKey = UUID.fromString(commandExecutionUUID.getCommandId());

            if (!executionStateMap.containsKey(executionKey)) {
                executionStateStreamObserver.onError(
                        generateGRPCError(SiLAFramework.SiLAError.newBuilder()
                                .setErrorType(SiLAFramework.SiLAError.ErrorType.FRAMEWORK)
                                .setIdentifier("InvalidCommandExecutionUUID")
                                .setMessage(commandExecutionUUID + " not found on server.")
                                .build())
                );
                return;
            }

            final ThermostatExecution thermostatExecution = executionStateMap.get(executionKey);

            // Always send back current Execution State
            executionStateStreamObserver.onNext(thermostatExecution.lastExecutionState);

            // Subscribe to Changes
            final ExecutionStateListener executionStateListener = executionStateStreamObserver::onNext;

            thermostatExecution.executionStateListeners.add(executionStateListener);
            log.info("[controlTemperatureState] Subscription started");

            // Wait until call has been cancelled by client
            while (!Context.current().isCancelled()) {
                if (!executionStateMap.containsKey(executionKey)) {
                    executionStateStreamObserver.onError(
                            generateGRPCError(SiLAFramework.SiLAError.newBuilder()
                                    .setErrorType(SiLAFramework.SiLAError.ErrorType.FRAMEWORK)
                                    .setIdentifier("InvalidCommandExecutionUUID")
                                    .setMessage(commandExecutionUUID + " not found on server.")
                                    .build())
                    );
                    break;
                }

                if (thermostatExecution.lastExecutionState.getCommandStatus().equals(
                        SiLAFramework.ExecutionState.CommandStatus.finishedSuccessfully) ||
                        thermostatExecution.lastExecutionState.getCommandStatus().equals(
                                SiLAFramework.ExecutionState.CommandStatus.finishedWithError)
                ) {
                    break;
                }

                try {
                    TimeUnit.SECONDS.sleep(1);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }

            thermostatExecution.executionStateListeners.remove(executionStateListener);
            log.info("[controlTemperatureState] Subscription cancelled");
            executionStateStreamObserver.onCompleted();
        }



        @Override
        public void controlTemperatureResult(SiLAFramework.CommandExecutionUUID commandExecutionUUID,
                StreamObserver<TemperatureControllerOuterClass.ControlTemperature_Responses> responsesStreamObserver) {
            final UUID executionKey = UUID.fromString(commandExecutionUUID.getCommandId());

            if (!executionStateMap.containsKey(executionKey)) {
                responsesStreamObserver.onError(
                        generateGRPCError(SiLAFramework.SiLAError.newBuilder()
                                .setErrorType(SiLAFramework.SiLAError.ErrorType.FRAMEWORK)
                                .setIdentifier("InvalidCommandExecutionUUID")
                                .setMessage(commandExecutionUUID + " not found on server.")
                                .build())
                );
                return;
            }

            final SiLAFramework.ExecutionState.CommandStatus commandStatus =
                    executionStateMap.get(executionKey).lastExecutionState.getCommandStatus();

            if (!commandStatus.equals(
                    SiLAFramework.ExecutionState.CommandStatus.finishedSuccessfully)) {
                responsesStreamObserver.onError(
                        generateGRPCError(SiLAFramework.SiLAError.newBuilder()
                                .setErrorType(SiLAFramework.SiLAError.ErrorType.FRAMEWORK)
                                .setIdentifier("CommandExecutionNotFinished")
                                .setMessage(commandExecutionUUID + " not finished succesfully.")
                                .build())
                );
                return;
            }

            responsesStreamObserver.onNext(TemperatureControllerOuterClass.ControlTemperature_Responses.newBuilder().build());
            responsesStreamObserver.onCompleted();
        }

        @Override
        public void subscribeCurrentTemperature(TemperatureControllerOuterClass.Subscribe_CurrentTemperature_Parameters parameters,
                StreamObserver<TemperatureControllerOuterClass.Subscribe_CurrentTemperature_Responses> responsesStreamObserver) {
            final ThermostatSimulation thermostatSimulation = ThermostatServer.this.thermostatSimulation;

            // Always send back current Temperature
            sendTemperature(responsesStreamObserver, thermostatSimulation.getCurrentTemperature());
            // Subscribe to Changes
            final ThermostatSimulation.TemperatureListener temperatureListener =
                    temperature->sendTemperature(responsesStreamObserver, temperature);
            thermostatSimulation.addListener(temperatureListener);
            log.info("[subscribeCurrentTemperature] Subscription started");

            // Wait until call has been cancelled by client
            while (!Context.current().isCancelled()) {
                try {
                    TimeUnit.SECONDS.sleep(1);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }

            thermostatSimulation.removeListener(temperatureListener);
            log.info("[subscribeCurrentTemperature] Subscription cancelled");
            responsesStreamObserver.onCompleted();
        }

        // Helper to send back temperature
        private void sendTemperature(StreamObserver<TemperatureControllerOuterClass.Subscribe_CurrentTemperature_Responses> responsesStreamObserver,
                double temperature) {
            responsesStreamObserver.onNext(
                    TemperatureControllerOuterClass.Subscribe_CurrentTemperature_Responses
                            .newBuilder()
                            .setCurrentTemperature(
                                    SiLAFramework.Real.newBuilder().setValue(temperature).build())
                            .build()
            );
        }
    }
}
