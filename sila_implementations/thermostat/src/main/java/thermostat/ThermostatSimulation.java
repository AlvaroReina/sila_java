package thermostat;

import lombok.extern.slf4j.Slf4j;

import javax.annotation.Nonnull;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.TimeUnit;

@Slf4j
public class ThermostatSimulation {
    public static double KELVIN_PER_SECONDS = 1; // [K/s] Update ramp
    public static double UPDATE_INTERVAL = 1; // [s] Interval temperature is updated
    public static double KELVIN_ACCURACY = 1; // [K] When control is assumed to be finished

    private volatile double currentTemperature = 293; // [K]
    private volatile double targetTemperature = 293; // [K]

    public interface TemperatureListener {
        void temperatureChanged(double temperature);
    }

    private class TemperatureChangeAgent implements Runnable {
        private double KELVIN_RAMP = KELVIN_PER_SECONDS * UPDATE_INTERVAL; // [Kelvin / step]
        private boolean active = true;

        @Override
        public void run() {
            while (active) {
                final long startTime = System.currentTimeMillis();
                if(Thread.interrupted()) {
                    //to quit from the middle of the loop
                    log.info("Thread.interrupted()");
                    active = false;
                    return;
                }

                // Linear Increase to target temperature until accuracy reached
                final double temperatureDifference = (targetTemperature - currentTemperature);
                final double absoluteTemperatureDifference = Math.abs(temperatureDifference);

                if (absoluteTemperatureDifference >= KELVIN_ACCURACY) {
                    if (KELVIN_RAMP > absoluteTemperatureDifference) {
                        currentTemperature = targetTemperature;
                    } else {
                        currentTemperature += Math.signum(temperatureDifference) * KELVIN_RAMP;
                    }

                    for (TemperatureListener temperatureListener : ThermostatSimulation.this.temperatureListenerList) {
                        temperatureListener.temperatureChanged(currentTemperature);
                    }
                }

                // Best effort to keep the update interval
                final long remainingTime =  TimeUnit.SECONDS.toMillis((long) UPDATE_INTERVAL) -
                        (System.currentTimeMillis() - startTime);
                try {
                    if (remainingTime > 0) Thread.sleep(remainingTime);
                } catch (InterruptedException e) {
                    active = false;
                }
            }
        }
    }

    private List<TemperatureListener> temperatureListenerList = new CopyOnWriteArrayList<>();
    private Thread agentThread = null;

    /**
     * Starts the TemperatureChangeAgent asynchronously
     */
    public void start() {
        if (agentThread != null) {
            throw new IllegalStateException("Can only be called once during the lifetime of ThermostatSimulation");
        }

        agentThread = new Thread(new TemperatureChangeAgent(),
                ThermostatSimulation.class.getName() + "_" + TemperatureChangeAgent.class.getName());
        //terminate the thread with the VM.
        agentThread.setDaemon(true);
        agentThread.start();
    }

    public void setTargetTemperature(double targetTemperature) {
        this.targetTemperature = targetTemperature;
    }

    public double getCurrentTemperature() {
        return this.currentTemperature;
    }

    public void addListener(@Nonnull TemperatureListener listener) {
        temperatureListenerList.add(listener);
    }

    public void removeListener(@Nonnull TemperatureListener listener) {
        temperatureListenerList.remove(listener);
    }
}
