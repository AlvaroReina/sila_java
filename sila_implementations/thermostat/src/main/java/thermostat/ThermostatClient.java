package thermostat;

import io.grpc.Context;
import io.grpc.ManagedChannel;
import sila2.org.silastandard.SiLAFramework;
import sila2.org.silastandard.examples.temperaturecontroller.v1.TemperatureControllerGrpc;
import sila2.org.silastandard.examples.temperaturecontroller.v1.TemperatureControllerOuterClass;
import sila_library.discovery.SiLAServerDiscovery;

import java.util.Collections;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;

public class ThermostatClient {
    public static void main(String[] args) throws InterruptedException {
        SiLAServerDiscovery.enable();

        // Blocking call to find server
        final ManagedChannel serviceChannel = SiLAServerDiscovery.blockAndGetChannel(ThermostatServer.SERVER_NAME);
        final TemperatureControllerGrpc.TemperatureControllerBlockingStub blockingStub =
                TemperatureControllerGrpc.newBlockingStub(serviceChannel);

        // Set Temperature
        final SiLAFramework.CommandConfirmation commandConfirmation = blockingStub.controlTemperature(TemperatureControllerOuterClass.ControlTemperature_Parameters.newBuilder()
                .setTargetTemperature(
                        SiLAFramework.Real.newBuilder()
                                .setValue(313)
                                .build())
                .build());

        // Example of subscription of normal operation
        final Context.CancellableContext cancellableContext = Context.current().withCancellation();

        cancellableContext.run(()->{
            final Iterator<TemperatureControllerOuterClass.Subscribe_CurrentTemperature_Responses> temperatureIterator =
                    blockingStub.subscribeCurrentTemperature(
                            TemperatureControllerOuterClass.Subscribe_CurrentTemperature_Parameters
                                    .newBuilder().build()
                    );

            final Iterator<SiLAFramework.ExecutionState> executionStateIterator =
                    blockingStub.controlTemperatureState(commandConfirmation.getCommandId());

            // Get Execution State
            SiLAFramework.ExecutionState executionState = executionStateIterator.next();
            System.out.println("Initial Execution State: " + executionState.toString());


            // Show subscription value until finished
            while (executionState.getCommandStatus().equals(SiLAFramework.ExecutionState.CommandStatus.running)) {
                executionState = executionStateIterator.next();
                System.out.println("Got Command Status: " + executionState.toString());
                System.out.println("Got temperature " + temperatureIterator.next());
            }
        });

        // This cancels the subscriptions from the client side
        cancellableContext.cancel(null);
        System.out.println("Stopping...");
        serviceChannel.shutdown().awaitTermination(5, TimeUnit.SECONDS);
        System.out.println("Stopped...");
    }
}
