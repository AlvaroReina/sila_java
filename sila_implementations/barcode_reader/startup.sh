#!/usr/bin/env bash

if [ "$#" -ne 2 ]; then
    echo "Usage: ./startup.sh [<path to .unitelabs>] [network-interface]"
    exit
fi

ulhome=$1
interface=$2

# Startup script to be added to /etc/rc.local of any
# pi which is to be running these components
# Also used for auto launch upon deployment
java -jar ${ulhome}/barcode_reader/barcode-reader-1.0.0-SNAPSHOT.jar ${interface}