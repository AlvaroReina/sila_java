#!/usr/bin/env bash

# exit from the script if any command returns non-zero
set -e
# Only for debugging to print the command being run (Super Handy!)
# set -x

# parse cmd line args
if [ "$#" -ne 1 ]; then
    echo "Usage: ./deploy.sh [user@hostname]"
    exit
fi
hostname=$1

barcode_dir="~/.unitelabs/barcode_reader"

ssh ${hostname} -t "mkdir -p ${barcode_dir}"

# Push the latest built libraries (dependencies) and jar's for this module
rsync -av target/*.jar ${hostname}:${barcode_dir}
rsync -av target/lib/* ${hostname}:${barcode_dir}/lib
rsync -av startup.sh ${hostname}:${barcode_dir}/startup.sh
