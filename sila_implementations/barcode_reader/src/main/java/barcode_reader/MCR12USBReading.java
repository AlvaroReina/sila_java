package barcode_reader;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Hex;

@Slf4j
class MCR12USBReading {
    private static final String USB_FD = "/dev/hidraw0";

    /**
     * Read Barcode from fixed File Descriptor
     *
     * @implNote Blocking Call
     */
    static String getBarcode() throws IOException {
        FileInputStream usbStream = null;
        try {
            final File usbFile = new File(USB_FD);
            usbStream = new FileInputStream(usbFile);

            final StringBuilder stringBuilder = new StringBuilder();

            while(true) {
                final byte[] data = new byte[16];
                usbStream.read(data);

                final String hexData = Hex.encodeHexString(data);

                log.info("Hex Data: " + hexData);

                // If key_map doesn't contain, continue
                if (!KEY_MAP.containsKey(hexData)) {
                    continue;
                }

                final String barcodeData = KEY_MAP.get(hexData);

                log.info("Appending data: " + barcodeData);
                if (barcodeData.equals("EOT1")) {
                    break;
                } else {
                    stringBuilder.append(barcodeData);
                }
            }

            return stringBuilder.toString();
        } finally {
            if (usbStream != null) usbStream.close();
        }
    }

    /**
     * Key Map of Hexcode to String
     */
    private static final Map<String, String> KEY_MAP;
    static {
        Map<String, String> aMap = new HashMap<>();
        aMap.put("00002700000000000000000000000000", "0");
        aMap.put("00001e00000000000000000000000000", "1");
        aMap.put("00001f00000000000000000000000000", "2");
        aMap.put("00002000000000000000000000000000", "3");
        aMap.put("00002100000000000000000000000000", "4");
        aMap.put("00002200000000000000000000000000", "5");
        aMap.put("00002300000000000000000000000000", "6");
        aMap.put("00002400000000000000000000000000", "7");
        aMap.put("00002500000000000000000000000000", "8");
        aMap.put("00002600000000000000000000000000", "9");
        aMap.put("00000400000000000000000000000000", "a");
        aMap.put("00000500000000000000000000000000", "b");
        aMap.put("00000600000000000000000000000000", "c");
        aMap.put("00000700000000000000000000000000", "d");
        aMap.put("00000800000000000000000000000000", "e");
        aMap.put("00000900000000000000000000000000", "f");
        aMap.put("00000a00000000000000000000000000", "g");
        aMap.put("00000b00000000000000000000000000", "h");
        aMap.put("00000c00000000000000000000000000", "i");
        aMap.put("00000d00000000000000000000000000", "j");
        aMap.put("00000e00000000000000000000000000", "k");
        aMap.put("00000f00000000000000000000000000", "l");
        aMap.put("00001000000000000000000000000000", "m");
        aMap.put("00001100000000000000000000000000", "n");
        aMap.put("00001200000000000000000000000000", "o");
        aMap.put("00001300000000000000000000000000", "p");
        aMap.put("00001400000000000000000000000000", "q");
        aMap.put("00001500000000000000000000000000", "r");
        aMap.put("00001600000000000000000000000000", "s");
        aMap.put("00001700000000000000000000000000", "t");
        aMap.put("00001800000000000000000000000000", "u");
        aMap.put("00001900000000000000000000000000", "v");
        aMap.put("00001a00000000000000000000000000", "w");
        aMap.put("00001b00000000000000000000000000", "x");
        aMap.put("00001c00000000000000000000000000", "y");
        aMap.put("00001d00000000000000000000000000", "z");
        aMap.put("02000400000000000000000000000000", "A");
        aMap.put("02000500000000000000000000000000", "B");
        aMap.put("02000600000000000000000000000000", "C");
        aMap.put("02000700000000000000000000000000", "D");
        aMap.put("02000800000000000000000000000000", "E");
        aMap.put("02000900000000000000000000000000", "F");
        aMap.put("02000a00000000000000000000000000", "G");
        aMap.put("02000b00000000000000000000000000", "H");
        aMap.put("02000c00000000000000000000000000", "I");
        aMap.put("02000d00000000000000000000000000", "J");
        aMap.put("02000e00000000000000000000000000", "K");
        aMap.put("02000f00000000000000000000000000", "L");
        aMap.put("02001000000000000000000000000000", "M");
        aMap.put("02001100000000000000000000000000", "N");
        aMap.put("02001200000000000000000000000000", "O");
        aMap.put("02001300000000000000000000000000", "P");
        aMap.put("02001400000000000000000000000000", "Q");
        aMap.put("02001500000000000000000000000000", "R");
        aMap.put("02001600000000000000000000000000", "S");
        aMap.put("02001700000000000000000000000000", "T");
        aMap.put("02001800000000000000000000000000", "U");
        aMap.put("02001900000000000000000000000000", "V");
        aMap.put("02001a00000000000000000000000000", "W");
        aMap.put("02001b00000000000000000000000000", "X");
        aMap.put("02001c00000000000000000000000000", "Y");
        aMap.put("02001d00000000000000000000000000", "Z");
        aMap.put("02001e00000000000000000000000000", "!");
        aMap.put("02003400000000000000000000000000", "\"");
        aMap.put("02002000000000000000000000000000", "#");
        aMap.put("02002100000000000000000000000000", "$");
        aMap.put("02002200000000000000000000000000", "%");
        aMap.put("02002400000000000000000000000000", "&");
        aMap.put("00003100000000000000000000000000", "\\");
        aMap.put("00003400000000000000000000000000", "\'");
        aMap.put("02002600000000000000000000000000", "(");
        aMap.put("02002700000000000000000000000000", ")");
        aMap.put("02002500000000000000000000000000", "*");
        aMap.put("02002e00000000000000000000000000", "+");
        aMap.put("00003600000000000000000000000000", ",");
        aMap.put("00002d00000000000000000000000000", "-");
        aMap.put("00003700000000000000000000000000", ".");
        aMap.put("00003800000000000000000000000000", "/");
        aMap.put("02003300000000000000000000000000", ":");
        aMap.put("00003300000000000000000000000000", ";");
        aMap.put("02003800000000000000000000000000", "?");
        aMap.put("02001f00000000000000000000000000", "@");
        aMap.put("00002f00000000000000000000000000", "[");
        aMap.put("00003000000000000000000000000000", "]");
        aMap.put("02002300000000000000000000000000", "^");
        aMap.put("02002d00000000000000000000000000", "_");
        aMap.put("02003100000000000000000000000000", "|");

        aMap.put("00002800000000000000000000000000", "EOT1");
        aMap.put("01000d00000000000000000000000000", "EOT2");

        KEY_MAP = Collections.unmodifiableMap(aMap);
    }
}