package barcode_reader;

public class BarcodeNotFoundException extends Exception {
    public BarcodeNotFoundException() { super(); }
    public BarcodeNotFoundException(String message) { super(message); }
}
