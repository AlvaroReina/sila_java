package barcode_reader;

import com.pi4j.io.gpio.*;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.*;

@Slf4j
public class BarcodeReaderDriver {
    // Broadcom GPIO pin numbering scheme (check with "gpio readall")
    private static final Pin RASPI_PIN = RaspiPin.GPIO_29; // Note: This is Pin 21 on BCM Scheme

    private final GpioController gpio;
    private final GpioPinDigitalOutput gpioPin;

    public BarcodeReaderDriver() {
        gpio = GpioFactory.getInstance();
        gpio.setShutdownOptions(true, PinState.LOW);

        gpioPin = gpio.provisionDigitalOutputPin(RASPI_PIN, "Barcode_Trigger", PinState.LOW);
    }

    /**
     * Synchronized Barcode Reading
     * @param timeout Timeout until Exception is thrown
     */
    public synchronized String getBarcode(long timeout) throws BarcodeNotFoundException {
        // Start Read Buffer
        final ExecutorService executor = Executors.newCachedThreadPool();
        final Future<String> future = executor.submit(MCR12USBReading::getBarcode);

        gpioPin.high();

        String result = null;
        try {
            result = future.get(timeout, TimeUnit.SECONDS);
        } catch (TimeoutException e) {
            log.error(e.getMessage());
            throw new BarcodeNotFoundException("No barcode found within " +
                    String.valueOf(timeout) + " seconds.");
        } catch (InterruptedException | ExecutionException e) {
            log.error(e.getMessage());
            throw new BarcodeNotFoundException("Failed unexpectedly.");
        } finally {
            future.cancel(true);
            gpioPin.low();
        }

        if (result == null || result.isEmpty()) {
            throw new BarcodeNotFoundException("No barcode found");
        }

        if (result.trim().isEmpty()) {
            throw new BarcodeNotFoundException("Barcode was in unexpected format. Ensure that the 1D barcode is in supported format.");
        }

        return result;
    }

    /**
     * Main Function trying to read a code in one second
     */
    public static void main(String[] args) {
        final BarcodeReaderDriver barcodeReaderDriver = new BarcodeReaderDriver();

        try {
            final String result = barcodeReaderDriver.getBarcode(1);
            System.out.println("Barcode found: " + result);
        } catch (BarcodeNotFoundException e) {
            System.out.println("No barcode found: " + e.getMessage());
        }
    }
}
