package barcode_reader;

import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.SiLAFramework;
import sila2.org.silastandard.core.barcodeprovider.v1.BarcodeProviderGrpc;
import sila2.org.silastandard.core.barcodeprovider.v1.BarcodeProviderOuterClass;
import sila_features.SiLAServiceServer;
import sila_tools.bridge.server.SiLAServerCore;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static sila_library.utils.SiLAErrors.generateGRPCError;
import static sila_library.utils.Utils.blockUntilStop;
import static sila_library.utils.Utils.getFileString;

@Slf4j
public class BarcodeReaderServer {
    private final static String SERVER_NAME = "BarcodeReader";
    private final static int BRIDGE_PORT = 50051; // Default
    private final static int SERVER_PORT = 50052; // Default

    private final static int FIXED_TIMEOUT = 1; // [s] Fixed timeout for barcode reading
    private final static int MAX_TIMEOUT = 20; // [s] Maximum allowed timeout for barcode reading

    private final BarcodeReaderDriver barcodeReaderDriver;
    private final SiLAServerCore siLAServerCore = new SiLAServerCore();

    BarcodeReaderServer() {
        this.barcodeReaderDriver = new BarcodeReaderDriver();
    }

    private void start(@Nonnull String interfaceName, int serverPort) {
        try {
            Map<String, String> fdl = new HashMap<String, String>() {
                {
                    put(
                            "BarcodeProvider",
                            getFileString(this.getClass().getResourceAsStream("/BarcodeProvider.xml"))
                    );
                }
            };

            this.siLAServerCore.start(
                    SERVER_NAME,
                    new SiLAServiceServer.ServerInfo(
                            "Barcode Reader MCR12",
                            "A small, camera-based barcode reader with red illumination. Reads most barcode standards.",
                            "www.unitelabs.ch"
                    ),
                    fdl, serverPort, interfaceName,
                    new ReaderImpl()
            );
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    public static void main(String[] args) throws IOException {
        final String usage = "Usage: (interface) (serverPort)";

        String interfaceName = "local";
        int serverPort = SERVER_PORT;

        if (args.length > 0) {
            interfaceName = args[0];
        }
        if (args.length > 1) {
            serverPort = Integer.valueOf(args[2]);
        }
        if (args.length > 2) {
            throw new IllegalArgumentException(usage);
        }

        // Start Server
        final BarcodeReaderServer server = new BarcodeReaderServer();
        server.start(interfaceName, serverPort);
        blockUntilStop();
        System.out.println("termination complete.");
    }

    class ReaderImpl extends BarcodeProviderGrpc.BarcodeProviderImplBase {
        @Override
        public void readCode(BarcodeProviderOuterClass.ReadCode_Parameters readCodeParameters,
                StreamObserver<BarcodeProviderOuterClass.ReadCode_Responses> responseObserver) {
            log.info("Trying to read barcode...");
            try {
                final String code = BarcodeReaderServer.this.barcodeReaderDriver
                        .getBarcode(FIXED_TIMEOUT);

                BarcodeProviderOuterClass.ReadCode_Responses response = BarcodeProviderOuterClass.ReadCode_Responses
                        .newBuilder()
                        .setBarcode(SiLAFramework.String
                                .newBuilder()
                                .setValue(code)
                                .build()
                        ).build();
                responseObserver.onNext(response);
                responseObserver.onCompleted();
            } catch (BarcodeNotFoundException e) {
                responseObserver.onError(
                        generateGRPCError(SiLAFramework.SiLAError.newBuilder()
                                .setErrorType(SiLAFramework.SiLAError.ErrorType.EXECUTION)
                                .setIdentifier("BarcodeNotFound")
                                .setMessage(e.getMessage())
                                .build())
                );
            }
        }

        @Override
        public void readUntil(BarcodeProviderOuterClass.ReadUntil_Parameters readUntilParameters,
                StreamObserver<BarcodeProviderOuterClass.ReadUntil_Responses> responseObserver) {
            final long timeout = readUntilParameters.getTimeout().getValue();

            if (timeout < 0) {
                responseObserver.onError(
                        generateGRPCError(SiLAFramework.SiLAError.newBuilder()
                                .setErrorType(SiLAFramework.SiLAError.ErrorType.VALIDATION)
                                .setIdentifier("Timeout")
                                .setMessage("Timeout can not be negative!")
                                .build())
                );
                return;
            } else if (timeout > MAX_TIMEOUT) {
                responseObserver.onError(
                        generateGRPCError(SiLAFramework.SiLAError.newBuilder()
                                .setErrorType(SiLAFramework.SiLAError.ErrorType.VALIDATION)
                                .setIdentifier("Timeout")
                                .setMessage("Timeout can not be larger than " + String.valueOf(MAX_TIMEOUT))
                                .build())
                );
                return;
            }

            try {
                final String code = BarcodeReaderServer.this.barcodeReaderDriver
                        .getBarcode(timeout);

                BarcodeProviderOuterClass.ReadUntil_Responses response = BarcodeProviderOuterClass.ReadUntil_Responses
                        .newBuilder()
                        .setBarcode(SiLAFramework.String
                                .newBuilder()
                                .setValue(code)
                                .build()
                        ).build();
                responseObserver.onNext(response);
                responseObserver.onCompleted();

            } catch (BarcodeNotFoundException e) {
                responseObserver.onError(
                        generateGRPCError(SiLAFramework.SiLAError.newBuilder()
                                .setErrorType(SiLAFramework.SiLAError.ErrorType.EXECUTION)
                                .setIdentifier("BarcodeNotFound")
                                .setMessage(e.getMessage())
                                .build())
                );
            }
        }
    }
}
