# Barcode reader for MCR12 
Driver example for an off-the-shelf MCR12 Barcode Scanner with a GPIO trigger
hardware modification. Runs on Raspberry Pi Model 3B.

In Idorsia: `pi@192.168.1.2`

## Pi4J Wiring Problem

We had to install the latest snapshot version of the Pi4J library to be able to use the current
kernel version (check with `uname -a`) of Raspbian 
[forum reference](https://www.raspberrypi.org/forums/viewtopic.php?t=182191). The snapshot version
does not come with WiringPi pre-packaged, so you have to install it from source on the Raspberry Pi.

### WiringPi Installation

Install it from source with `tag 2.46` or `hash 8d188fa0e00bb8c6ff6eddd07bf92857e9bd533a` according
to their instructions [link](http://wiringpi.com/download-and-install/).



