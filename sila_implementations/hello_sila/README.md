# HelloSiLA JAVA Implementation
The goal is to show how a SiLA server can be implemented in JAVA.

## Building and Running

To install the package, simply use maven:

    mvn install
    
Then the JARs can be deployed on machines running JDK 8, and both server and client
can be tested:

    cd target/
    java -cp hello-sila-1.0.0-SNAPSHOT-jar-with-dependencies.jar hello_sila.HelloSiLAServer (interfaceName) (port)
    java -cp hello-sila-1.0.0-SNAPSHOT-jar-with-dependencies.jar hello_sila.HelloSiLAClient (interfaceName)
    
The first argument is the interface name the discovery should work on. If none
is given, then "local" is assumed.

You can run the server and client on any host on your local network.