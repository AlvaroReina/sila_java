package hello_sila;

import io.grpc.ManagedChannel;
import io.grpc.StatusRuntimeException;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import sila2.org.silastandard.SiLAFramework;
import sila2.org.silastandard.examples.greetingprovider.v1.GreetingProviderGrpc;
import sila2.org.silastandard.examples.greetingprovider.v1.GreetingProviderOuterClass;
import sila2.org.silastandard.core.silaservice.v1.SiLAServiceGrpc;
import sila2.org.silastandard.core.silaservice.v1.SiLAServiceOuterClass;
import sila_library.discovery.SiLAServerDiscovery;

import java.util.List;
import java.util.concurrent.TimeoutException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import static sila_library.utils.SiLAErrors.retrieveSiLAError;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class HelloSiLAServerTest {
    private final static String INTERFACE = "local";

    // @TODO: Check why the timeout has to be so high
    private final static int DISCOVERY_TIMEOUT = 20000; // [ms]

    private ManagedChannel channel;
    private GreetingProviderGrpc.GreetingProviderBlockingStub blockingStub;

    @BeforeAll
    void HelloSiLAServerTest() throws TimeoutException {
        final HelloSiLAServer server = new HelloSiLAServer();
        server.start(INTERFACE, HelloSiLAServer.SERVER_PORT);

        SiLAServerDiscovery.enable();

        // Blocking call to find server
        this.channel = SiLAServerDiscovery.blockAndGetChannel(HelloSiLAServer.SERVER_NAME, DISCOVERY_TIMEOUT);
        this.blockingStub = GreetingProviderGrpc.newBlockingStub(this.channel);
    }

    @Test
    void testSiLAService() {
        final SiLAServiceGrpc.SiLAServiceBlockingStub serviceStub = SiLAServiceGrpc
                .newBlockingStub(this.channel);

        final List<SiLAServiceOuterClass.DataType_Identifier> featureIdentifierList = serviceStub
                .getImplementedFeatures(SiLAServiceOuterClass.Get_ImplementedFeatures_Parameters.newBuilder().build()).getImplementedFeaturesList();

        assertTrue(featureIdentifierList.size() == 1);

        for (SiLAServiceOuterClass.DataType_Identifier featureIdentifier : featureIdentifierList) {
            assertEquals("GreetingProvider", featureIdentifier.getIdentifier().getValue());
        }
    }

    @Test
    void testHello() {
        final GreetingProviderOuterClass.SayHello_Parameters.Builder parameter =
                GreetingProviderOuterClass.SayHello_Parameters.newBuilder();

        final String testName = "SiLA";
        final GreetingProviderOuterClass.SayHello_Responses result = blockingStub.sayHello(parameter.setName(
                SiLAFramework.String.newBuilder()
                        .setValue(testName)
        ).build());

        assertEquals("Hello " + testName, result.getGreeting().getValue());
    }

    @Test
    void testEmptyParameter() {
        final GreetingProviderOuterClass.SayHello_Parameters.Builder parameter =
                GreetingProviderOuterClass.SayHello_Parameters.newBuilder();

        // Has to throw a validation error
        try {
            blockingStub.sayHello(parameter.build());
        } catch (StatusRuntimeException e) {
            final SiLAFramework.SiLAError siLAError = retrieveSiLAError(e);
            assertEquals(SiLAFramework.SiLAError.ErrorType.VALIDATION, siLAError.getErrorType());
            return;
        }
        fail("HelloSiLAServer did not throw a validation error with empty parameter");
    }
}
