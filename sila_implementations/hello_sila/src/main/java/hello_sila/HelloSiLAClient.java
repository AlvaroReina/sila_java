package hello_sila;

import com.google.protobuf.InvalidProtocolBufferException;
import io.grpc.ManagedChannel;
import io.grpc.StatusRuntimeException;
import sila2.org.silastandard.SiLAFramework;
import sila2.org.silastandard.examples.greetingprovider.v1.GreetingProviderGrpc;
import sila2.org.silastandard.examples.greetingprovider.v1.GreetingProviderOuterClass;
import sila2.org.silastandard.core.silaservice.v1.SiLAServiceGrpc;
import sila2.org.silastandard.core.silaservice.v1.SiLAServiceOuterClass;
import sila_library.discovery.SiLAServerDiscovery;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static sila_library.utils.SiLAErrors.generateSiLAErrorString;
import static sila_library.utils.SiLAErrors.retrieveSiLAError;

/**
 * Client to retrieve greeting from the {@link HelloSiLAServer}.
 */
public class HelloSiLAClient {
    private GreetingProviderGrpc.GreetingProviderBlockingStub blockingStub;

    private void buildStub(final ManagedChannel channel) {
        this.blockingStub = GreetingProviderGrpc.newBlockingStub(channel);
    }

    /**
     * Say hello to server.
     */
    private void greet(String name) throws InvalidProtocolBufferException {
        System.out.println("Will try to greet " + name + " ...");
        GreetingProviderOuterClass.SayHello_Parameters.Builder parameter = GreetingProviderOuterClass.SayHello_Parameters.newBuilder();
        GreetingProviderOuterClass.SayHello_Responses result;

        try {
            result = blockingStub.sayHello(parameter.setName(
                    SiLAFramework.String.newBuilder()
                            .setValue(name)
            ).build());
        } catch (StatusRuntimeException e) {
            final SiLAFramework.SiLAError siLAError = retrieveSiLAError(e);

            if (siLAError == null) {
                throw new RuntimeException("Not A SiLA Error: " + e.getMessage());
            }

            System.out.println(generateSiLAErrorString(siLAError));
            return;
        }

        System.out.println("Greeting: " + result.getGreeting().getValue());

        System.out.println("Will try to get start year...");
        GreetingProviderOuterClass.Get_StartYear_Responses startYear;
        try {
            startYear = blockingStub.getStartYear(
                    GreetingProviderOuterClass.Get_StartYear_Parameters.newBuilder().build()
            );
        } catch (StatusRuntimeException e) {
            final SiLAFramework.SiLAError siLAError = retrieveSiLAError(e);

            if (siLAError == null) {
                throw new RuntimeException("Not A SiLA Error: " + e.getMessage());
            }

            System.out.println(generateSiLAErrorString(siLAError));
            return;
        }
        System.out.println("Start Year: " + String.valueOf(startYear.getStartYear()));
    }

    public static void main(String[] args) throws InvalidProtocolBufferException, InterruptedException {
        HelloSiLAClient client = new HelloSiLAClient();
        SiLAServerDiscovery.enable();

        // Blocking call to find server
        final ManagedChannel serviceChannel = SiLAServerDiscovery.blockAndGetChannel(HelloSiLAServer.SERVER_NAME);
        SiLAServiceGrpc.SiLAServiceBlockingStub serviceStub = SiLAServiceGrpc
                .newBlockingStub(serviceChannel);

        System.out.println("Found Features:");
        List<SiLAServiceOuterClass.DataType_Identifier> featureIdentifierList = serviceStub
                .getImplementedFeatures(SiLAServiceOuterClass.Get_ImplementedFeatures_Parameters.newBuilder().build()).getImplementedFeaturesList();

        for (SiLAServiceOuterClass.DataType_Identifier featureIdentifier : featureIdentifierList) {
            System.out.println("\t" + featureIdentifier.getIdentifier());
        }

        // Use the discovered channel
        client.buildStub(serviceChannel);
        try {
            String user = "SiLA";
            client.greet(user);
        } finally {
            serviceChannel.shutdown().awaitTermination(5, TimeUnit.SECONDS);
        }
    }
}