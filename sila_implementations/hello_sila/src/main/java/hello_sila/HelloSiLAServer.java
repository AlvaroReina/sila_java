package hello_sila;

import io.grpc.stub.StreamObserver;
import sila2.org.silastandard.SiLAFramework;
import sila2.org.silastandard.examples.greetingprovider.v1.GreetingProviderGrpc;
import sila2.org.silastandard.examples.greetingprovider.v1.GreetingProviderOuterClass;
import sila_base.EmptyClass;
import sila_features.SiLAServiceServer;
import sila_tools.bridge.server.SiLAServerCore;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.time.Year;
import java.util.*;

import static sila_library.utils.SiLAErrors.generateGRPCError;
import static sila_library.utils.Utils.blockUntilStop;
import static sila_library.utils.Utils.getFileString;

/**
 * Example SiLA Server using mandatory SiLA Capabilities
 */
public class HelloSiLAServer {
    final static String SERVER_NAME = "HelloSiLAServer";
    final static int SERVER_PORT = 50052; // Default

    private final static String featureLocation =
            "/sila_base/feature_definitions/org/silastandard/examples/GreetingProvider.xml";

    private final SiLAServerCore siLAServerCore = new SiLAServerCore();

    void start(@Nonnull String interfaceName, int serverPort) {
        try {
            Map<String, String> fdl = new HashMap<String, String>() {
                {
                    put(
                            "GreetingProvider",
                            getFileString(EmptyClass.class.getResourceAsStream(featureLocation))
                    );
                }
            };

            this.siLAServerCore.start(
                    SERVER_NAME,
                    new SiLAServiceServer.ServerInfo(
                            "Hello SiLA Server",
                            "Simple Example of a SiLA Server",
                            "www.sila-standard.org"
                    ),
                    fdl, serverPort, interfaceName,
                    new GreeterImpl()
            );
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    public static void main(String[] args) {
        final String usage = "Usage: (interface) (serverPort)";

        String interfaceName = "local";
        int serverPort = SERVER_PORT;

        if (args.length > 0) {
            interfaceName = args[0];
        }
        if (args.length > 1) {
            serverPort = Integer.valueOf(args[1]);
        }
        if (args.length > 2) {
            throw new IllegalArgumentException(usage);
        }

        // Start Server
        final HelloSiLAServer server = new HelloSiLAServer();
        server.start(interfaceName, serverPort);
        blockUntilStop();
        System.out.println("termination complete.");
    }

    static class GreeterImpl extends GreetingProviderGrpc.GreetingProviderImplBase {
        @Override
        public void sayHello(
                GreetingProviderOuterClass.SayHello_Parameters req,
                StreamObserver<GreetingProviderOuterClass.SayHello_Responses> responseObserver
        ) {
            if (!req.hasName()) {
                responseObserver.onError(
                        generateGRPCError(SiLAFramework.SiLAError.newBuilder()
                                .setErrorType(SiLAFramework.SiLAError.ErrorType.VALIDATION)
                                .setIdentifier("Name")
                                .setMessage("Name parameter was not set.")
                                .build())
                );
                return;
            }

            // Custom ValidationError example
            String name = req.getName().getValue();
            if ("error".equalsIgnoreCase(name)) {
                responseObserver.onError(
                        generateGRPCError(SiLAFramework.SiLAError.newBuilder()
                                .setErrorType(SiLAFramework.SiLAError.ErrorType.VALIDATION)
                                .setIdentifier("Name")
                                .setMessage("Name was called error therefore throw an error :)")
                                .build())
                );
                return;
            }

            final String msg = "Hello " + name;
            GreetingProviderOuterClass.SayHello_Responses result =
                    GreetingProviderOuterClass.SayHello_Responses
                            .newBuilder()
                            .setGreeting(SiLAFramework.String.newBuilder()
                                    .setValue(msg)
                            )
                            .build();
            responseObserver.onNext(result);
            responseObserver.onCompleted();
            System.out.println("Request received on " + SERVER_NAME + " " + msg);
        }

        @Override
        public void getStartYear(
                GreetingProviderOuterClass.Get_StartYear_Parameters request,
                StreamObserver<GreetingProviderOuterClass.Get_StartYear_Responses> responseObserver
        ) {
            responseObserver.onNext(
                    GreetingProviderOuterClass.Get_StartYear_Responses.newBuilder()
                            .setStartYear(
                                    SiLAFramework.Integer.newBuilder()
                                            .setValue(Year.now().getValue())
                                    )
                            .build()
            );
            responseObserver.onCompleted();
        }
    }
}
