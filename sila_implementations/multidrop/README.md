# Multidrop
SiLA Java Multidrop server and respective client, compatible with MultidropMicro.

The server can be started with:

```bash
java -cp multidrop-1.0.0-SNAPSHOT-jar-with-dependencies.jar multidrop.MultidropServer wlan0
```

The driver has been tested on Ubuntu 16.04 and Raspbian.

## Todo
* Support Different Labware Types (96 well and 384 well).
* Re-design feature with observable command.