package multidrop;

import io.grpc.ManagedChannel;
import sila2.org.silastandard.SiLAFramework;
import sila2.org.silastandard.core.shakecontroller.v1.ShakeControllerGrpc;
import sila2.org.silastandard.core.shakecontroller.v1.ShakeControllerOuterClass;
import sila_library.discovery.SiLAServerDiscovery;

import java.util.Collections;
import java.util.concurrent.TimeUnit;

/**
 * Client to call Multidrop Server
 */
public class MultidropClient {
    public static void main(String[] args) throws Exception {
        SiLAServerDiscovery.enable();

        // Blocking call to find server
        final ManagedChannel channel = SiLAServerDiscovery.blockAndGetChannel(MultidropServer.SERVER_NAME);
        final ShakeControllerGrpc.ShakeControllerBlockingStub blockingStub =
                ShakeControllerGrpc.newBlockingStub(channel);

        // Call the server and try to shake
        try {
            blockingStub.shake(ShakeControllerOuterClass.Shake_Parameters.newBuilder()
                    .setDuration(SiLAFramework.Integer.newBuilder()
                            .setValue(1) // one second
                    ).build());
        } finally {
            channel.shutdown().awaitTermination(5, TimeUnit.SECONDS);
        }
    }
}