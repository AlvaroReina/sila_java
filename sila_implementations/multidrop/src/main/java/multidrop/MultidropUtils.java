package multidrop;

import java.util.HashMap;
import java.util.Map;

class MultidropUtils {
    static int MAX_VOLUME = 100; // [ml]
    static int MAX_COLUMN = 12; // [-]
    static int MAX_SHAKE_TIME = 20; // [s]

    enum DeviceType {
        MultidropCombi, Multidrop384, MultidropDW, MultidropMicro
    }

    class PlateType {
        static final int WellPlate_96 = 0, WellPlate_384 = 1;
    }

    class MultiDropError {
        String message;
        String action;
        String code;

        MultiDropError(String code, String message, String action) {
            this.code = code;
            this.message = message;
            this.action = action;
        }
    }

    static final Map<String, MultiDropError> ERRORS = new HashMap<>();
    {
        ERRORS.put("ER1",  new MultiDropError("ER1", "1 - Internal firmware error. Contact service.", "E")); // EnterInError
        ERRORS.put("ER2",  new MultiDropError("ER2", "2 - The instrument did not recognize the command it received. Contact the SiLAConverter vendor.", "E")); // EnterInError
        ERRORS.put("ER3",  new MultiDropError("ER3", "3 - The arguments of the received command are not valid. Contact the SiLAConverter vendor.", "E")); // EnterInError
        ERRORS.put("ER4",  new MultiDropError("ER4", "4 - Pump position error. Reduce dispensing speed or, if using the small tube cassette, use standard tube cassette instead. If the problem persists even with normal viscosity fluids, contact service.", "AA")); // AbortAll
        ERRORS.put("ER5",  new MultiDropError("ER5", "5 - Plate X position error. If this error is reported repeatedly, contact service.", "AA")); // AbortAll
        ERRORS.put("ER6",  new MultiDropError("ER6", "6 - Plate Y position error. If this error is reported repeatedly, contact service.", "AA")); // AbortAll
        ERRORS.put("ER7",  new MultiDropError("ER7", "7 - Z position error. If this error is reported repeatedly, contact service.", "AA")); // AbortAll
        ERRORS.put("ER8",  new MultiDropError("ER8", "8 - Selector position error. If this error is reported repeatedly, contact service.", "AA")); // AbortAll
        ERRORS.put("ER9",  new MultiDropError("ER9", "9 - Attempt to reset the serial number. Do not try to reprogram the serial number.", "AA")); // AbortAll
        ERRORS.put("ER10", new MultiDropError("ER10", "10 - Nonvolatile parameters lost. Contact service.", "E")); // EnterInError
        ERRORS.put("ER11", new MultiDropError("ER11", "11 - No more memory for storing user data. Release some user data no longer needed.", "AA")); // AbortAll
        ERRORS.put("ER12", new MultiDropError("ER12", "12 - A previous command is executing in the background. Do not use commands interfering with the ongoing operation without first stopping the operation.", "AA")); // AbortAll
        ERRORS.put("ER13", new MultiDropError("ER13", "13 - X and Z positions conflict. Contact service.", "E")); // EnterInError
        ERRORS.put("ER14", new MultiDropError("ER14", "14 - Cannot dispense when pump not primed. Prime the pump before dispensing.", "PRIR")); // PrimeRetry
        ERRORS.put("ER15", new MultiDropError("ER15", "15 - Missing prime vessel. Insert the priming vessel.", "R")); // Retry
        ERRORS.put("ER16", new MultiDropError("ER16", "16 - Rotor shield not in place. Slide the rotor cover in place.", "R")); // Retry
        ERRORS.put("ER17", new MultiDropError("ER17", "17 - Dispense volume for all wells is 0. Select a nonezero volume for at least one plate well.", "AA")); // AbortAll
        ERRORS.put("ER18", new MultiDropError("ER18", "18 - Cannot dispense because the plate type is invalid (bad plate index). Use a valid plate index.", "AA")); // AbortAll
        ERRORS.put("ER19", new MultiDropError("ER19", "19 - Cannot dispense because the plate has not beend defined. Define the plate.", "E")); // EnterInError
        ERRORS.put("ER20", new MultiDropError("ER20", "20 - Invalid rows in plate definition. Check the WPL or PLA command.", "E")); // EnterInError
        ERRORS.put("ER21", new MultiDropError("ER21", "21 - Invalid columns in plate definition. Check the WPL or PLA command.", "E")); // EnterInError
        ERRORS.put("ER22", new MultiDropError("ER22", "22 - Plate height is invalid. Check the WPL or PLA command.", "E")); // EnterInError
        ERRORS.put("ER23", new MultiDropError("ER23", "23 - Plate well volume is invalid (too small or too big). Check the WPL or PLA command.", "E")); // EnterInError
        ERRORS.put("ER24", new MultiDropError("ER24", "24 - The cassette type is invalid (bad cassette index). Use a valid cassette index.", "AA")); // AbortAll
        ERRORS.put("ER25", new MultiDropError("ER25", "25 - Cassette not defined. Define the cassette.", "E")); // EnterInError
        ERRORS.put("ER26", new MultiDropError("ER26", "26 - Volume increment defined for cassette is invalid.", "E")); // EnterInError
        ERRORS.put("ER27", new MultiDropError("ER27", "27 - Maximum volume defined for the cassette is invalid.", "E")); // EnterInError
        ERRORS.put("ER28", new MultiDropError("ER28", "28 - Minimum volume defined for the cassette is invalid.", "E")); // EnterInError
        ERRORS.put("ER29", new MultiDropError("ER29", "29 - Minimum or maximum pump speed defined for the cassette is invalid.", "E")); // EnterInError
        ERRORS.put("ER30", new MultiDropError("ER30", "30 - Pump rotor offset in the cassete definition is invalid.", "E")); // EnterInError
        ERRORS.put("ER32", new MultiDropError("ER32", "32 - Dispensing volume is not within limits allowed for the cassette. Correct the volume or use a different cassette.", "AA")); // AbortAll
        ERRORS.put("ER33", new MultiDropError("ER33", "33 - Invalid selector channel. Correct the selector channel number.", "AA")); // AbortAll
        ERRORS.put("ER34", new MultiDropError("ER34", "34 - Invalid dispensing speed. Use a valid value.", "AA")); // AbortAll
        ERRORS.put("ER35", new MultiDropError("ER35", "35 - Invalid dispensing height. Correct the dispensing heigth.", "AA")); // AbortAll
        ERRORS.put("ER36", new MultiDropError("ER36", "36 - Invalid predispense volume. Change the predispense volume of the protocol.", "AA")); // AbortAll
        ERRORS.put("ER37", new MultiDropError("ER37", "37 - Invalid dispensing order of the plate. Use dispensing order 0 or 1.", "AA")); // AbortAll
        ERRORS.put("ER38", new MultiDropError("ER38", "38 - Invalid X or Y dispensing offset. Fix the offsets. Note that the limits depend on the used plate type.", "AA")); // AbortAll
        ERRORS.put("ER39", new MultiDropError("ER39", "39 - RFID option not present. Purchase the RFID option for the instrument.", "E")); // EnterInError
        ERRORS.put("ER40", new MultiDropError("ER40", "40 - RFID tag not present. Insert a cassette type containing a RFID tag.", "E")); // EnterInError
        ERRORS.put("ER41", new MultiDropError("ER41", "41 - RFID tag data checksum is incorrect. If the cassette has never been calibrated, calibrate it. Else discard the cassette.", "E")); // EnterInError
        ERRORS.put("ER42", new MultiDropError("ER42", "42 - Selector valve not installed. Do not try to operate the selector unless it is installed.", "E")); // EnterInError
        ERRORS.put("ER43", new MultiDropError("ER43", "43 - Wrong cassette type. Use a cassette type matching the cassette type of the current protocol.", "AA")); // AbortAll
        ERRORS.put("ER44", new MultiDropError("ER44", "44 - Protocol, plate or liquid profile may not be modified or deleted because it is currenty used.", "E")); // EnterInError
        ERRORS.put("ER45", new MultiDropError("ER45", "45 - Cannot modify or delete protocol or a plate because it is read only.", "E")); // EnterInError
        ERRORS.put("ER46", new MultiDropError("ER46", "46 - Pressure failure. Adjusting the pressure to the required value failed. Check that the reagent bottle cap is properly closed and that all tubes are tightly fitted.", "R")); // Retry
        ERRORS.put("ER47", new MultiDropError("ER47", "47 - Hash collision. Two different commands have the same hash value. This error is not fatal, but should never be reported.", "E")); // EnterInError
        ERRORS.put("ER48", new MultiDropError("ER48", "Too high pressure measurement electronics offset. Contact service.", "E")); // EnterInError
        ERRORS.put("ER49", new MultiDropError("ER49", "Pressure is off. This error is reported in return to dispense or prime commands if the pressure is off.", "E")); // EnterInError
        ERRORS.put("ER",   new MultiDropError("", " - unknown error number. Take a look to the manual or contact service.", "E")); // EnterInError
    }
}
