package multidrop;

import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.SiLAFramework;
import sila2.org.silastandard.core.dispensecontroller.v1.DispenseControllerGrpc;
import sila2.org.silastandard.core.dispensecontroller.v1.DispenseControllerOuterClass;
import sila2.org.silastandard.core.shakecontroller.v1.ShakeControllerGrpc;
import sila2.org.silastandard.core.shakecontroller.v1.ShakeControllerOuterClass;
import sila_features.SiLAServiceServer;
import sila_tools.bridge.server.SiLAServerCore;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.util.*;

import static multidrop.MultidropUtils.MAX_COLUMN;
import static multidrop.MultidropUtils.MAX_SHAKE_TIME;
import static multidrop.MultidropUtils.MAX_VOLUME;
import static sila_library.utils.SiLAErrors.generateGRPCError;
import static sila_library.utils.Utils.blockUntilStop;
import static sila_library.utils.Utils.getFileString;

/**
 * SiLA Server for Multidrop
 *
 * @implNote Hacky Implementation that needs logging and better resource handling
 */
@Slf4j
public class MultidropServer {
    final static String SERVER_NAME = "Multidrop";
    private final static int SERVER_PORT = 50051;

    private MultidropDriver driver;
    private final SiLAServerCore siLAServerCore = new SiLAServerCore();

    private void start(@Nonnull String interfaceName) {
        driver = new MultidropDriver();

        try {
            driver.start();
        } catch (IOException e) {
            log.error(e.getMessage());
        }

        try {
            final Map<String, String> fdl = new HashMap<String, String>() {
                {
                    put(
                            "DispenseController",
                            getFileString(this.getClass().getResourceAsStream("/DispenseController.xml"))
                    );
                    put(
                            "ShakeController",
                            getFileString(this.getClass().getResourceAsStream("/ShakeController.xml"))
                    );
                }
            };

            this.siLAServerCore.start(
                    SERVER_NAME,
                    new SiLAServiceServer.ServerInfo(
                            "Multidrop Micro",
                            "Liquid dispenser for 96 and 384 well plates with shaking function. Requires manual "
                                    + "installation for a liquid reservoir",
                            "http://www.titertek-berthold.com"
                    ),
                    fdl, SERVER_PORT, interfaceName,
                    new DispenseImpl(),
                    new ShakeImpl()
            );
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static void main(String[] args) {
        final String usage = "Usage: (interface)";

        final String interfaceName;
        if (args.length == 0) {
            interfaceName = "local";
        } else if (args.length == 1) {
            interfaceName = args[0];
        } else {
            throw new IllegalArgumentException(usage);
        }

        final MultidropServer server = new MultidropServer();

        server.start(interfaceName);
        blockUntilStop();
        log.info("termination complete.");
    }

    class DispenseImpl extends DispenseControllerGrpc.DispenseControllerImplBase {
        @Override
        public void dispensePlate(DispenseControllerOuterClass.DispensePlate_Parameters req,
                                  StreamObserver<DispenseControllerOuterClass.DispensePlate_Responses> responseObserver) {
            genericDispense(req.getVolume().getValue(), 1, 12,
                    responseObserver,
                    DispenseControllerOuterClass.DispensePlate_Responses.newBuilder().build());
        }

        @Override
        public void dispenseColumns(DispenseControllerOuterClass.DispenseColumns_Parameters req,
                                    StreamObserver<DispenseControllerOuterClass.DispenseColumns_Responses> responseObserver) {
            genericDispense(req.getVolume().getValue(),
                    req.getColumnStart().getValue(),
                    req.getColumnEnd().getValue(),
                    responseObserver,
                    DispenseControllerOuterClass.DispenseColumns_Responses.newBuilder().build());
        }

        private <T> void genericDispense(
                long volume, long columnStart, long columndEnd,
                StreamObserver<T> responseObserver, T response){
            if (!MultidropServer.this.driver.isDriverUp()) {
                responseObserver.onError(
                        generateGRPCError(SiLAFramework.SiLAError.newBuilder()
                                .setErrorType(SiLAFramework.SiLAError.ErrorType.EXECUTION)
                                .setIdentifier("DeviceNotUp")
                                .setMessage("It seems like the dispenser is not connected or turned off!")
                                .build())
                );
                return;
            }

            // @implNote The inputs will be read from the feature definition
            if (volume > MAX_VOLUME) {
                responseObserver.onError(
                        generateGRPCError(SiLAFramework.SiLAError.newBuilder()
                                .setErrorType(SiLAFramework.SiLAError.ErrorType.VALIDATION)
                                .setIdentifier("Volume")
                                .setMessage("Volume can be maximal " + String.valueOf(MAX_VOLUME) + " [ml]")
                                .build())
                );
                return;
            }

            if (columndEnd > MAX_COLUMN) {
                responseObserver.onError(
                        generateGRPCError(SiLAFramework.SiLAError.newBuilder()
                                .setErrorType(SiLAFramework.SiLAError.ErrorType.VALIDATION)
                                .setIdentifier("ColumnEnd")
                                .setMessage("Column can be maximal " + String.valueOf(MAX_COLUMN))
                                .build())
                );
                return;
            }

            try {
                MultidropServer.this.driver.dispense((int) volume, (int) columnStart,
                        (int) columndEnd);
            } catch (IOException e) {
                responseObserver.onError(
                        generateGRPCError(SiLAFramework.SiLAError.newBuilder()
                                .setErrorType(SiLAFramework.SiLAError.ErrorType.EXECUTION)
                                .setIdentifier("IOException")
                                .setMessage(e.getMessage())
                                .build())
                );
                return;
            }

            responseObserver.onNext(response);
            responseObserver.onCompleted();
        }
    }

    class ShakeImpl extends ShakeControllerGrpc.ShakeControllerImplBase {
        @Override
        public void shake(ShakeControllerOuterClass.Shake_Parameters req,
                          StreamObserver<ShakeControllerOuterClass.Shake_Responses> responseObserver){
            if (!MultidropServer.this.driver.isDriverUp()) {
                responseObserver.onError(
                        generateGRPCError(SiLAFramework.SiLAError.newBuilder()
                                .setErrorType(SiLAFramework.SiLAError.ErrorType.EXECUTION)
                                .setIdentifier("DeviceNotUp")
                                .setMessage( "It seems like the dispenser is not connected or turned off!")
                                .build())
                );
                return;
            }

            final long duration = req.getDuration().getValue();

            if (duration > MAX_SHAKE_TIME) {
                responseObserver.onError(
                        generateGRPCError(SiLAFramework.SiLAError.newBuilder()
                                .setErrorType(SiLAFramework.SiLAError.ErrorType.VALIDATION)
                                .setIdentifier("Duration")
                                .setMessage("Duration can be maximal " + String.valueOf(MAX_SHAKE_TIME) + " [s]")
                                .build())
                );
                return;
            }
            
            try {
                MultidropServer.this.driver.shake((int) duration);
            } catch (IOException e) {
                responseObserver.onError(
                        generateGRPCError(SiLAFramework.SiLAError.newBuilder()
                                .setErrorType(SiLAFramework.SiLAError.ErrorType.EXECUTION)
                                .setIdentifier("IOException")
                                .setMessage(e.getMessage())
                                .build())
                );
                return;
            }

            responseObserver.onNext(ShakeControllerOuterClass.Shake_Responses.newBuilder().build());
            responseObserver.onCompleted();
        }
    }
}
