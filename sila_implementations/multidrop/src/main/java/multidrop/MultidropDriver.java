package multidrop;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import lombok.extern.slf4j.Slf4j;
import sila_library.serial.SerialCommunication;

import static multidrop.MultidropUtils.ERRORS;

/**
 * Multidrop driver that handles the physical connection to the device
 *
 * @implNote Currently only for Multidrop Micro
 *
 * @implNote To use this Driver for Thermofisher Multidrop Combi:
 * Simply Emulate Multidrop Micro by going to Options-> Second Field -> Multidrop Micro
 */
@Slf4j
class MultidropDriver {
    private static final int INPUT_TIMEOUT = 5000; // [ms] timeout on receiving message
    private final static int SAMPLING_TIME = 2000; // [ms] time to sample heartBeat
    private static final String DELIMITER = "\r\n";

    private final SerialCommunication serialCommunication = new SerialCommunication();

    private MultidropUtils.DeviceType type = null;
    private int _plateType = MultidropUtils.PlateType.WellPlate_96;     // Default plate type

    /**
     * Entry point for Driver, needs to be started
     */
    void start() throws IOException {
        try {
            serialCommunication.open();
        } catch (IOException e) {
            log.info("Initial probing of serial port failed: " + e.getMessage());
        }

        serialCommunication.startHeartbeat(SAMPLING_TIME, this::checkDeviceHealth);

        if (checkDeviceHealth()) {
            primePosition();
        }
    }

    boolean isDriverUp () {
        return checkDeviceHealth();
    }

    /**
     * Simple Validator to check if the device connected is the correct one
     */
    private boolean checkDeviceHealth() {
        try {
            getDeviceType();
            deviceTypeTestCondition();
            setPlateType();
        } catch (IOException | IllegalStateException e) {
            log.info(e.getMessage());
            return false;
        }
        return true;
    }

    /**
     * Default Timeout Sending
     */
    private String sendReceive(String msg) throws IOException {
        return sendReceive(msg, 0);
    }

    /**
     * Send and Receive Messages over Serial Port
     *
     * @param msg Message to serialise and send
     * @param additionalTimeout Additional Timeout in ms to the default
     *
     * @return The answer from the device
     */
    private String sendReceive(String msg, long additionalTimeout) throws IOException {
        final String result = serialCommunication.sendReceive(msg, DELIMITER, INPUT_TIMEOUT + additionalTimeout);

        if (ERRORS.containsKey(result)) {
            throw new IOException(ERRORS.get(result).message);
        }

        return result.replaceAll("([\\r\\n\\t])", "");
    }

    /**
     * Shake the plate for certain amount of time
     *
     * @param time Time in seconds
     */
    void shake(int time) throws IOException {
        log.info("shake(" + time + ")");
        sendReceive("Z" + time, TimeUnit.SECONDS.toMillis(time));
    }

    /**
     * Dispense Liquid into the wells
     *
     * @param volume Volume in microliters
     * @param columnStart First column to dispense in
     * @param columnEnd Last column to dispense in
     */
    void dispense(int volume, int columnStart, int columnEnd)
            throws IllegalArgumentException, IOException {
        log.info("dispense(" + volume + ", " +
                columnStart + ", " + columnEnd + ")");

        if (columnStart > columnEnd) {
            throw new IllegalArgumentException("Column start must be smaller that column end");
        }

        String result;
        
        // Using semi-automatic approach
        result = sendReceive("S" + columnStart);
        log.info("1. Column [S]tart set: " + result);

        result = sendReceive("V" + volume);
        log.info("3. Dispensing [V]olume set: " + result);

        primePosition();

        final int wells = columnEnd - columnStart + 1;
        log.info("   Start column: " + columnStart +
                " End column: " + columnEnd +
                " amount of columns: " + wells);
        for (int i = 0; i < wells; i++){
            // S+M mode requires us to setup start well,
            // then dispense the said volume (3.) with M on that column well
            final int currentWell = i + columnStart;
            sendReceive("S" + currentWell);
            result = sendReceive("M");
            log.info( currentWell + ". Dispensing column " +
                    currentWell + " set [M]: " + result);
        }

        primePosition();
    }

    private void primePosition() throws IOException {
        String msg = sendReceive("O");
        log.info("Primed: " + msg);
    }

    /**
     * Set Plate Type of Class
     *
     * @implNote In the future, this should be an input
     */
    private void setPlateType() throws IOException {
        sendReceive("T" + String.valueOf(_plateType));
        if (_plateType == MultidropUtils.PlateType.WellPlate_96) {
            log.debug("Set plateType to 96-well plate");
        } else if (_plateType == MultidropUtils.PlateType.WellPlate_384) {
            log.debug("Set plateType to 384-well plate");
        }
    }

    /**
     * Check if the device type queried is supported, throw Exception otherwise
     */
    private void deviceTypeTestCondition() throws IllegalStateException {
        switch(type) {
            case MultidropMicro:
                log.debug("Device was tested. Passing test condition.");
                break;
            default:
                throw new IllegalStateException("Device type " + type.name() + " not supported!");
        }
    }

    /**
     * Set the device type on the class via querying the version
     */
    private void getDeviceType() throws IOException, IllegalStateException {
        final String version = sendReceive("VER");
        log.debug("Version: " + version);
        if ( version != null && !version.isEmpty()  ) {
            if (version.contains("MultidropCombi")){
                log.info("Detected MultidropCombi device");
                type = MultidropUtils.DeviceType.MultidropCombi;
            } else if (version.contains("MdropDW")) {
                log.info("Detected MultidropDW device");
                type = MultidropUtils.DeviceType.MultidropDW;
            } else if (version.contains("MDMicro")) {
                log.info("Detected MultidropMicro device");
                type = MultidropUtils.DeviceType.MultidropMicro;
            } else if (version.contains("Mdrop384")) {
                log.info("Detected Multidrop384 device");
                type = MultidropUtils.DeviceType.Multidrop384;
            } else {
                throw new IllegalStateException("Version " + version + " not supported!");
            }
        } else {
            throw new IllegalStateException("Can not recognise Device Type");
        }
    }

    // Testing multidrop driver
    public static void main(String[] args) throws IOException {
        MultidropDriver driver = new MultidropDriver();
        driver.start();

        // testing dispense
        // dispense 50 micro L from well 9 to 12
        driver.dispense(50, 9, 12);

        // other tests
        if (args.length > 0) {
            if (args[0].equals("shake")) {
                driver.shake(Integer.parseInt(args[1]));
            }
        }
        
        log.info("... finished");
    }
}