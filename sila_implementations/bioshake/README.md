# BioShake 3000 ELM
SiLA Java BioShaker server and respective client.

The server can be started with:

```bash
java -cp bioshake-1.0.0-SNAPSHOT-jar-with-dependencies.jar bioshake.BioShakeServer wlan0
```