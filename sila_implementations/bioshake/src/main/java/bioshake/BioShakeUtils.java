package bioshake;

import lombok.Value;

import java.util.HashMap;
import java.util.Map;

/**
 * Various Utility Classes for the BioShake Driver
 */
class BioShakeUtils {
    /**
     * Edge Locking Mechanism
     */
    public enum ELMStatePosition {
        BETWEEN  (0, "ELM not in lock or unlock position"),
        LOCKED   (1, "ELM in lock position - Microplate is locked"),
        UNLOCKED (3, "ELM in unlock position - Microplate is unlocked"),
        ERROR    (9, "Error - detecting ELM state");

        public final int value;
        public final String message;

        ELMStatePosition(int value, String message) {
            this.value = value;
            this.message = message;
        }
    }

    /**
     * States Manual Compliant
     */
    @Value
    static class BioShakeState {
        String message;
        String codeString;
    }

    static final Map<Integer, BioShakeState> STATES = new HashMap<>();
    static {
        STATES.put(0, new BioShakeState("Shaking is active", "RUN"));
        STATES.put(1, new BioShakeState("Shaker has a stop command detect", "ESTOP"));
        STATES.put(2, new BioShakeState("Shaker in the braking mode", null));
        STATES.put(3, new BioShakeState("Arrived in the home position", "STOP"));
        STATES.put(4, new BioShakeState("Manual mode for external control", null));
        STATES.put(5, new BioShakeState("Acceleration", "RAMP+"));
        STATES.put(6, new BioShakeState("Deceleration", "RAMP-"));
        STATES.put(7, new BioShakeState("Deceleration with stopping", null));
        STATES.put(90, new BioShakeState("ECO mode", null));
        STATES.put(99, new BioShakeState("Boot process running", null));
    }

    public enum DeviceState {
        SHAKING      (0),
        STOP_DETECTED(1),
        BRAKING_MODE (2),
        HOME         (3),
        MANUAL_MODE  (4),
        ACCELERATION (5),
        DECELERATION (6),
        DECELERATION_WITH_STOPPING (7),
        ECO_MODE     (90),
        BOOTING      (99),
        ERROR        (-1);

        public int value;
        public BioShakeState state;

        DeviceState(int value){
            this.value = value;
            this.state = STATES.get(value);
        }
    }

    static String getStateMessage(int stateNumber) {
        return STATES.get(stateNumber).message;
    }

    static ELMStatePosition getELMStatePosition(int value) {
        for (ELMStatePosition elm : ELMStatePosition.values()) {
            if (elm.value == value) return elm;
        }
        return ELMStatePosition.ERROR;
    }

    static DeviceState getDeviceState(int value) {
        for (DeviceState d : DeviceState.values()) {
            if (d.value == value) {
                return d;
            }
        }
        return DeviceState.ERROR;
    }

    static void consoleShow(String version, String description, BioShakeDriver.DeviceType type) {
        System.out.println("\n");
        System.out.println(" ╔=============================================╗");
        System.out.println(" ║          Server Driver Information         ╔╝");
        System.out.println(" ║  Version: " + version);
        System.out.println(" ║  Description: " + description);
        System.out.println(" ║  Type: " + type);
        System.out.println(" ║                                            ╚╗");
        System.out.println(" ╚=============================================╝");
        System.out.println("\n");
    }
}
