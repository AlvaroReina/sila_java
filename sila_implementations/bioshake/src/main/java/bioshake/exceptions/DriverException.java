package bioshake.exceptions;

public class DriverException extends Exception {
    public DriverException(String message) {
        super(message);
    }

    public static class DeviceNotRunningException extends DriverException {
        public DeviceNotRunningException() {
            super("It seems like the BioShake is not connected or turned off!");
        }
    }

    public static class CommandNotFoundException extends DriverException {
        public CommandNotFoundException(String command) {
            super("Command " + command + " not found.");
        }
    }

    public static class AlreadyShakingException extends DriverException {
        public AlreadyShakingException() {
            super("Device already shaking.");
        }
    }

    public static class DeviceInterrupted extends DriverException {
        public DeviceInterrupted() {
            super("Driver interrupted while operating");
        }
    }
}
