package bioshake.exceptions;

public class BioShakeException extends DriverException {
    public BioShakeException(String message) {
        super(message);
    }
}