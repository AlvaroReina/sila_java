package bioshake;

import java.util.*;

class BioShakeErrorHelper {
    private static final Map<Integer, String> ERRORS = new HashMap<>();
    static {
        ERRORS.put(-1, "Unknown Error by the SiLA2 Driver");

        // mixing function (check manual)
        ERRORS.put(101, "Error by the DC motor controller. Please CALL the service line.");
        ERRORS.put(102, "Error due speed failure, for example happens through mechanical locking.");
        ERRORS.put(103, "Errors caused by an uninitialized shaker or incorrect initialization parameters after switch on.\n" +
                        "Please see the service manual to start a special initialization routine for optimization of motor parameters.");
        ERRORS.put(104, "Errors caused by unsuccessful initialization routine. Please see the service manual.");
        ERRORS.put(105, "Errors caused by not achieving of the home position at the stop command / routine. Please CALL the service line.");
        ERRORS.put(106, "Errors caused by over speed. Please CALL the service line.");

        // temperature control
        ERRORS.put(201, "Error due failed answers from temperature sensors or incorrect internal settings of temperature sensors. Please CALL the service line.");
        ERRORS.put(202, "Error due temperature communication bus system. Please CALL the service line.");
        ERRORS.put(203, "Sensor with the requested ID is not found while working. Please RESTART the system.");
        ERRORS.put(204, "Errors caused by a faulty temperature measurement while working. Please RESTART the system.");
        ERRORS.put(206, "Error caused by checksum of the internal temperature sensor. Please CALL the service line.");
        ERRORS.put(207, "Error caused by checksum of the main temperature sensor. Please CALL the service line");
        ERRORS.put(208, "Error caused by general checksum. Please CALL the service line");
        ERRORS.put(209, "Error caused by unknown temperature method. Please CALL the service line");
        ERRORS.put(210, "Error caused by over heating. Please CALL the service line");

        // ELM control
        ERRORS.put(300, "General error. Please CALL the service line.");
        ERRORS.put(301, "IC-Driver error. Please CALL the service line.");
        ERRORS.put(303, "Verification error by the unlock position. Please RESTART the system.");
        ERRORS.put(304, "Error caused by unsuccessful reach the lock position (timeout). Please RESTART the system.");
        ERRORS.put(305, "Error caused by unsuccessful reach the unlock position (timeout). Please RESTART the system.");
        ERRORS.put(306, "Error caused by unsuccessful reach the lock position (over current). Please RESTART the system.");
        ERRORS.put(307, "Error caused by unsuccessful reach the unlock position (over current). Please RESTART the system.");
    }

    /**
     * Reaponsible for mapping 'getErrorList' result to list of errors
     * @param list String list with error codes like such: {e1;...;en} example: {101;102}
     * @return List<BioShakeError> List of BioShakeError
     */
    public static List<String> mapStringErrorsToList(String list) {
        List<String> bioShakeErrors = new ArrayList<>();
        list = list.replaceAll("([{|}])", "");
        if (list.isEmpty()) return bioShakeErrors;

        for (String item : list.split(";")) {
            final int itemCode = Integer.parseInt(item);
            if (ERRORS.containsKey(itemCode)) {
                bioShakeErrors.add("Code: " + item + " - Message: " + ERRORS.get(itemCode));
            } else {
                bioShakeErrors.add(ERRORS.get(-1));
            }
        }
        return bioShakeErrors;
    }
}