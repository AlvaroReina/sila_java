package bioshake;

import bioshake.BioShakeUtils.DeviceState;
import bioshake.BioShakeUtils.ELMStatePosition;
import bioshake.exceptions.BioShakeException;
import bioshake.exceptions.DriverException;
import lombok.extern.slf4j.Slf4j;
import sila_library.serial.SerialCommunication;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * Bioshaker driver that handles the physical connection to the server
 */
@Slf4j
class BioShakeDriver {
    private SerialCommunication serialCommunication;

    private final static int SAMPLING_TIME = 2000; // [ms] time to sample heartBeat
    private static final int INPUT_TIMEOUT = 10000; // [ms] @todo nr: manual says max 5? to be tested!

    // these are all the "non booting states" the server can supply us
    private static final List<String> NON_BOOTING_STATES = Arrays.asList("0", "1", "2", "3", "4", "5", "6", "7");
    private static final String DELIMITER = "\r\n";

    private static final int SLEEP_TIME = 1000; // in [ms]

    // "e" is the string that's outputted when command gives error
    private static final String ERROR_MESSAGE_FORMAT = "e";
    /**
     * RANGES
     */
    // Acceleration Range
    static final long ACCELERATION_MAX = 30;

    static final long ACCELERATION_MIN = 0;
    // Time Range
    static final long DURATION_MAX = 99999;

    static final long DURATION_MIN = 0;
    // RPM's Range
    static final long RPM_MAX = 3000;

    static final long RPM_MIN = 200;

    // @implNote not used yet | might have to be changed in the future to accommodate other bioshake versions
    private static final String DEVICE_SERIAL_NAME = "BIOSHAKE 3000";

    /**
     * State
     */
    private int currentVelocity = 0;
    private int currentAcceleration = 0;

    /**
     * DeviceType Enum
     * @implNote we are currently only concerned on the 3000 ELM BioShaker version
     */
    public enum DeviceType {
        THREE_THOUSAND_ELM,
        THREE_THOUSAND, THREE_THOUSAND_T,
        THREE_THOUSAND_ELM_T, THREE_THOUSAND_ELM_DWP,
        D_THIRTY_ELM, D_THIRTY_ELM_T, FIVE_THOUSAND_ELM, HEATH_PLATE
    }

    /**
     * BioShakeDriver constructor
     *
     * @implNote Starts upon construction to setup a connection
     */
    BioShakeDriver(@Nonnull String serialComName) {
        serialCommunication = new SerialCommunication(serialComName);

        try {
            serialCommunication.open();
        } catch (IOException e) {
            log.info("Initial probing of serial port failed: " + e.getMessage());
        }

        serialCommunication.startHeartbeat(SAMPLING_TIME, () -> {
            try {
                String result = serialCommunication.sendReceive(
                        "getShakeState", DELIMITER, INPUT_TIMEOUT)
                        .replaceAll("([\\r\\n\\t])", "");
                final boolean booted = NON_BOOTING_STATES.contains(result);
                if (!booted) return false;

                final String version = serialCommunication
                        .sendReceive("getDescription", DELIMITER, INPUT_TIMEOUT);
                return version.contains(DEVICE_SERIAL_NAME);
            } catch (IOException e) {
                return false;
            }
        });
    }

    /**
     * Checks whether server is up or down
     *
     * @return true/false
     */
    private boolean isDriverUp() {
        return (serialCommunication != null) && serialCommunication.isSerialUp();
    }

    /**
     * Magic function to speak with server.
     *
     * @param msg Message with command (and parameters going with it)
     * @return String with result.
     * @throws IOException Input/Output error
     * @throws BioShakeException BioShake error
     */
    private synchronized String sendReceive(String msg) throws IOException, DriverException {
        String result;
        result = serialCommunication.sendReceive(msg, DELIMITER, INPUT_TIMEOUT);
        result = result.replaceAll("([\\r\\n\\t])", ""); // remove line chars
        if (result.equals(ERROR_MESSAGE_FORMAT)) {
            throw new BioShakeException("Error encountered, please check the Error List or the State");
        }
        if (result.contains("u -> ")) {
            throw new DriverException.CommandNotFoundException(msg);
        }
        return result;
    }

    /**
     * Stops shake immediately
     *
     * @return String with result message
     * @throws IOException Input/Output message
     * @throws BioShakeException BioShake error
     */
    public String shakeOff() throws IOException, DriverException {
        if (!isDriverUp()) throw new DriverException.DeviceNotRunningException();
        String result;
        synchronized (this) {
            result = sendReceive("shakeOff"); // just changes State :)
            while (!testIfShakeFinished()) {
                try {
                    Thread.sleep(SLEEP_TIME); // duration in [ms]
                } catch (InterruptedException e) {
                    log.error("[shake] Driver Interrupted. " + e.getMessage());
                }
            }
            // is state right, unlock server to allow Microplate to be removed
            setElmUnlockPos();
        }
        return result;
    }

    /**
     * Starts shake indefinitely with previously saved values
     */
    public String shakeIndefinitely() throws IOException, DriverException {
        if (!isDriverUp()) throw new DriverException.DeviceNotRunningException();
        if (!testIfShakeFinished()) throw new DriverException.AlreadyShakingException();
        synchronized (this) {
            // check if has shakeTargetSpeed set!
            if (currentVelocity == 0) {
                throw new BioShakeException("Please set Target Speed first.");
            }

            setElmLockPos();

            sendReceive("setShakeTargetSpeed" + currentVelocity);
            sendReceive("setShakeAcceleration" + currentAcceleration);

            return sendReceive("shakeOn");
        }
    }

    /**
     * Shakes for duration, at a certain velocity. Acceleration can be added as well
     *
     * @param duration in seconds
     * @param velocity in RPM's
     * @param acceleration in s
     * @return string with result message
     * @throws IOException Input/Output error
     * @throws BioShakeException BioShake error
     */
    public String shakePredetermined(int duration, int velocity, int acceleration) throws IOException, DriverException {
        if (!isDriverUp()) throw new DriverException.DeviceNotRunningException();

        String result;
        if (!testIfShakeFinished()) throw new DriverException.AlreadyShakingException();

        synchronized (this) {
            // let's lock!
            setElmLockPos();

            currentVelocity = velocity;
            currentAcceleration = acceleration;
            // set target speed, acceleration and then shake with runtime
            sendReceive("setShakeTargetSpeed" + velocity);
            sendReceive("setShakeAcceleration" + acceleration);
            result = sendReceive("shakeOnWithRuntime" + duration);
        }

        while (!testIfShakeFinished()) {
            try {
                Thread.sleep(SLEEP_TIME); // duration in [ms]
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
        // is state right, unlock server to allow Microplate to be removed
        setElmUnlockPos();

        // now we can proceed and return result
        return result;
    }

    // set Shake Target Speed
    String setShakeTargetSpeed(int velocity) throws DriverException {
        if (!isDriverUp()) throw new DriverException.DeviceNotRunningException();
        currentVelocity = velocity;
        return "ok";
    }

    // set Shake Acceleration
    String setShakeAcceleration(int acceleration) throws DriverException {
        if (!isDriverUp()) throw new DriverException.DeviceNotRunningException();
        currentAcceleration = acceleration;
        return "ok";
    }

    /**
     * InitController.xml Command GetState
     */
    String getState() throws IOException, DriverException {
        if (!isDriverUp()) throw new DriverException.DeviceNotRunningException();
        String result = sendReceive("getShakeState");
        return BioShakeUtils.getStateMessage(Integer.parseInt(result));
    }

    /**
     * Get's error list from server
     */
    List<String> getErrorList() throws IOException, DriverException {
        if (!isDriverUp()) throw new DriverException.DeviceNotRunningException();
        return BioShakeErrorHelper.mapStringErrorsToList(sendReceive("getErrorList"));
    }

    /**
     * InitController.xml Property Version
     */
    String getVersion() throws IOException, DriverException {
        if (!isDriverUp()) throw new DriverException.DeviceNotRunningException();
        return sendReceive("getVersion");
    }

    /**
     * InitController.xml Property Description
     */
    String getDescription() throws IOException, DriverException {
        if (!isDriverUp()) throw new DriverException.DeviceNotRunningException();
        return sendReceive("getDescription");
    }



    /** Private Utility Functions **/
    private boolean testIfShakeFinished() throws IOException, DriverException {
        final String result = sendReceive("getShakeState");
        final DeviceState deviceState = BioShakeUtils.getDeviceState(Integer.parseInt(result));
        log.info("[shaking test]: #{} => {}", result, deviceState.state.getMessage());
        return deviceState.equals(DeviceState.HOME);
    }

    /**
     * @Note: this is a state driven procedure, so we will not ask to lock the ELM if ELM is already locked.
     *
     * LOCKS ELM, so that Microplate can be firmly shaken.
     * @return String with result of ELM state
     */
    private void setElmLockPos() throws IOException, DriverException {
        // first check state
        ELMStatePosition elmStatePos = BioShakeUtils
                .getELMStatePosition(Integer.parseInt(sendReceive("getElmState")));
        // let's loop until Server is locked or errors
        while (!elmStatePos.equals(ELMStatePosition.LOCKED)) {
            log.info("[closeElm][auto-close ELM] getElmState: {}", elmStatePos.message);

            // let's check for error ELM state position and break out
            if (elmStatePos.equals(ELMStatePosition.ERROR)) {
                throw new BioShakeException("Locking Error: " + elmStatePos.message);
            }

            // let's unlock
            final String lockResult = sendReceive("setElmLockPos");
            log.info("[closeElm][auto-close ELM] setElmLockPos: {}", lockResult);
            try {
                Thread.sleep(SLEEP_TIME);
            } catch (InterruptedException e) {
                log.error(e.getMessage());
            }
            // get ELM state
            elmStatePos = BioShakeUtils.getELMStatePosition(Integer.parseInt(sendReceive("getElmState")));
        }
        log.info("[closeElm][auto-close ELM] setElmLockPos: {}", elmStatePos.message);
    }

    /**
     * setElmUnlockPos()
     *
     * UNLOCKS ELM, so that Microplate can be removed.
     * Note: this is a state driven procedure, so we will not ask to unlock the ELM if ELM is already unlock.
     * @return String with result of ELM state
     */
    private String setElmUnlockPos() throws IOException, DriverException {
        // first check state
        ELMStatePosition elmStatePos = BioShakeUtils
                .getELMStatePosition(Integer.parseInt(sendReceive("getElmState")));
        // let's loop until Server is unlocked or errors
        while (!elmStatePos.equals(ELMStatePosition.UNLOCKED)) {
            log.info("[openElm][auto-open ELM] getElmState: {}", elmStatePos.message);
            // let's check for error ELM state position and break out
            if (elmStatePos.equals(ELMStatePosition.ERROR)) {
                return elmStatePos.message;
            }

            // let's check for driver down
            try {
                if (!isDriverUp()) throw new DriverException.DeviceInterrupted();
            } catch (DriverException e) {
                return "Driver is down.";
            }

            // let's unlock
            String unlockResult = sendReceive("setElmUnlockPos");
            log.info("[openElm][auto-open ELM] setElmUnlockPos: {}", unlockResult);
            // now sleep
            try {
                Thread.sleep(SLEEP_TIME);
            } catch (InterruptedException e) {
                log.error(e.getMessage());
            }
            // get ELM state
            elmStatePos = BioShakeUtils.getELMStatePosition(Integer.parseInt(sendReceive("getElmState")));
        }
        log.info("[openElm][auto-open ELM] setElmUnlockPos: {}", elmStatePos.message);
        return elmStatePos.message;
    }
}