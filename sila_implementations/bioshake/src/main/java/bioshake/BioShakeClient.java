package bioshake;

import io.grpc.ManagedChannel;
import sila2.org.silastandard.SiLAFramework;
import sila2.org.silastandard.core.shakecontroller.v1.ShakeControllerGrpc;
import sila2.org.silastandard.core.shakecontroller.v1.ShakeControllerOuterClass;
import sila_library.discovery.SiLAServerDiscovery;

import java.util.Collections;
import java.util.concurrent.TimeUnit;

/**
 * Client to call BioShaker Server
 */
public class BioShakeClient {
    public static void main(String[] args) throws Exception {
        SiLAServerDiscovery.enable();

        // Blocking call to find server
        final ManagedChannel channel = SiLAServerDiscovery.blockAndGetChannel(BioShakeServer.SERVER_NAME);
        final ShakeControllerGrpc.ShakeControllerBlockingStub blockingStub =
                ShakeControllerGrpc.newBlockingStub(channel);

        // Call the server and try to shake
        try {
            blockingStub.shakePredetermined(ShakeControllerOuterClass.ShakePredetermined_Parameters.newBuilder()
                    .setDuration(SiLAFramework.Integer.newBuilder().setValue(10)) // one second
                    .setTargetSpeed(
                        ShakeControllerOuterClass.DataType_Speed.newBuilder().setSpeed(
                                SiLAFramework.Integer.newBuilder().setValue(10)
                        ).build()
                    )
                    .setTargetAccelerationDuration(
                            ShakeControllerOuterClass.DataType_AccelerationDuration.newBuilder()
                                    .setAccelerationDuration(
                                            SiLAFramework.Integer.newBuilder().setValue(2000)
                                    ).build()
                    )
                    .build());
        } finally {
            channel.shutdown().awaitTermination(5, TimeUnit.SECONDS);
        }
    }
}