package bioshake;

import bioshake.exceptions.DriverException;
import io.grpc.StatusRuntimeException;
import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;
import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.impl.Arguments;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.Namespace;
import sila2.org.silastandard.SiLAFramework;
import sila2.org.silastandard.core.shakecontroller.v1.ShakeControllerGrpc;
import sila2.org.silastandard.core.shakecontroller.v1.ShakeControllerOuterClass;
import sila2.org.silastandard.core.stateprovider.v1.StateProviderGrpc;
import sila2.org.silastandard.core.stateprovider.v1.StateProviderOuterClass;
import sila_features.SiLAServiceServer;
import sila_library.serial.SerialCommunication;
import sila_library.utils.ListNetworkInterfaces;
import sila_library.utils.ListSerialComInterfaces;
import sila_library.utils.SiLAErrors;
import sila_tools.bridge.server.SiLAServerCore;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.util.*;

import static bioshake.BioShakeDriver.*;
import static sila_library.utils.SiLAErrors.generateGRPCError;
import static sila_library.utils.Utils.blockUntilStop;
import static sila_library.utils.Utils.getFileString;

/**
 * SiLA Server for BioShake
 */
@Slf4j
public class BioShakeServer {
    final static String SERVER_NAME = "BioShake";
    private final static int SERVER_PORT = 50051;

    private BioShakeDriver driver;
    private final SiLAServerCore siLAServerCore = new SiLAServerCore();

    /**
     * Start's server and server
     * @param serverPort Requires a server port.
     */
    private void start(@Nonnull String interfaceName, int serverPort, @Nonnull String serialComName) {
        try {
            driver = new BioShakeDriver(serialComName);
            Map<String, String> fdl = new HashMap<String, String>() {
                {
                    put(
                            "StateProvider",
                            getFileString(this.getClass().getResourceAsStream("/StateProvider.xml"))
                    );
                    put(
                            "ShakeController",
                            getFileString(this.getClass().getResourceAsStream("/ShakeController.xml"))
                    );
                }
            };

            this.siLAServerCore.start(
                    SERVER_NAME,
                    new SiLAServiceServer.ServerInfo(
                            "BioShake",
                            "The BioShake provides high-speed mixing action and " +
                                    "temperature control for the most demanding robotics applications.",
                            "www.QInstruments.com"
                    ),
                    fdl, serverPort, interfaceName,
                    new StateImpl(),
                    new ShakeImpl()
            );
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    /**
     * Main execution entry point
     * @param args Any command line argument (list of string).
     */
    public static void main(String[] args) {
        // Argument Parser
        ArgumentParser parser = ArgumentParsers.newFor(BioShakeDriver.class.toString()).build()
                .defaultHelp(true)
                .description("BioShake server has defaults and some options.");
        // Port Argument (optional)
        parser.addArgument("-p", "--port")
                .type(Integer.class)
                .setDefault(BioShakeServer.SERVER_PORT) // defaults to SERVER_PORT
                .help("Specify port to use.");
        // Network Interface Argument
        parser.addArgument("-n", "--networkInterface")
                .type(String.class)
                .setDefault("local")
                .help("Specify network interface. Check ifconfig (LNX & MAC)"
                        + " and for windows, ask us for a tiny java app @ www.unitelabs.ch .");
        // Serial Com Argument
        parser.addArgument("-s", "--serialCom")
                .type(String.class)
                .setDefault(SerialCommunication.DEFAULT_SERIAL_COM_NAME)
                .help("Specify serial communication name for the driver to find the proper one. It might change" +
                        " from OS to OS.");
        // Only list System Information
        parser.addArgument("-l", "--listInfo")
                .type(Arguments.booleanType("yes", "no"))
                .setDefault(false)
                .help("Only lists system information such as:\n" +
                        "1. List of names of network interfaces found\n" +
                        "2. List of names of USB-Serial interfaces found\n" +
                        "This information can then be used to initialize the server " +
                        "properly on OS like windows, where the names differ from *unix* systems.");
        Namespace ns = parser.parseArgsOrFail(args);

        // if user requested to list System Information only...
        final Boolean listMode = ns.getBoolean("listInfo");
        if (listMode) {
            ListNetworkInterfaces.display();
            ListSerialComInterfaces.display();
            System.exit(0);
        }

        final String interfaceName = ns.getString("networkInterface");
        final String serialComName = ns.getString("serialCom");
        final int port = ns.getInt("port");

        final BioShakeServer server = new BioShakeServer();
        server.start(interfaceName, port, serialComName);
        blockUntilStop();
        log.info("termination complete.");
    }

    /**
     * Class InitImpl (inner class)
     *
     * Extends gRPC-generated ImplBase class, where we implement the Feature Definitions
     */
    class StateImpl extends StateProviderGrpc.StateProviderImplBase {

        /**
         * getVersion()
         *
         * Get's Server Version [Property]
         * @param req Request
         * @param res Response (were return is embedded)
         */
        @Override
        public void getVersion(
                StateProviderOuterClass.Get_Version_Parameters req,
                StreamObserver<StateProviderOuterClass.Get_Version_Responses> res
        ) {
            try {
                String version = BioShakeServer.this.driver.getVersion();
                res.onNext(
                        StateProviderOuterClass.Get_Version_Responses.newBuilder()
                                .setVersion(SiLAFramework.String.newBuilder().setValue(version))
                                .build()
                );
                res.onCompleted();
            } catch (IOException | DriverException e) {
                StatusRuntimeException exception = SiLAErrors.generateGenericExecutionError(e);
                log.error( e.getMessage() );
                res.onError(exception);
            }
        }

        /**
         * getDescription()
         *
         * Get's Server Description [Property]
         * @param req Request
         * @param res Response (were return is embedded)
         */
        @Override
        public void getModelInformation(
                StateProviderOuterClass.Get_ModelInformation_Parameters req,
                StreamObserver<StateProviderOuterClass.Get_ModelInformation_Responses> res
        ) {
            try {
                String description = BioShakeServer.this.driver.getDescription();
                res.onNext(
                        StateProviderOuterClass.Get_ModelInformation_Responses.newBuilder()
                                .setModelInformation(SiLAFramework.String.newBuilder().setValue(description)).build()
                );
                res.onCompleted();
            } catch (IOException | DriverException e) {
                StatusRuntimeException exception = SiLAErrors.generateGenericExecutionError(e);
                log.error( e.getMessage() );
                res.onError(exception);
            }
        }

        /**
         * getState()
         *
         * Get's Server State [Command]
         * @param req Request
         * @param res Response (were return is embedded)
         */
        @Override
        public void getState(
                StateProviderOuterClass.GetState_Parameters req,
                StreamObserver<StateProviderOuterClass.GetState_Responses> res
        ) {
            try {
                String stateResult = BioShakeServer.this.driver.getState();
                res.onNext(
                        StateProviderOuterClass.GetState_Responses.newBuilder()
                                .setResult( SiLAFramework.String.newBuilder().setValue(stateResult))
                                .build()
                );
                res.onCompleted();
            } catch (IOException | DriverException e) {
                StatusRuntimeException exception = SiLAErrors.generateGenericExecutionError(e);
                log.error( e.getMessage() );
                res.onError(exception);
            }
        }

        /**
         * getErrorList()
         *
         * Get's error list lie {e1;...;en}, example: {101,102} [Command]
         * @param req Request
         * @param res Response (where return is embedded)
         */
        @Override
        public void getErrorList(
                StateProviderOuterClass.GetErrorList_Parameters req,
                StreamObserver<StateProviderOuterClass.GetErrorList_Responses> res
        ) {
            try {
                final List<String> result = BioShakeServer.this.driver.getErrorList();
                final StateProviderOuterClass.GetErrorList_Responses.Builder responseBuilder =
                        StateProviderOuterClass.GetErrorList_Responses.newBuilder();

                for (String error : result) {
                    responseBuilder.addErrorList(SiLAFramework.String.newBuilder().setValue(error));
                }

                res.onNext(responseBuilder.build());
                res.onCompleted();
            } catch (IOException | DriverException e) {
                final StatusRuntimeException exception = SiLAErrors.generateGenericExecutionError(e);
                log.error(e.getMessage());
                res.onError(exception);
            }
        }
    }

    /**
     * Class ShakeImpl (inner class)
     *
     * Extends gRPC-generated ImplBase class, where we implement the Feature Definitions
     */
    class ShakeImpl extends ShakeControllerGrpc.ShakeControllerImplBase {

        /**
         * Set's Target Speed [Command]
         */
        @Override
        public void setTargetSpeed(
                ShakeControllerOuterClass.SetTargetSpeed_Parameters req,
                StreamObserver<ShakeControllerOuterClass.SetTargetSpeed_Responses> res
        ) {
            final long velocity = req.getTargetSpeed().getSpeed().getValue();

            try {
                checkVelocityInRange(velocity);
            } catch (StatusRuntimeException e) {
                res.onError(e);
                return;
            }

            try {
                String setTargetResult = BioShakeServer.this.driver
                        .setShakeTargetSpeed((int) velocity);
                res.onNext(
                        ShakeControllerOuterClass.SetTargetSpeed_Responses.newBuilder()
                                .setSuccess(SiLAFramework.String.newBuilder()
                                        .setValue(setTargetResult)).build()
                );
                res.onCompleted();
            } catch (DriverException e) {
                StatusRuntimeException exception = SiLAErrors.generateGenericExecutionError(e);
                log.error(e.getMessage());
                res.onError(exception);
            }
        }

        /**
         * Set's Target Acceleration [Command]
         */
        @Override
        public void setTargetAcceleration(
                ShakeControllerOuterClass.SetTargetAcceleration_Parameters req,
                StreamObserver<ShakeControllerOuterClass.SetTargetAcceleration_Responses> res
        ) {
            final long acceleration = req.getTargetAccelerationDuration().getAccelerationDuration().getValue();
            try {
                checkAccelerationInRange(acceleration);
            } catch (StatusRuntimeException e) {
                res.onError(e);
                return;
            }
            try {
                String setAccelerationResult = BioShakeServer.this.driver
                        .setShakeAcceleration((int) acceleration);
                res.onNext(
                        ShakeControllerOuterClass.SetTargetAcceleration_Responses.newBuilder()
                                .setSuccess(SiLAFramework.String.newBuilder()
                                        .setValue(setAccelerationResult)).build()
                );
                res.onCompleted();
            } catch (DriverException e) {
                StatusRuntimeException exception = SiLAErrors.generateGenericExecutionError(e);
                log.error(e.getMessage());
                res.onError(exception);
            }
        }

        /**
         * Shakes Indefinitely [Command]
         */
        @Override
        public void shakeIndefinitely(
                ShakeControllerOuterClass.ShakeIndefinitely_Parameters req,
                StreamObserver<ShakeControllerOuterClass.ShakeIndefinitely_Responses> res
        ) {
            try {
                String shakeResult = BioShakeServer.this.driver.shakeIndefinitely();
                res.onNext(
                        ShakeControllerOuterClass.ShakeIndefinitely_Responses.newBuilder()
                                .setSuccess(SiLAFramework.String.newBuilder().setValue(shakeResult))
                                .build()
                );
                res.onCompleted();
            } catch (IOException | DriverException e) {
                StatusRuntimeException exception = SiLAErrors.generateGenericExecutionError(e);
                log.error(e.getMessage());
                res.onError(exception);
            }
        }

        /**
         * Pre-defined shake (with a set of duration, velocity and acceleration) [Command]
         */
        @Override
        public void shakePredetermined(
                ShakeControllerOuterClass.ShakePredetermined_Parameters req,
                StreamObserver<ShakeControllerOuterClass.ShakePredetermined_Responses> res
        ) {
            final long duration = req.getDuration().getValue();
            final long velocity = req.getTargetSpeed().getSpeed().getValue();
            final long acceleration = req.getTargetAccelerationDuration().getAccelerationDuration().getValue();
            // @implNote: Validations will be done by SiLA Bridge in the future
            try {
                checkDurationInRange(duration);
                checkVelocityInRange(velocity);
                checkAccelerationInRange(acceleration);
            } catch (StatusRuntimeException e) {
                res.onError(e);
                return;
            }

            try {
                String shakeResult = BioShakeServer.this.driver.shakePredetermined(
                        (int) duration, (int) velocity, (int) acceleration
                );
                res.onNext(
                        ShakeControllerOuterClass.ShakePredetermined_Responses.newBuilder()
                                .setSuccess(SiLAFramework.String.newBuilder().setValue(shakeResult))
                                .build()
                );
                res.onCompleted();
            } catch (IOException | DriverException e) {
                final StatusRuntimeException exception = SiLAErrors.generateGenericExecutionError(e);
                log.error(e.getMessage());
                res.onError(exception);
            }
        }

        /**
         * Stops shaking immediately [Command]
         */
        @Override
        public void shakeOff(
                ShakeControllerOuterClass.ShakeOff_Parameters req,
                StreamObserver<ShakeControllerOuterClass.ShakeOff_Responses> res
        ) {
            try {
                String shakeResult = BioShakeServer.this.driver.shakeOff();
                res.onNext(
                        ShakeControllerOuterClass.ShakeOff_Responses.newBuilder()
                                .setSuccess(SiLAFramework.String.newBuilder().setValue(shakeResult))
                                .build()
                );
                res.onCompleted();
            } catch (IOException | DriverException e) {
                final StatusRuntimeException exception = SiLAErrors.generateGenericExecutionError(e);
                log.error(e.getMessage());
                res.onError(exception);
            }
        }

        /**
         * Checks whether or not velocity is in Range
         *
         * @param velocity Velocity
         * @throws StatusRuntimeException if not in range
         */
        private void checkVelocityInRange(long velocity) {
            checkWithinMaxRangeIncluding(velocity, RPM_MAX, "Velocity", "[rpm]");
            checkWithinMinRangeIncluding(velocity, RPM_MIN, "Velocity", "[rpm]");
        }

        /**
         * Checks whether or not acceleration is in Range
         *
         * @param acceleration Acceleration
         * @throws StatusRuntimeException if not in range
         */
        private void checkAccelerationInRange(long acceleration)
                throws StatusRuntimeException {
            checkWithinMaxRangeIncluding(acceleration, ACCELERATION_MAX,
                    "Acceleration", "[s]"
            );
            checkWithinMinRangeIncluding(acceleration, ACCELERATION_MIN,
                    "Acceleration", "[s]"
            );
        }

        /**
         * Checks whether or not duration is in Range
         *
         * @param duration Duration
         * @throws StatusRuntimeException if not in range
         */
        private void checkDurationInRange(long duration) {
            checkWithinMaxRangeIncluding(duration, DURATION_MAX, "Duration", "[s]");
            checkWithinMinRangeIncluding(duration, DURATION_MIN, "Duration", "[s]");
        }

        private void checkWithinMinRangeIncluding(
                long value,
                long min,
                String parameter,
                String units
        )
                throws StatusRuntimeException {
            if (value < min) {
                throw generateGRPCError(SiLAFramework.SiLAError.newBuilder()
                        .setErrorType(SiLAFramework.SiLAError.ErrorType.VALIDATION)
                        .setIdentifier(parameter)
                        .setMessage(
                                parameter + " must be at least " + String.valueOf(min) + " "
                                        + units)
                        .build()
                );
            }
        }

        private void checkWithinMaxRangeIncluding(
                long value,
                long max,
                String parameter,
                String units
        )
                throws StatusRuntimeException {
            if (value > max) {
                throw generateGRPCError(SiLAFramework.SiLAError.newBuilder()
                        .setErrorType(SiLAFramework.SiLAError.ErrorType.VALIDATION)
                        .setIdentifier(parameter)
                        .setMessage(parameter + " can be maximal " + String.valueOf(max)
                                + " " + units)
                        .build()
                );
            }
        }
    }
}
