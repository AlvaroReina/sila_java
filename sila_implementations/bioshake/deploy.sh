#!/usr/bin/env bash

# exit from the script if any command returns non-zero
set -e
# Only for debugging to print the command being run (Super Handy!)
# set -x

# parse cmd line args
if [ "$#" -ne 1 ]; then
    echo "Usage: ./deploy.sh [user@hostname]"
    exit
fi
hostname=$1

deployable_dir="~/.unitelabs/bioshake"

ssh ${hostname} -t "mkdir -p ${deployable_dir}"

# Push the latest built libraries (dependencies) and jar's for this module
rsync -av target/*.jar ${hostname}:${deployable_dir}
rsync -av target/lib/* ${hostname}:${deployable_dir}/lib
rsync -av bioshake.cmd ${hostname}:${deployable_dir}
