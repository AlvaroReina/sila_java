package smartlightbulb;

import io.grpc.stub.StreamObserver;
import sila2.org.silastandard.SiLAFramework;
import sila2.org.silastandard.core.exampleprovider.v1.ExampleProviderGrpc;
import sila2.org.silastandard.core.exampleprovider.v1.ExampleProviderOuterClass;
import sila2.org.silastandard.core.lightprovider.v1.LightProviderGrpc;
import sila2.org.silastandard.core.lightprovider.v1.LightProviderOuterClass;
import sila_features.SiLAServiceServer;
import sila_library.utils.Utils;
import sila_tools.bridge.server.SiLAServerCore;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;
import java.util.stream.IntStream;

import static sila_library.utils.Utils.blockUntilStop;

public class LightServer {
    private Logger logger = Logger.getLogger(LightServer.class.getName());
    final static String SERVER_NAME = "LightServer";
    private final static int SERVER_PORT = 50052; // Default

    private final SiLAServerCore siLAServerCore = new SiLAServerCore();

    public LightServer() {}

    private void start(String interfaceName, int serverPort) {
        try {
            Map<String, String> fdl = new HashMap<String, String>() {
                {
                    put("LightProvider", Utils.getFileString(this.getClass().getResourceAsStream("/LightProvider.xml")));
                    put("ExampleProvider", Utils.getFileString(this.getClass().getResourceAsStream("/ExampleProvider.xml")));
                }
            };

            this.siLAServerCore.start(
                    SERVER_NAME,
                    new SiLAServiceServer.ServerInfo(
                            "LightBulb controller",
                            "A controller coupled with the light located at my room",
                            "alvaro.example"
                    ),
                    fdl, serverPort, interfaceName, new LightControllerImpl(), new ExampleImpl()
            );
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage());
        }
    }
    public static void main(String[] args) {
        String interfaceName = "local";
        final LightServer server = new LightServer();
        server.start(interfaceName, SERVER_PORT);
        blockUntilStop();
        System.out.println("termination complete.");
    }

    private class LightControllerImpl extends LightProviderGrpc.LightProviderImplBase {
        @Override
        public void checkStatus(LightProviderOuterClass.CheckStatus_Parameters request, StreamObserver<LightProviderOuterClass.CheckStatus_Responses> responseObserver) {
            logger.info("Check status request");
            responseObserver.onNext(LightProviderOuterClass.CheckStatus_Responses.newBuilder().setStatus(SiLAFramework.Boolean.newBuilder().setValue(true)).build());
            responseObserver.onCompleted();
        }

        @Override
        public void hello(LightProviderOuterClass.Hello_Parameters request, StreamObserver<LightProviderOuterClass.Hello_Responses> responseObserver) {
            logger.info("Hello Request from " + request.getName().getValue());
            responseObserver.onNext(
                    LightProviderOuterClass.Hello_Responses.newBuilder()
                            .setGreeting(
                                    SiLAFramework.String.newBuilder()
                                            .setValue("Hello " + request.getName().getValue())
                            ).build()
            );
            responseObserver.onCompleted();
        }
    }

    private class ExampleImpl extends ExampleProviderGrpc.ExampleProviderImplBase {

        private final int TASK_DURATION = 5000;
        //TODO Concurrent hashmap containing current executions data (progress, identifiers...)
        @Override
        public void observableMethod(ExampleProviderOuterClass.ObservableMethod_Parameters request, StreamObserver<SiLAFramework.CommandConfirmation> responseObserver) {
            logger.info("Observable method request");
            //TODO More commandCorfimation data
            responseObserver.onNext(SiLAFramework.CommandConfirmation.newBuilder()
                    .setLifetimeOfExecution(
                            SiLAFramework.Duration.newBuilder()
                                    .setSeconds(TASK_DURATION/1000).build())
                    .build());
            responseObserver.onCompleted();
        }

        @Override
        public void observableMethodState(SiLAFramework.CommandExecutionUUID request, StreamObserver<SiLAFramework.ExecutionState> responseObserver) {
            logger.info("Observable method status request");
            //TODO Remaining time, running status.
            responseObserver.onNext(SiLAFramework.ExecutionState.newBuilder().build());
            responseObserver.onCompleted();
        }

        @Override
        public void observableMethodIntermediate(SiLAFramework.CommandExecutionUUID request, StreamObserver<ExampleProviderOuterClass.ObservableMethod_IntermediateResponses> responseObserver) {
            logger.info("Observable method intermediate request");
            //TODO Get progress from concurrent hashmap
            IntStream.range(0, 5).forEach(x -> {
                        responseObserver.onNext(
                                ExampleProviderOuterClass.ObservableMethod_IntermediateResponses.newBuilder()
                                        .setTaskStatus(
                                                SiLAFramework.Integer.newBuilder()
                                                        .setValue(x * 20).build()
                                        ).build()
                        );
                try {
                    //Simulating long computation
                    Thread.sleep(TASK_DURATION /5);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            });
            responseObserver.onCompleted();
        }

        @Override
        public void observableMethodResult(SiLAFramework.CommandExecutionUUID request, StreamObserver<ExampleProviderOuterClass.ObservableMethod_Responses> responseObserver) {
            logger.info("Observable method result request");
            new java.util.Timer().schedule(
                    new java.util.TimerTask() {
                        @Override
                        public void run() {
                            responseObserver.onNext(ExampleProviderOuterClass.ObservableMethod_Responses.newBuilder()
                                    .setCompleted(
                                            SiLAFramework.Boolean.newBuilder()
                                                    .setValue(true).build()
                                    ).build());
                            responseObserver.onCompleted();
                        }
                    },
                    TASK_DURATION
            );

        }
    }


}
