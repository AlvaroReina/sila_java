package smartlightbulb;

import io.grpc.ManagedChannel;
import io.grpc.stub.StreamObserver;
import sila2.org.silastandard.SiLAFramework;
import sila2.org.silastandard.core.exampleprovider.v1.ExampleProviderGrpc;
import sila2.org.silastandard.core.exampleprovider.v1.ExampleProviderOuterClass;
import sila2.org.silastandard.core.lightprovider.v1.LightProviderGrpc;
import sila2.org.silastandard.core.lightprovider.v1.LightProviderOuterClass;
import sila2.org.silastandard.core.silaservice.v1.SiLAServiceGrpc;
import sila2.org.silastandard.core.silaservice.v1.SiLAServiceOuterClass;
import sila_library.discovery.SiLAServerDiscovery;

import java.util.List;

public class LightClient {
    private LightProviderGrpc.LightProviderBlockingStub lightBlockingStub;
    private ExampleProviderGrpc.ExampleProviderStub exampleStub;


    private void buildStub(final ManagedChannel channel) {
        this.lightBlockingStub = LightProviderGrpc.newBlockingStub(channel);
        this.exampleStub = ExampleProviderGrpc.newStub(channel);
    }

    public void checkStatus() {
        System.out.println("Will try to check light status");
        LightProviderOuterClass.CheckStatus_Parameters.Builder parameter = LightProviderOuterClass.CheckStatus_Parameters.newBuilder();
        LightProviderOuterClass.CheckStatus_Responses result;
        result = lightBlockingStub.checkStatus(parameter.build());
        System.out.println("Result: " + result.getStatus().getValue());
    }

    public void hello(String name) {
        System.out.println("Will try to say hello");
        LightProviderOuterClass.Hello_Parameters message = LightProviderOuterClass.Hello_Parameters.newBuilder().setName(SiLAFramework.String.newBuilder().setValue(name)).build();
        LightProviderOuterClass.Hello_Responses result = lightBlockingStub.hello(message);
        System.out.println("Response: " + result.getGreeting().getValue());
    }

    public void ObservableMethod() {
        System.out.println("Will try to call the observable method");
        ExampleProviderOuterClass.ObservableMethod_Parameters parameters = ExampleProviderOuterClass.ObservableMethod_Parameters.newBuilder().build();
        StreamObserver<ExampleProviderOuterClass.ObservableMethod_Responses> resultStream = new StreamObserver<ExampleProviderOuterClass.ObservableMethod_Responses>() {
            @Override
            public void onNext(ExampleProviderOuterClass.ObservableMethod_Responses observableMethod_responses) {
                System.out.println("Result: " + observableMethod_responses.getCompleted().getValue());
            }

            @Override
            public void onError(Throwable throwable) {
                System.out.println("Result: ERROR");
            }

            @Override
            public void onCompleted() {
                System.out.println("Observable method result completed");
            }
        };

        StreamObserver<ExampleProviderOuterClass.ObservableMethod_IntermediateResponses> intermediateStream = new StreamObserver<ExampleProviderOuterClass.ObservableMethod_IntermediateResponses>() {
            @Override
            public void onNext(ExampleProviderOuterClass.ObservableMethod_IntermediateResponses observableMethod_intermediateResponses) {
                System.out.println("Progress: " + observableMethod_intermediateResponses.getTaskStatus().getValue() + "%");
            }

            @Override
            public void onError(Throwable throwable) {
                System.out.println("Intermediate Error");
            }

            @Override
            public void onCompleted() {
                System.out.println("Intermediate Completed");
            }
        };

        StreamObserver<SiLAFramework.CommandConfirmation> initiateMethod = new StreamObserver<SiLAFramework.CommandConfirmation>() {
            @Override
            public void onNext(SiLAFramework.CommandConfirmation commandConfirmation) {
                System.out.println("Received commandConfirmation");
                System.out.println("Time to complete: " + commandConfirmation.getLifetimeOfExecution().getSeconds() + " seconds");
                exampleStub.observableMethodIntermediate(commandConfirmation.getCommandId(), intermediateStream);
                exampleStub.observableMethodResult(commandConfirmation.getCommandId(), resultStream);
            }

            @Override
            public void onError(Throwable throwable) {
                System.out.println("Error at observable method call");
            }

            @Override
            public void onCompleted() {
                System.out.println("Observable method call completed");
            }
        };
        exampleStub.observableMethod(parameters, initiateMethod);
    }

    public static void main(String[] args) {
        LightClient client = new LightClient();
        SiLAServerDiscovery.enable();

        final ManagedChannel serviceChannel = SiLAServerDiscovery.blockAndGetChannel(LightServer.SERVER_NAME);
        SiLAServiceGrpc.SiLAServiceBlockingStub serviceBlockingStub = SiLAServiceGrpc.newBlockingStub(serviceChannel);

        System.out.println("Found Features: ");
        List<SiLAServiceOuterClass.DataType_Identifier> dataTypeIdentifiers = serviceBlockingStub.getImplementedFeatures(SiLAServiceOuterClass.Get_ImplementedFeatures_Parameters.newBuilder().build()).getImplementedFeaturesList();

        for (SiLAServiceOuterClass.DataType_Identifier dataTypeIdentifier : dataTypeIdentifiers) {
            System.out.println("\t" + dataTypeIdentifier.getIdentifier());
        }

        client.buildStub(serviceChannel);

        client.checkStatus();
        System.out.println();
        client.hello("Alvaro");
        System.out.println();
        client.ObservableMethod();
    }
}
